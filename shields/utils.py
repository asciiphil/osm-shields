#!/usr/bin/python

import cmath
import copy
import math
import re
import svgwrite

from pkg_resources import resource_stream

from .font import get_font
from .readsvg import read as read_svg

COLOR_WHITE  = 'white'
COLOR_BLACK  = 'black'
COLOR_BLUE   = '#003f87'  # Pantone 294
COLOR_RED    = '#af1e2d'  # Pantone 187
COLOR_GREEN  = '#006b54'  # Pantone 342
COLOR_BROWN  = '#603311'  # Pantone 469
COLOR_YELLOW = '#fcd116'  # Pantone 116
COLOR_ORANGE = '#dd7500'  # Pantone 152

STYLE_GENERIC = 'generic'
STYLE_GUIDE   = 'guide'
STYLE_CUTOUT  = 'cutout'
STYLE_SIGN    = 'sign'
STYLES = frozenset([STYLE_GENERIC, STYLE_GUIDE, STYLE_CUTOUT, STYLE_SIGN])

REF_MARGIN = 1.0

def text_length(text, font_height, font_series, spacing=1.0, size_func=None):
    font = get_font(font_series)
    text = [c if c in font.glyphs else '?' for c in text]
    if size_func is None:
        text_width_native = sum([font.glyphs[c].width for c in text]) - \
                            font.glyphs[text[0]].left_space - \
                            font.glyphs[text[-1]].right_space
        inner_spacing = sum([font.glyphs[c].left_space + font.glyphs[c].right_space for c in text]) - \
                        font.glyphs[text[0]].left_space - \
                        font.glyphs[text[-1]].right_space
        inner_spacing *= 1.0 - spacing
    else:
        text_width_native = 0
        inner_spacing = 0
        for i, c in enumerate(text):
            height, series, spacing = size_func(c)
            char_scale = height / font_height
            glyph = font.glyphs[c]
            text_width_native += glyph.width * char_scale
            inner_spacing += (glyph.left_space + glyph.right_space) * (1.0 - spacing) * char_scale
            if i == 0:
                text_width_native -= glyph.left_space * char_scale
                inner_spacing -= glyph.left_space * (1.0 - spacing) * char_scale
            elif i == len(text)-1:
                text_width_native -= glyph.right_space * char_scale
                inner_spacing -= glyph.right_space * (1.0 - spacing) * char_scale
    return (text_width_native - inner_spacing) / get_font(font_series).cap_height * font_height

def char_code(char, series):
    return '_' + series + format(ord(char), 'x')

def add_chars_to_drawing(drawing, text, font_series):
    font = get_font(font_series)
    text = [c if c in font.glyphs else '?' for c in text]
    defs = set([e['id'] for e in drawing.defs.elements])
    for c in text:
        if char_code(c, font_series) not in defs:
            glyph = font.glyphs[c]
            if glyph.d is not None and glyph.d != '':
                drawing.defs.add(drawing.path(glyph.d, id=char_code(c, font_series)))
            defs.add(char_code(c, font_series))
                
def render_text(drawing, text, x, y, width, height, font_series, color=COLOR_WHITE, center=True, debug=False, spacing=1.0, size_func=None, vertical_center=False):
    add_chars_to_drawing(drawing, text, font_series)
    font = get_font(font_series)
    text = [c if c in font.glyphs else '?' for c in text]

    text_width = text_length(text, height, font_series, spacing, size_func)
    if text_width > width:
        text_scale = height / font.cap_height * width / text_width
        y_offset = (height - height * width / text_width) / 2
        text_width = width
    else:
        text_scale = float(height) / font.cap_height
        y_offset = 0

    # This group serves to consolidate common properties of the text, including
    # its transform.  Note that the y scale factor is negative!  This is because
    # the roadgeek font assumes (0,0) is in the lower lefthand corner and we
    # have to adjust for SVG's (0,0) being in the *upper* lefthand corner.
    container = drawing.g(fill=color, transform='matrix({} 0 0 {} {} {})'.format(text_scale, -1 * text_scale, x, y + height - y_offset))
    container.set_desc(title=''.join(text))
    if debug:
        container.add(drawing.rect((0, -y_offset / text_scale), (width / text_scale, height / text_scale), stroke='green', fill='none', stroke_width=height / 100 / text_scale))

    if size_func:
        char_height, char_series, char_spacing = size_func(text[0])
        left_space = get_font(char_series).glyphs[text[0]].left_space
    else:
        left_space = font.glyphs[text[0]].left_space
    if center:
        pos = width / text_scale / 2 - text_width / text_scale / 2 - left_space
    else:
        pos = -left_space
    pos += left_space * (1.0 - spacing)
    for c in text:
        if size_func:
            char_height, char_series, char_spacing = size_func(c)
            glyph = get_font(char_series).glyphs[c]
            left_space = glyph.left_space
            char_scale = char_height / height
        else:
            char_spacing = spacing
            glyph = font.glyphs[c]
            char_scale = 1.0
        pos -= glyph.left_space * (1.0 - char_spacing) * char_scale
        if glyph.d is not None and glyph.d != '':
            if vertical_center and char_scale != 1.0:
                transform = 'translate({} {})'.format(pos, (height - char_height)/2 / text_scale)
            else:
                transform = 'translate({})'.format(pos)
            if char_scale != 1.0:
                transform += ' scale({})'.format(char_scale)
            path = drawing.use('#' + char_code(c, font_series), transform=transform)
            container.add(path)
        pos += glyph.width * char_scale
        pos -= glyph.right_space * (1.0 - char_spacing) * char_scale
        
    return container

def render_text_along_arc(drawing, text, center, radius, angle, height, series, color, debug=False, spacing=1.0, size_func=None, centered=True, invert=False):
    """Places text along an arc defined by CENTER and RADIUS.  If CENTERED is
    True, ANGLE is the angle from the CENTER to the center of the text.
    Otherwise, it's the angle to the left side of the text.

    CENTER should be a complex number.

    If INVERT is true, the text will be placed with its top along the arc
    instead of its bottom.

    """

    add_chars_to_drawing(drawing, text, series)
    font = get_font(series)
    text = [c if c in font.glyphs else '?' for c in text]

    angle_rad = float(angle) / 180 * math.pi - math.pi/2
    text_width = text_length(text, height, series, spacing, size_func)
    text_width_rad = text_width / radius
    text_scale = float(height) / font.cap_height

    # This group serves to consolidate common properties of the text, including
    # its transform.  Note that the y scale factor is negative!  This is because
    # the roadgeek font assumes (0,0) is in the lower lefthand corner and we
    # have to adjust for SVG's (0,0) being in the *upper* lefthand corner.
    container = drawing.g(
        fill=color,
        transform='translate({0} {1})'.format(center.real, center.imag))
    container.set_desc(title=''.join(text))
    if debug:
        if centered:
            offset = text_width_rad/2
        else:
            offset = 0
        debug_outline = drawing.path(
            stroke='green', fill='none', stroke_width=height / 10)
        container.add(debug_outline)
        ll = cmath.rect(radius, angle_rad - offset)
        ul = cmath.rect(radius + height, angle_rad - offset)
        lr = cmath.rect(radius, angle_rad - offset + text_width_rad)
        ur = cmath.rect(radius + height, angle_rad - offset + text_width_rad)
        debug_outline.push('M', (ll.real, ll.imag))
        debug_outline.push_arc(
            (lr.real, lr.imag), 0, radius, large_arc=text_width_rad > math.pi,
            angle_dir='+', absolute=True)
        debug_outline.push('L', (ur.real, ur.imag))
        debug_outline.push_arc(
            (ul.real, ul.imag), 0, radius + height, large_arc=text_width_rad > math.pi,
            angle_dir='-', absolute=True)
        debug_outline.push('Z')

    if size_func:
        char_height, char_series, char_spacing = size_func(text[0])
        left_space = get_font(char_series).glyphs[text[0]].left_space * text_scale
    else:
        left_space = font.glyphs[text[0]].left_space * text_scale
    if centered:
        pos = -text_width_rad/2 - left_space / radius
    else:
        pos = 0.0
    pos += left_space * (1.0 - spacing) / radius
    for c in reversed(text) if invert else text:
        if size_func:
            char_height, char_series, char_spacing = size_func(c)
            glyph = get_font(char_series).glyphs[c]
            left_space = glyph.left_space
            char_scale = char_height / height * text_scale
        else:
            char_spacing = spacing
            glyph = font.glyphs[c]
            char_scale = text_scale
        pos -= glyph.left_space * (1.0 - char_spacing) * char_scale / radius
        pos += glyph.width * char_scale / radius / 2
        if glyph.d is not None and glyph.d != '':
            path = drawing.use(
                '#' + char_code(c, series),
                transform='rotate({2}) matrix({5}{0} 0 0 {4}{0} {4}{3} -{1})'.format(
                    char_scale,
                    radius + (height if invert else 0),
                    pos / math.pi * 180 + angle,
                    glyph.width * char_scale / 2,
                    '' if invert else '-',
                    '-' if invert else ''))
            container.add(path)
        pos += glyph.width * char_scale / radius / 2
        pos -= glyph.right_space * (1.0 - char_spacing) * char_scale / radius
        
    return container

def series_for_text(text, font_height, font_width, available_series=['D', 'C', 'B'], spacing=1.0):

    """Takes a set of dimensions (height and width) and a list of series in
    preferred order.  Returns the first of the series that will allow the
    text at the given height to fit into the given width.  If no size will
    work, returns the last series in the list."""
    for s in available_series:
        if text_length(text, font_height, s, spacing=spacing) <= font_width:
            return s
    return available_series[-1]

def intersect_line_and_arc(p1, p2, arc_center, arc_radius):
    """Returns the coordinates of the point that lies on both the line from p1
    to p2 and the arc around the given center with the specified radius.  If
    there are two such points, the one with the lower x coordinate will be
    returned."""
    # box equation:
    # y = mx + b
    # m = box_slope
    # b = box_intercept
    box_slope = (p1.imag - p2.imag) / (p1.real - p2.real)
    box_intercept = p1.imag - box_slope * p1.real

    # circle equation:
    # (x - c_x)^2 + (y - c_y)^2 = z^2
    # (c_x, c_y) = arc_center
    # z = arc_radius
    # Now we substitute the above y = mx + B into the circle equation:
    # (x - c_x)^2 + (mx + B - c_y)^@ = z^2
    # Multiplying that out gives:
    # (m^2+1)x^2 + (2mB-2mc_y-2c_x)x + (c_x^2 + (B-c_y)^2 - z^2) = 0
    # Thus, for the quadratic equation, we have:
    # a = m^2 + 1
    a = box_slope**2 + 1
    # b = 2mB - 2mc_y - 2c_x
    b = 2 * box_slope * box_intercept - 2 * box_slope * arc_center.imag - 2 * arc_center.real
    # c = c_x^2 + (B-c_y)^2 - z^2
    c = arc_center.real**2 + (box_intercept - arc_center.imag)**2 - arc_radius**2

    x_0 = (-b + math.sqrt(b**2 - 4 * a * c)) / (2 * a)
    x_1 = (-b - math.sqrt(b**2 - 4 * a * c)) / (2 * a)
    x = x_0 if x_0 < x_1 else x_1
    y = box_slope * x + box_intercept
    
    return x + y * 1j

def rectangle_in_ellipse_with_ratio(major_axis, minor_axis, ratio):
    """Given the dimensions of an ellipse and an aspect ratio (major/minor),
    returns the width and height of the largest rectangle with the given aspect
    ratio that will fit inside the ellipse."""

    a = float(major_axis) / 2
    b = float(minor_axis) / 2
    r = float(ratio)

    # We're looking for x and y such that:
    #   x / y == r
    #   x**2 / a**2 + y**2 / b**2 == 1

    y = math.sqrt((a**2 * b**2) / (r**2 * b**2 + a**2))
    x = r * y
    
    return (2*x, 2*y)

def line_tangent_to_one_circle(p, c, r, right=True):
    """Given a point (p), a circle center (c) and radius (r), gives the point
    intersecting the circle and a line through the given point tangent to
    the circle.  If "left" is True, gives the point to the left of the
    line from p to c, otherwise, gives the right intersection."""
    distance = abs(c - p)
    alpha = math.asin(r / distance)
    beta = math.atan2(c.imag - p.imag, c.real - p.real)
    if right:
        theta = beta + alpha
        return c + r * -math.sin(theta) + r * math.cos(theta) * 1j
    else:
        theta = beta - alpha
        return c + r * math.sin(theta) + r * -math.cos(theta) * 1j

def line_tangent_to_two_circles(c1, r1, c2, r2, right=True):
    """Returns the two points where a line tangent to both circles intersects
    each."""

    if r2 > r1:
        return tuple(reversed(line_tangent_to_two_circles(c2, r2, c1, r1, not right)))

    xp = (c2.real * r1 - c1.real * r2) / (r1 - r2)
    yp = (c2.imag * r1 - c1.imag * r2) / (r1 - r2)
    P = xp + yp * 1j

    return (line_tangent_to_one_circle(P, c1, r1, right),
            line_tangent_to_one_circle(P, c2, r2, right))
    
def intersect_circles(c1, r1, c2, r2):
    distance = math.sqrt((c2.real - c1.real)**2 + (c2.imag - c1.imag)**2)
    if distance > r1 + r2 or distance < abs(r1 - r2):
        return None

    ex = (c2.real - c1.real) / distance
    ey = (c2.imag - c1.imag) / distance

    x = (r1**2 - r2**2 + distance**2) / (2 * distance)
    y = math.sqrt(r1**2 - x**2)

    i1 = (c1.real + ex * x - ey * y) + \
         (c1.imag + ey * x + ex * y) * 1j
    i2 = (c1.real + ex * x + ey * y) + \
         (c1.imag + ey * x - ex * y) * 1j

    return (i1, i2)

def intersect_lines(l1a, l1b, l2a, l2b):
    """Given two points for each of two lines, returns the point where the lines
    intersect."""
    px = ((l1a.real * l1b.imag - l1a.imag * l1b.real) * (l2a.real - l2b.real) - \
          (l1a.real - l1b.real) * (l2a.real * l2b.imag - l2a.imag * l2b.real)) / \
         ((l1a.real - l1b.real) * (l2a.imag - l2b.imag) - \
          (l1a.imag - l1b.imag) * (l2a.real - l2b.real))
    py = ((l1a.real * l1b.imag - l1a.imag * l1b.real) * (l2a.imag - l2b.imag) - \
          (l1a.imag - l1b.imag) * (l2a.real * l2b.imag - l2a.imag * l2b.real)) / \
         ((l1a.real - l1b.real) * (l2a.imag - l2b.imag) - \
          (l1a.imag - l1b.imag) * (l2a.real - l2b.real))
    return px + py * 1j

def compose_shields(shields):
    """Given a list of svgwrite Drawing objects, returns a new Drawing with
    all of the objects stacked vertically and centered horizontally.

    """
    drawing = svgwrite.Drawing()

    defs = set()
    
    width = max([float(s['width']) for s in shields])
    y = 0
    for shield in shields:
        for e in shield.defs.elements:
            if e['id'] not in defs:
                drawing.defs.add(e)
                defs.add(e['id'])
        g = drawing.g(transform='translate({},{})'.format((width-float(shield['width']))/2, y))
        drawing.add(g)
        for e in shield.elements[1:]:
            g.add(e)
        y += float(shield['height'])
    drawing['width'] = width
    drawing['height'] = y

    return drawing

def read_template(template):
    return read_svg(resource_stream(__name__, 'template/{}.svg'.format(template)))

def read_and_position_template(template, x, y, width, height):
    tpl = read_template(template)
    if len(tpl.elements) > 2:
        result = tpl.g()
        for e in tpl.elements[1:]:
            result.add(e)
    else:
        result = tpl.elements[1]
    if 'viewBox' in tpl.attribs:
        tpl_vb_x, tpl_vb_y, tpl_width, tpl_height = \
            [float(f) for f in tpl['viewBox'].split()]
    else:
        tpl_width = float(tpl['width'])
        tpl_height = float(tpl['height'])
    tpl_scale_x = width / tpl_width
    tpl_scale_y = height / tpl_height

    # Determine what sort of transform to apply to the object.
    if x == 0 and y == 0 and tpl_scale_x == 1 and tpl_scale_y == 1:
        # No transform needed.
        return result
    if x == 0 and y == 0 and tpl_scale_x == tpl_scale_y:
        # No translation; uniform scaling
        transform = 'scale({})'.format(tpl_scale_x)
    elif x == 0 and y == 0:
        # No translation; disparate scaling
        transform = 'scale({} {})'.format(tpl_scale_x, tpl_scale_y)
    elif y == 0 and tpl_scale_x == 1 and tpl_scale_y == 1:
        # x translation only
        transform = 'translate({})'.format(x)
    elif tpl_scale_x == 1 and tpl_scale_y == 1:
        # translation only
        transform = 'translate({} {})'.format(x, y)
    elif y == 0 and tpl_scale_x == tpl_scale_y:
        # unidirectional translation and scaling.
        transform = 'translate({}) scale({})'.format(x, tpl_scale_x)
    else:
        # General form; translation and scaling.
        transform = 'matrix({} 0 0 {} {} {})'.format(
            tpl_scale_x, tpl_scale_y, x, y)
        
    if 'transform' in result.attribs:
        result['transform'] = transform + ' ' + result['transform']
    else:
        result['transform'] = transform

    return result

def split_num_char(string):
    """Takes a string consiting of letters and numbers and decomposes it into
    alternating integers and character strings.  e.g. "56A2" yields 56,
    "A", and 2.  Always returns a number first; if string starts with a
    letter, the first number will be zero.

    """
    first = True
    for i, s in re.findall(r'([\d]+)|([^\d]+)', string):
        if i:
            yield int(i)
        else:
            if first:
                yield 0
            yield s
        first = False
            
def partition_upper_lower(string):
    """Takes a string consiting of upper and lower case letters.  Returns a
    list of strings split into uppercase and lowercase sections.

    e.g. "This is FDR Drive." will yield ["T", "his is ", "FDR", " ", "D", "rive."]."""
    for u, l in re.findall(r'([A-Z]+)|([^A-Z]+)', string):
        if u:
            yield u
        else:
            yield l
            
