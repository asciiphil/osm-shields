#!/usr/bin/python

import xml.etree.ElementTree as ET

import svgwrite

SVG_PREFIX = '{http://www.w3.org/2000/svg}'

def read(filename):
    et = ET.parse(filename)
    src = et.getroot()
    dst = svgwrite.Drawing(debug=False)
    for k, v in src.attrib.items():
        dst[k] = v
    copy_element(src, dst, dst)
    return dst
    
def copy_element(src, dst, dwg):
    for child in src:
        if child.tag.startswith(SVG_PREFIX):
            tag = child.tag[len(SVG_PREFIX):]
            if tag == 'defs':
                pass
            elif tag in ('rect'):
                attrib = child.attrib
                insert = (float(attrib['x']), float(attrib['y']))
                size = (float(attrib['width']), float(attrib['height']))
                for k in ['x', 'y', 'width', 'height']:
                    del attrib[k]
                new_child = getattr(dwg, tag)(insert, size, **attrib)
                dst.add(new_child)
                copy_element(child, new_child, dwg)
            elif tag in ('circle'):
                attrib = child.attrib
                center = (float(attrib['cx']), float(attrib['cy']))
                del attrib['cx']
                del attrib['cy']
                new_child = getattr(dwg, tag)(center, **attrib)
                dst.add(new_child)
                copy_element(child, new_child, dwg)
            elif tag in ('ellipse'):
                attrib = child.attrib
                center = (float(attrib['cx']), float(attrib['cy']))
                radii = (float(attrib['rx']), float(attrib['ry']))
                for k in ['cx', 'cy', 'rx', 'ry']:
                    del attrib[k]
                new_child = getattr(dwg, tag)(center, radii, **attrib)
                dst.add(new_child)
                copy_element(child, new_child, dwg)
            elif tag in ('text', 'tspan'):
                for attr in ('x', 'y'):
                    if attr in child.attrib:
                        child.attrib[attr] = [child.attrib[attr]]
                new_child = getattr(dwg, tag)(child.text if child.text is not None else '', **child.attrib)
                dst.add(new_child)
                copy_element(child, new_child, dwg)
            elif hasattr(dwg, tag):
                new_child = getattr(dwg, tag)(**child.attrib)
                dst.add(new_child)
                copy_element(child, new_child, dwg)
