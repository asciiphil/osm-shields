#!/usr/bin/python
"""
Classes and functions for geometric manipulation.

"""

import cmath
import math


# All points are complex numbers.
def point(x, y):
    return x + y * 1j
def unpoint(c):
    return (c.real, c.imag)

def pythagoras(x=None, y=None, h=None):
    """Given two sides of a right triangle, returns the third."""
    if x is not None and y is not None and h is not None:
        raise ValueError('No values needed to calculate.')
    if x is None:
        if y is None or h is None:
            raise ValueError('Must supply two of the three values.')
        return math.sqrt(h**2 - y**2)
    if y is None:
        return pythagoras(y, x, h)
    return math.sqrt(x**2 + y**2)

def unit_vector(start, end):
    """Returns a unit vector in the direction from start to end."""
    return (end - start) / abs(end - start)


class Circle:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

    def __repr__(self):
        return 'Circle(({}+{}j), {})'.format(self.center.real, self.center.imag, self.radius)

    @classmethod
    def from_chord_and_radius(cls, p1, p2, radius, left=True):
        """If left is True, you will get the circle with its center to the left of
        the vector from p1 to p2."""
        distance_from_chord_to_circle = \
            (2 * radius - math.sqrt(4 * radius**2 - abs(p2 - p1)**2)) / 2
        distance_from_chord_to_center = radius - distance_from_chord_to_circle
        vector_to_chord_middle = (p2 - p1) / 2
        vector_to_center = unit_vector(p1, p2) * (-1j if left else 1j) * distance_from_chord_to_center
        center = p1 + vector_to_chord_middle + vector_to_center
        return cls(center, radius)
        
    def point_on_circle(self, x=None, y=None):
        """Returns the point or points on the circle with the given coordinate.

        If there are two points, the one closer to the origin is returned
        first.
        """
        if x is None and y is None:
            raise ValueError('Must pass either x or y.')
        if x is not None and y is not None:
            raise ValueError('Cannot pass both x and y.')
        if x is not None:
            sqrt = math.sqrt(self.radius**2 - (x - self.center.real)**2)
            p0 = x + (self.center.imag - sqrt) * 1j
            p1 = x + (self.center.imag + sqrt) * 1j
        else:
            sqrt = math.sqrt(self.radius**2 - (y - self.center.imag)**2)
            p0 = (self.center.real - sqrt) + y * 1j
            p1 = (self.center.real + sqrt) + y * 1j

        if sqrt == 0:
            return (p0,)

        if abs(p0) == abs(p1) and p1.real < p0.real:
            p0, p1 = p1, p0
        elif abs(p1) < abs(p0):
            p0, p1 = p1, p0
    
        return (p0, p1)

    def add_radius(self, length):
        return Circle(self.center, self.radius + length)

    def translate(self, vector):
        return Circle(self.center + vector, self.radius)
    
    def intersect_line(self, line):
        """Returns the points of intersection between this circle and the given line.

        May return two, one, or zero points.  If two points, the one
        closest to the origin will be returned first.
        """
        # circle equation:
        # (x - c_x)^2 + (y - c_y)^2 = z^2
        # (c_x, c_y) = arc_center
        # z = arc_radius
        # Now we substitute the above y = mx + B into the circle equation:
        # (x - c_x)^2 + (mx + B - c_y)^@ = z^2
        # Multiplying that out gives:
        # (m^2+1)x^2 + (2mB-2mc_y-2c_x)x + (c_x^2 + (B-c_y)^2 - z^2) = 0
        # Thus, for the quadratic equation, we have:
        # a = m^2 + 1
        a = line.slope**2 + 1
        # b = 2mB - 2mc_y - 2c_x
        b = 2 * line.slope * line.intercept - 2 * line.slope * self.center.imag - 2 * self.center.real
        # c = c_x^2 + (B-c_y)^2 - z^2
        c = self.center.real**2 + (line.intercept - self.center.imag)**2 - self.radius**2

        quadratic_inner = b**2 - 4 * a * c
        if quadratic_inner < 0:
            # No interstction
            return ()
        
        x0 = (-b + math.sqrt(quadratic_inner)) / (2 * a)
        x1 = (-b - math.sqrt(quadratic_inner)) / (2 * a)

        p0 = line.point_on_line(x=x0)
        p1 = line.point_on_line(x=x1)
        if quadratic_inner == 0:
            return (p0,)

        if abs(p0) == abs(p1) and p1.real < p0.real:
            p0, p1 = p1, p0
        elif abs(p1) < abs(p0):
            p0, p1 = p1, p0
    
        return (p0, p1)

    def intersect_circle(self, other):
        if self == other:
            raise ValueError('Circles overlap exactly.')

        distance = math.sqrt((other.center.real - self.center.real)**2 +
                             (other.center.imag - self.center.imag)**2)
        if distance > self.radius + other.radius \
           or distance < abs(other.radius - self.radius):
            return ()
    
        ex = (other.center.real - self.center.real) / distance
        ey = (other.center.imag - self.center.imag) / distance
    
        x = (self.radius**2 - other.radius**2 + distance**2) / (2 * distance)
        y = math.sqrt(self.radius**2 - x**2)
    
        i1 = (self.center.real + ex * x - ey * y) + \
             (self.center.imag + ey * x + ex * y) * 1j
        i2 = (self.center.real + ex * x + ey * y) + \
             (self.center.imag + ey * x - ex * y) * 1j

        if i1 == i2:
            return (i1,)
        
        if abs(i1) == abs(i2) and i2.real < i1.real:
            i1, i2 = i2, i1
        elif abs(i2) < abs(i1):
            i1, i2 = i2, i1
        
        return (i1, i2)

    def tangent_line_from_point(self, point, left=True, include_intersection=False):
        """Returns a Line that is tangent to this circle and passes through the
        given point.

        If `left` is True, returns the line that passes to the left of the
        line from the point to the circle.  Otherwise, returns the line to
        the right.

        If `include_intersection` is true, two values are returned; the
        second is the point of intersection between the circle and the
        tangent line.
        """
        if abs(self.center - point) < self.radius:
            raise ValueError('Point {} is inside circle {}; no tangent line to it exists.'.format(point, self))
        distance_to_tangent_point = math.sqrt(abs(self.center - point)**2 - self.radius**2)
        alpha = math.asin(self.radius / abs(self.center - point))
        r, phi = cmath.polar(self.center - point)
        if left:
            theta = phi + alpha
        else:
            theta = phi - alpha
        tangent_vector = cmath.rect(distance_to_tangent_point, theta)
        tangent_point = point + tangent_vector
        tangent_line = Line.from_points(point, tangent_point)
        if include_intersection:
            return tangent_line, tangent_point
        else:
            return tangent_line
        
class Line(object):
    def __init__(self, slope, intercept):
        self.slope = slope
        self.intercept = intercept

    def __repr__(self):
        return 'Line({}, {})'.format(self.slope, self.intercept)
    
    @classmethod
    def horizontal(cls, y):
        return cls(0.0, y)

    @classmethod
    def vertical(cls, x):
        return VerticalLine(x)

    @classmethod
    def from_points(cls, p1, p2):
        slope = (p2.imag - p1.imag) / (p2.real - p1.real)
        intercept = p1.imag - slope * p1.real
        return cls(slope, intercept)
        
    def intersect_circle(self, circle):
        """Calls circle.intersect_line(self); see that function for details."""
        return circle.intersect_line(self)
    
    def point_on_line(self, x=None, y=None):
        """Returns the point on the line with the given coordinate."""
        if x is None and y is None:
            raise ValueError('Must pass either x or y.')
        if x is not None and y is not None:
            raise ValueError('Cannot pass both x and y.')
        if x is not None:
            return x + (self.slope * x + self.intercept) * 1j
        else:
            return y * 1j + (y - self.intercept) / self.slope

    def translate(self, vector):
        """Add a complex number to a line to shift the line using the complex
        number as a vector.  Returns a new Line."""
        return Line(self.slope, self.intercept + self.slope * vector.real - vector.imag)

    def intersect_line(self, other):
        x = (self.intercept - other.intercept) / (other.slope - self.slope)
        y = self.slope * x + self.intercept
        return point(x, y)

class VerticalLine(Line):
    def __init__(self, x):
        self.x = x

    def __repr__(self):
        return 'VerticalLine({})'.format(self.x)
    
    def intersect_circle(self, circle):
        return circle.point_on_circle(x=self.x)
    
    def point_on_line(self, x=None, y=None):
        if x is None and y is None:
            raise ValueError('Must pass either x or y.')
        if x is not None and y is not None:
            raise ValueError('Cannot pass both x and y.')
        if x is not None:
            return float('Inf')
        else:
            return y

    def translate(self, vector):
        return VerticalLine(self.x + vector.real)
