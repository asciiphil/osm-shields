#!/usr/bin/python

# American Samoa!
#
# I haven't been able to locate an official government source for route
# marker design in American Samoa.  The stuff below is based on photos of
# signs.
#
# I have specifically been unable to locate any images of guide signs with
# American Samoa route markers on them, so I'm just using the "sign" style
# for everything.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:AS'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:AS'] = lambda m,r: USSamoaShield(m, r, style).make_shield()

class USSamoaShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(USSamoaShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 24.0
        self.border_width = 0.5
        self.corner_radius = 1.5
        
        self.bottom_width = 20.0
        self.bottom_thickness = 1.5
        self.bottom_y = 21.0
        self.stroke_width = 1.0
        self.roof_width = 19.5
        self.hut_width = 17.0
        self.roof_y = 1.5
        self.roof_height = 10.5
        self.roof_arc_radius = 11.0

        self.ref_height = 5.0
        self.ref_series = 'D'
        self.ref_x = self.width/2 - self.hut_width/2 + self.stroke_width + REF_MARGIN
        ref_midline = 16.5
        self.ref_y = ref_midline - self.ref_height/2
        self.ref_width = self.width - self.ref_x * 2

        self.as_height = 2.0
        self.as_series = 'C'
        self.as_x = self.ref_x
        self.as_width = self.ref_width
        self.american_y = 6.0
        self.samoa_y = 8.5
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN
        self.hut_color = COLOR_BLACK

    @property
    def blank_key(self):
        return 'us_as'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.border_width/2
        bg_g.add(self.drawing.rect(
            (border_offset, border_offset),
            (self.width - border_offset * 2, self.height - border_offset * 2),
            self.corner_radius - border_offset,
            stroke=self.fg, fill=self.bg, stroke_width=self.border_width))

        bg_g.add(self.drawing.line(
            (self.width/2 - self.bottom_width/2, self.bottom_y + self.bottom_thickness/2),
            (self.width/2 + self.bottom_width/2, self.bottom_y + self.bottom_thickness/2),
            stroke=self.hut_color, stroke_width=self.bottom_thickness))

        roof_corner_l = (
            self.width/2 - self.roof_width/2 + self.stroke_width/2,
            self.roof_y + self.roof_height - self.stroke_width/2)
        roof_corner_r = (self.width - roof_corner_l[0], roof_corner_l[1])
        roof_tip = (self.width/2, self.roof_y + self.stroke_width/2)
        hut_ul_corner = (
            self.width/2 - self.hut_width/2 + self.stroke_width/2,
            roof_corner_l[1])
        hut_ur_corner = (self.width - hut_ul_corner[0], hut_ul_corner[1])
        hut_height = self.bottom_y + self.bottom_thickness/2 - hut_ul_corner[1]
        hut = self.drawing.path(
            stroke=self.hut_color, fill='none', stroke_width=self.stroke_width)
        hut.push('M', roof_corner_l)
        hut.push_arc(roof_tip, 0, self.roof_arc_radius, large_arc=False,
                     angle_dir='+', absolute=True)
        hut.push_arc(roof_corner_r, 0, self.roof_arc_radius, large_arc=False,
                     angle_dir='+', absolute=True)
        hut.push('Z')
        hut.push('M', hut_ul_corner)
        hut.push('v', hut_height)
        hut.push('M', hut_ur_corner)
        hut.push('v', hut_height)
        bg_g.add(hut)

        bg_g.add(render_text(
            self.drawing, 'AMERICAN', self.as_x, self.american_y, self.as_width,
            self.as_height, self.as_series, self.fg))
        bg_g.add(render_text(
            self.drawing, 'SAMOA', self.as_x, self.samoa_y, self.as_width,
            self.as_height, self.as_series, self.fg))
        
        return bg_g
