#!/usr/bin/python

# https://www.deldot.gov/Publications/manuals/de_mutcd/pdfs/archived/Sign_Book_April_2010.pdf#page=114

from ..us_state import STYLE_ELLIPSE, USGenericStateShield

def register(network_registry, style):
    network_registry['US:DE'] = lambda m,r: USGenericStateShield(m, r, STYLE_ELLIPSE).make_shield()
