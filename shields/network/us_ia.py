#!/usr/bin/python

# https://iowadot.gov/design/SRP/IndividualStandards/esi121.pdf#page=3

from ..us_state import USGenericStateShield, STYLE_ELONGATED
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:IA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:IA'] = lambda m,r: USGenericStateShield(m, r, STYLE_ELONGATED).make_shield()

