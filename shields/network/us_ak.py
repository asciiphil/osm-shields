#!/usr/bin/python

# http://www.dot.state.ak.us/stwddes/dcstraffic/assets/pdf/asds/asds_signs_2015.pdf#page=106

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:AK'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:AK'] = lambda m,r: AKStateShield(m, r).make_shield()

# Skewing the text to approximate italics causes an offset to the
# baseline.  This is a fudge factor to put it back roughly where it should
# be.
AK_X_FUDGE = 1.0

STAR_ID = 'us_ak-star'

class AKStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(AKStateShield, self).__init__(modifier, ref)

        star_offset_y = [None] * 7
        star_offset_x = [None] * 7
        
        self.width = 24.0 # A
        self.height = 24.0 # A
        self.border_width = 0.75 # B
        ak_offset_x_left = 10.13 # C
        self.ak_x = self.width/2 - ak_offset_x_left + AK_X_FUDGE
        ak_offset_x_right = 4.13 # D
        self.ak_width = ak_offset_x_left + ak_offset_x_right
        north_star_offset_x = 4.31 # E
        north_star_offset_y = 3.0 # F
        self.ref_height = 10.0  # G
        self.ref_y = self.height/2 - self.ref_height/2
        self.north_star = (self.width/2 + ak_offset_x_right + north_star_offset_x,
                           self.ref_y - north_star_offset_y)
        star_offset_y[6] = 1.5 # H
        star_offset_y[5] = 2.5 # J
        star_offset_x[6] = 1.0 # M
        star_offset_x[5] = 1.88 # N
        star_offset_x[4] = 1.88 # P
        star_offset_x[3] = 1.88 # P
        star_offset_x[2] = 1.75 # Q
        star_offset_x[1] = 2.56 # R
        star_offset_x[0] = 2.88 # S
        star_offset_y[4] = 2.69 # T
        star_offset_y[3] = 1.63 # U
        star_offset_y[2] = 1.31 # V
        star_offset_y[1] = 0.69 # W
        star_offset_y[0] = 2.13 # X
        ak_offset_y = 6.5 # Y
        self.ak_height = 3.0 # AA
        self.ak_y = self.height/2 - ak_offset_y - self.ak_height
        self.corner_radius = 1.5 # AB
        self.north_star_arm = 1.5 # AC
        self.star_arm = 0.94 # AD
        
        self.ak_series = 'D'
        self.ak_spacing = 0.5
        self.ref_x = 10.0
        self.ref_width = self.width - self.ref_x - self.border_width * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['E', 'D', 'C', 'B'])

        self.stars = [[None, None] for i in range(0, 7)]
        self.stars[0][1] = self.height/2 + star_offset_y[0]
        for i in range(1, 5):
            self.stars[i][1] = self.stars[i-1][1] + star_offset_y[i]
        self.stars[6][1] = self.height/2 + self.ref_height/2 + star_offset_y[6]
        self.stars[5][1] = self.stars[6][1] + star_offset_y[5]
        self.stars[4][0] = self.width/2 - star_offset_x[4]
        self.stars[3][0] = self.stars[4][0]
        for i in range(2, -1, -1):
            self.stars[i][0] = self.stars[i+1][0] - star_offset_x[i]
        self.stars[5][0] = self.width/2 + star_offset_x[5]
        self.stars[6][0] = self.stars[5][0] + star_offset_x[6]
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ak-blank'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect(
            (self.border_width/2, self.border_width/2),
            (self.width - self.border_width, self.height - self.border_width),
            self.corner_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        ak_skew = self.drawing.g(transform='skewX(-10)')
        ak_skew.add(render_text(
            self.drawing, 'ALASKA', self.ak_x, self.ak_y, self.ak_width,
            self.ak_height, self.ak_series, self.fg, spacing=self.ak_spacing))
        bg_g.add(ak_skew)

        bg_g.add(self.add_star(self.north_star, self.north_star_arm / self.star_arm))
        for center in self.stars:
            bg_g.add(self.add_star(center))
        
        return bg_g

    def add_star(self, center, scale=None):
        self.add_star_to_defs()
        star = self.drawing.use('#' + STAR_ID,
                                transform='translate({} {})'.format(center[0], center[1]))
        if scale is not None:
            star['transform'] += ' scale({})'.format(scale)
        return star

    def add_star_to_defs(self):
        for e in self.drawing.defs.elements:
            if e['id'] == STAR_ID:
                return
        
        points = []
        r = self.star_arm
        phi = -math.pi/2
        while phi < 2 * math.pi - math.pi / 2:
            points.append(cmath.rect(r, phi))
            phi += 2 * math.pi / 5

        indents = []
        first_indent = intersect_lines(points[0], points[2], points[1], points[4])
        r, phi = cmath.polar(first_indent)
        limit = 2 * math.pi + phi
        while phi < limit:
            indents.append(cmath.rect(r, phi))
            phi += 2 * math.pi / 5
            
        star = self.drawing.path(fill=self.fg, id=STAR_ID)
        star.push('M', (indents[-1].real, indents[-1].imag))
        for p, i in zip(points, indents):
            star.push('L', (p.real, p.imag))
            star.push('L', (i.real, i.imag))
        star.push('Z')

        self.drawing.defs.add(star)
