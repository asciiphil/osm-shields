#!/usr/bin/python

# http://www.virginiadot.org/business/resources/TED/final_MUTCD/Standard_Highway_Signs_Book.pdf#page=123

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:VA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:VA:SECONDARY'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:VA'] = lambda m,r: VAStateShield(m, r, style).make_shield()
        network_registry['US:VA:SECONDARY'] = lambda m,r: VASecondaryShield(m, r, style).make_shield()

class VAStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(VAStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN

        self.stroke_width = 0.5
        if len(self.ref) <= 2:
            self.width = 24.0
            self.l_arc_x = self.stroke_width + 23.0 - 8.5013
            self.l_arc_y = self.stroke_width + 8.8028
            self.l_arc_radius = 14.5
        else:
            self.width = 30.0
            self.l_arc_x = self.stroke_width + 29.0 - 12.5
            self.l_arc_y = self.stroke_width + 6.6181
            self.l_arc_radius = 16.5
        self.height = 24.0
        self.ref_y = 4.0
        self.ref_height = 12.0
        self.ref_series = 'C'
        self.u_arc_x = 3.25 + self.stroke_width
        self.u_arc_y = 3.25 + self.stroke_width
        self.u_arc_radius = 3.25
        self.corner_radius = 1.5

        self.ref_x = self.stroke_width * 2
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        
    @property
    def blank_key(self):
        return 'us_va-{}'.format(int(self.width))
        
    def make_blank_shield(self):
        background_g = self.drawing.g()

        if self.sign:
            background_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            
        ul_arc_r = (self.u_arc_x, self.stroke_width/2)
        ur_arc_l = (self.width - ul_arc_r[0], ul_arc_r[1])
        ul_arc_l = (self.stroke_width/2, self.u_arc_y)
        ur_arc_r = (self.width - ul_arc_l[0], ul_arc_l[1])
        ll_arc_l = (self.stroke_width/2, self.l_arc_y)
        lr_arc_r = (self.width - ll_arc_l[0], ll_arc_l[1])
        lower_point = (self.width/2, self.height - self.stroke_width/2)
        
        outline = self.drawing.path(
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
        background_g.add(outline)
    
        outline.push('M', ul_arc_r)
        outline.push_arc(ul_arc_l, 0, self.u_arc_radius + self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('V', ll_arc_l[1])
        outline.push_arc(lower_point, 0, self.l_arc_radius + self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc(lr_arc_r, 0, self.l_arc_radius + self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('V', ur_arc_r[1])
        outline.push_arc(ur_arc_l, 0, self.u_arc_radius + self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('Z')
        
        return background_g
    
class VASecondaryShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(VASecondaryShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE

        if self.guide:
            self.stroke_width = 0.5
        else:
            self.stroke_width = 1.0
        if len(self.ref) <= 3:
            self.ref_y = 7.0
            self.ref_height = 10.0
        else:
            self.ref_y = 8.0
            self.ref_height = 8.0
        if self.guide:
            self.ref_height += 1
            self.ref_y -= 0.5
        self.width = 24.0
        self.height = 24.0
        self.corner_radius = 1.5
        self.ref_series = 'B'
        self.circle_radius = 11.0

        self.ref_x = self.stroke_width * 2
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        
    @property
    def blank_key(self):
        return 'us_va-secondary'
    
    def make_blank_shield(self):
        background_g = self.drawing.g()

        if self.sign:
            background_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
        
        outline = self.drawing.circle(
            (self.width/2, self.height/2),
            self.circle_radius + self.stroke_width/2,
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
        background_g.add(outline)
    
        return background_g
