#!/usr/bin/python

# http://www.robertandjennifer.com/M1i100.pdf

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:IL'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:IL'] = lambda m,r: ILStateShield(m, r).make_shield()

class ILStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(ILStateShield, self).__init__(modifier, ref)

        if len(ref) <= 2:
            self.width = 24.0
            self.ref_series = 'D'
        else:
            self.width = 30.0
            if len(ref) <= 3:
                if '1' in self.ref:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
            else:
                if '1' in self.ref:
                    self.ref_series = 'C'
                else:
                    self.ref_series = 'B'
        self.height = 24.0
        self.ref_height = 12.0

        # All following dimensions are assumed/estimated.
        self.border = 0.4
        self.stroke_width = 0.6
        self.corner_radius = 1.5

        self.ref_x = self.border * 2 + self.stroke_width
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 10.0
        self.il_height = 3.0
        self.il_series = 'D'
        self.il_width = 15.5
        self.il_x = (self.width - self.il_width)/2

        self.il_y = 7.0 - self.il_height
        self.ref_y = 7.0 + 13.0 - self.ref_height
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_il-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_ul = (self.border + self.stroke_width/2, self.border + self.stroke_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius - bg_ul[0]
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width))

        bg_g.add(render_text(
            self.drawing, 'ILLINOIS', self.il_x, self.il_y, self.il_width,
            self.il_height, self.il_series, color=self.fg))
        
        return bg_g
