#!/usr/bin/python

# I couldn't find any official documentation.  SVGs taken from Wikipedia.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:AR'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:AR'] = lambda m,r: ARStateShield(m, r, sign=True).make_shield()
    else:
        network_registry['US:AR'] = lambda m,r: ARStateShield(m, r).make_shield()

class ARStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=False):
        super(ARStateShield, self).__init__(modifier, ref)

        self.sign = sign
        self.airport = self.ref == '980'
        
        ref_pieces = list(split_num_char(self.ref))
        if len(ref_pieces) > 1:
            self.ref = ''.join([str(p) for p in ref_pieces[:-1]])
            self.suffix = ref_pieces[-1]
        else:
            self.suffix = ''
            
        if (len(self.ref) + len(self.suffix) <= 2) or self.airport:
            self.width = 24.0
            self.outline = AR_OUTLINE_2
        else:
            self.width = 30.0
            self.outline = AR_OUTLINE_3
        self.height = 24.0
        self.border_width = 0.5
        self.corner_radius = 1.5

        if self.airport:
            self.ref_x = 4.5
            self.ref_width = 13.5
            self.ref_y = 17.0
            self.ref_height = 4.875

            self.airport_x = 1.5
            self.airport_width = 19.5
            self.airport_y = 2.5
            self.airport_height = 3.375
            self.airport_series = 'D'
        else:
            if self.suffix != '':
                ref_right = 9.0
            else:
                ref_right = 6.0
            self.ref_x = 2.0
            self.ref_y = 6.5
            self.ref_height = 12.0
            self.ref_width = self.width - self.ref_x - ref_right
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])

        if self.suffix != '':
            self.suffix_x = self.width - ref_right
            self.suffix_y = 4.0
            self.suffix_width = 6.0
            self.suffix_height = 6.0
            self.suffix_series = series_for_text(self.suffix, self.suffix_height, self.suffix_width, ['D', 'C', 'B'])

        if self.airport:
            self.fg = COLOR_WHITE
            self.bg = COLOR_BLUE
        else:
            self.fg = COLOR_BLACK
            self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        if self.airport:
            return 'us_al-airport'
        else:
            return 'us_al-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(self.drawing.path(
                d=self.outline, fill=self.bg,
                transform='matrix({0} 0 0 {0} {1} {1})'.format(
                    (self.width - self.border_width * 2) / (self.width - self.border_width),
                    self.border_width/2)))
            return bg_g
        else:
            return self.drawing.path(d=self.outline, stroke=self.fg, fill=self.bg,
                                     stroke_width=self.border_width, stroke_linejoin='round')

    def make_ref(self):
        ref_g = self.drawing.g()

        if self.airport:
            ref_g.add(render_text(
                self.drawing, 'AIRPORT', self.airport_x, self.airport_y,
                self.airport_width, self.airport_height, self.airport_series,
                color=self.fg))
            ref_g.add(self.drawing.path(d=AIRPLANE, fill=self.fg))
            
        ref_g.add(render_text(
            self.drawing, self.ref, self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            color=self.fg))

        if self.suffix != '':
            ref_g.add(render_text(
                self.drawing, self.suffix, self.suffix_x, self.suffix_y,
                self.suffix_width, self.suffix_height, self.suffix_series,
                color=self.fg))
            
        return ref_g

AR_OUTLINE_2 = 'M 3.5249,22.021 V 20.293 H 1.21887 V 7.491 L 0.770748,3.975 C 0.524283,2.042 0.315727,0.413 0.30729,0.355 L 0.291951,0.251 H 21.331051 L 21.466351,0.317 C 21.681951,0.422 21.780251,0.557 21.844651,0.833 21.892551,1.038 21.895651,1.089 21.870351,1.251 21.854551,1.352 21.826551,1.554 21.808051,1.7 21.781551,1.91 21.748751,2.026 21.650651,2.258 21.532951,2.536 21.526351,2.564 21.517851,2.829 L 21.508951,3.022 21.610651,3.184 21.712251,3.261 H 23.741051 V 4.614 L 23.745751,4.661 C 23.681451,4.842 23.668851,4.917 23.668851,5.122 L 23.505751,5.622 23.236851,6.329 C 22.925651,7.033 22.884351,7.144 22.867551,7.317 22.841751,7.582 22.657251,8.457 22.560051,8.774 22.516551,8.916 22.436451,9.216 22.381951,9.44 22.279251,9.863 22.292651,9.919 21.992351,10.386 21.920551,10.497 21.764251,10.743 21.763751,10.87 21.763551,10.906 21.696251,11.16 21.514551,11.537 21.332251,11.915 21.168651,12.292 21.149551,12.376 21.109151,12.556 20.463951,13.795 19.974151,14.634 19.695151,15.112 19.618651,15.272 19.341951,15.955 19.144251,16.443 19.013051,16.805 18.989051,16.93 18.968051,17.04 18.933551,17.211 18.912351,17.31 18.883151,17.448 18.873651,17.676 18.873251,18.252 L 18.872751,18.927 18.961251,19.297 C 19.009851,19.454 19.074051,19.688 19.103851,19.817 19.133651,19.946 19.175051,20.173 19.195951,20.217 19.228051,20.285 19.232651,20.411 19.226251,21.543 L 19.218651,22.789 19.133451,23.057 C 19.086651,23.157 18.997251,23.303 18.934851,23.38 18.872551,23.458 18.821551,23.53 18.821551,23.54 18.821551,23.566 18.752651,23.638 18.727251,23.639 18.715651,23.639 18.677351,23.664 18.642151,23.694 18.578951,23.748 18.481651,23.748 11.051451,23.749 H 3.524851 Z'
AR_OUTLINE_3 = 'M 3.5249,22.021 V 20.293 H 1.21887 V 7.491 L 0.770748,3.975 C 0.524283,2.042 0.315727,0.413 0.30729,0.355 L 0.291951,0.251 H 27.3311 L 27.4664,0.317 C 27.682,0.422 27.7803,0.557 27.8447,0.833 27.8926,1.038 27.8957,1.089 27.8704,1.251 27.8546,1.352 27.8266,1.554 27.8081,1.7 27.7816,1.91 27.7488,2.026 27.6507,2.258 27.533,2.536 27.5264,2.564 27.5179,2.829 L 27.509,3.022 27.6107,3.184 27.7123,3.261 H 29.7411 V 4.614 L 29.7458,4.661 C 29.6815,4.842 29.6689,4.917 29.6689,5.122 L 29.5058,5.622 29.2369,6.329 C 28.9257,7.033 28.8844,7.144 28.8676,7.317 28.8418,7.582 28.6573,8.457 28.5601,8.774 28.5166,8.916 28.4365,9.216 28.382,9.44 28.2793,9.863 28.2927,9.919 27.9924,10.386 27.9206,10.497 27.7643,10.743 27.7638,10.87 27.7636,10.906 27.6963,11.16 27.5146,11.537 27.3323,11.915 27.1687,12.292 27.1496,12.376 27.1092,12.556 26.464,13.795 25.9742,14.634 25.6952,15.112 25.6187,15.272 25.342,15.955 25.1443,16.443 25.0131,16.805 24.9891,16.93 24.9681,17.04 24.9336,17.211 24.9124,17.31 24.8832,17.448 24.8737,17.676 24.8733,18.252 L 24.8728,18.927 24.9613,19.297 C 25.0099,19.454 25.0741,19.688 25.1039,19.817 25.1337,19.946 25.1751,20.173 25.196,20.217 25.2281,20.285 25.2327,20.411 25.2263,21.543 L 25.2187,22.789 25.1335,23.057 C 25.0867,23.157 24.9973,23.303 24.9349,23.38 24.8726,23.458 24.8216,23.53 24.8216,23.54 24.8216,23.566 24.7527,23.638 24.7273,23.639 24.7157,23.639 24.6774,23.664 24.6422,23.694 24.579,23.748 3.524851,23.749 3.524851,23.749 Z'
AIRPLANE = 'M 16.224609 6.5175781 C 15.710902 6.6866189 15.197302 6.7347755 14.683594 7.0273438 C 14.35311 7.3620633 13.910944 8.4711907 13.521484 9.21875 C 12.638242 9.3101 12.291194 9.3425894 8.1601562 11.794922 L 8.3085938 11.267578 L 7.84375 11.267578 L 7.4628906 11.710938 L 5.2890625 10.486328 L 4.3183594 11.078125 L 4.7617188 11.289062 L 6.0703125 13.0625 L 4.4238281 14.75 L 5.3730469 14.644531 L 6.5976562 13.294922 C 8.8770601 13.458917 9.3365429 13.193438 10.820312 12.914062 L 7.1035156 16.228516 L 9.1308594 15.933594 L 11.980469 13.189453 C 14.518323 11.96671 13.654726 12.547647 14.619141 11.478516 C 15.611371 11.001298 17.569027 10.517649 17.597656 9.6425781 C 17.397757 9.2302504 16.667806 9.3467647 16.203125 9.1992188 L 16.730469 8.609375 L 15.925781 8.2460938 L 15.380859 8.3320312 L 16.224609 6.5175781 z M 12.771484 9.4394531 L 14.082031 9.5371094 L 14.015625 9.9589844 L 12.689453 9.6308594 L 12.771484 9.4394531 z M 12.623047 9.6914062 L 12.896484 9.7773438 L 13.945312 10.085938 C 13.647381 10.328703 13.376127 10.514804 12.947266 10.800781 L 12.486328 10.285156 L 12.498047 9.8261719 L 12.623047 9.6914062 z M 12.416016 10.328125 L 12.845703 10.855469 L 12.009766 11.267578 L 11.640625 10.761719 L 12.416016 10.328125 z M 11.546875 10.802734 L 11.916016 11.320312 L 11.101562 11.720703 L 10.785156 11.216797 L 11.546875 10.802734 z M 10.6875 11.273438 L 11.003906 11.777344 L 10.212891 12.169922 L 9.9238281 11.667969 L 10.6875 11.273438 z'

