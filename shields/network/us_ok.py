#!/usr/bin/python

# http://www.okladot.state.ok.us/traffic/traffic2009/trf_std_2009-120.pdf
# http://www.okladot.state.ok.us/traffic/traffic2009/trf_std_2009-121.pdf

import cmath
import math
import re
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:OK'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:OK:TURNPIKE'] = lambda m,r: USGenericStateShield('TOLL', re.sub(' TURNPIKE$', '', r)).make_shield()
    else:
        network_registry['US:OK'] = lambda m,r: OKStateShield(m, r).make_shield()
        network_registry['US:OK:TURNPIKE'] = lambda m,r: OKTurnpikeShield(m, r).make_shield()

class OKStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(OKStateShield, self).__init__(modifier, ref)

        if len(self.ref) <= 2:
            self.width = 24.0
            self.ok_x = 2.0
            self.ok_lower_right_height = 6.5
            self.ok_right_diagonal_height = 4.0
            self.ok_right_offset = 2.3
            self.ok_height = 11.7
            self.ok_top_width = 18.8
            self.ok_panhandle_width = 6.0
            self.ok_lower_left_height = 5.0
            self.ok_south_border_width = 14.0
            self.ok_south_border_height = 3.3
            self.ref_series = 'D'
        else:
            self.width = 30.0
            self.ok_x = 2.5
            self.ok_lower_right_height = 8.1
            self.ok_right_diagonal_height = 5.1
            self.ok_right_offset = 3.8
            self.ok_height = 14.7
            self.ok_top_width = 23.5
            self.ok_panhandle_width = 7.5
            self.ok_lower_left_height = 6.4
            self.ok_south_border_width = 17.5
            self.ok_south_border_height = 4.2
            if len(self.ref) == 3 and '1' in self.ref:
                self.ref_series = 'D'
            elif len(self.ref) == 3:
                self.ref_series = 'C'
            else:
                self.ref_series = 'B'
        self.height = 24.0
        self.margin = 0.25
        self.border_width = 0.375
        self.corner_radius = 1.5
        self.ok_y = 3.0
        ref_offset_y = 6.0
        self.ref_y = self.ok_y + ref_offset_y
        self.ref_height = 12.0
        self.let_height = 8.0

        self.ok_stroke_width = 0.3
        self.ok_y = 3.0

        self.ref_x = self.margin * 2 + self.border_width
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ok-{}'.format(int(self.width))
    
    def make_size_func(self):
        def size_func(letter):
            if letter in set('0123456789'):
                return (self.ref_height, self.ref_series, 1.0)
            else:
                return (self.let_height, self.ref_series, 1.0)
        return size_func
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_ul = (self.margin + self.border_width/2, self.margin + self.border_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius - bg_ul[0]
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width = self.border_width))

        ul = (self.ok_x, self.ok_y)
        ur = (self.width - self.ok_right_offset, self.ok_y)
        l3 = (self.ok_x + self.ok_panhandle_width, self.ok_y + self.ok_height - self.ok_south_border_height)
        l2 = (l3[0], l3[1] - self.ok_lower_left_height)
        l1 = (ul[0], l2[1])
        r3 = (self.width - self.ok_x, self.ok_y + self.ok_height)
        r2 = (r3[0], r3[1] - self.ok_lower_right_height)
        r1 = (ur[0], r2[1] - self.ok_right_diagonal_height)

        ok = self.drawing.path(stroke=self.fg, fill='none', stroke_width=self.ok_stroke_width)
        bg_g.add(ok)
        ok.push('M', r1)
        ok.push('V', ur[1])
        ok.push('H', ul[0])
        ok.push('V', l1[1])
        ok.push('H', l2[0])
        ok.push('V', l3[1])
        for x, y in OK_SOUTH_BORDER_POINTS:
            ok.push('L', (l3[0] + x * self.ok_south_border_width,
                          l3[1] + y * self.ok_south_border_height))
        ok.push('L', r3)
        ok.push('V', r2[1])
        ok.push('Z')
        
        return bg_g

    def make_ref(self):
        ref_g = self.drawing.g()

        ref_width = text_length(self.ref, self.ref_height, self.ref_series,
                                size_func=self.make_size_func())
        if ref_width > self.ref_width:
            ref_height = self.ref_height / ref_width * self.ref_width
            ref_width = self.ref_width
        else:
            ref_height = self.ref_height
        halo_width = ref_width + 0.6
        halo_height = ref_height + 0.6
        ref_g.add(self.drawing.rect(
            (self.ref_x + self.ref_width/2 - halo_width/2,
             self.ref_y + self.ref_height/2 - halo_height/2),
            (halo_width, halo_height), fill=self.bg))
        
        ref_g.add(render_text(
            self.drawing, self.ref, self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            color=self.fg, size_func=self.make_size_func(),
            vertical_center=True))
        
        return ref_g
    
class OKTurnpikeShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(OKTurnpikeShield, self).__init__(modifier, ref)

        if self.ref.endswith(' TURNPIKE'):
            self.ref = self.ref[:-len(' TURNPIKE')]
            
        self.width = 24.0
        self.height = 24.0

        self.margin = 0.375
        self.border_width = 0.625
        self.corner_radius = 1.5

        self.circle_stroke = 0.5
        self.circle_radius = 6.0

        self.center_angle = 14.40 / 180.0 * math.pi
        self.road_angle = 57.32 / 180.0 * math.pi
        self.outer_angle = 108.64 / 180.0 * math.pi
        self.dash_points = [0.25, (0.75, 1.75), (2.5, 3.75), 4.75]

        self.text_radius = 7.0
        self.ref_height = 2.5
        self.ref_series = 'C'
        self.ref_spacing = 0.4
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        self.circle_fill = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_ok-turnpike'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g(transform='translate({} {})'.format(self.width/2, self.height/2))

        border_offset = self.margin + self.border_width/2
        bg_ul = (border_offset - self.width/2, border_offset - self.height/2)
        bg_dims = (self.width - border_offset * 2, self.height - border_offset * 2)
        bg_radius = self.corner_radius - border_offset
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        outer_point = cmath.rect(self.circle_radius, math.pi/2 + self.outer_angle/2)
        road_point = cmath.rect(self.circle_radius, math.pi/2 + self.road_angle/2)
        center_angle = math.pi/2 + self.center_angle/2
        center_point = cmath.rect(self.circle_radius, math.pi/2 + self.center_angle/2)
        dash_0 = cmath.rect(self.dash_points[0], center_angle)
        dash_n = cmath.rect(self.dash_points[-1], center_angle)
        
        circle_fill = self.drawing.path(fill=self.circle_fill)
        bg_g.add(circle_fill)
        circle_fill.push('M', (0, 0))
        circle_fill.push('L', (outer_point.real, outer_point.imag))
        circle_fill.push_arc((-outer_point.real, outer_point.imag), 0,
                             self.circle_radius, angle_dir='+', absolute=True)
        circle_fill.push('Z')

        road = self.drawing.path(fill=self.fg, fill_rule='evenodd')
        bg_g.add(road)
        road.push('M', (0, 0))
        road.push('L', (road_point.real, road_point.imag))
        road.push_arc((center_point.real, center_point.imag), 0,
                      self.circle_radius, large_arc=False,
                      angle_dir='-', absolute=True)
        road.push('L', (dash_n.real, dash_n.imag))
        road.push('H', -dash_n.real)
        road.push('L', (-center_point.real, center_point.imag))
        road.push_arc((-road_point.real, road_point.imag), 0,
                      self.circle_radius, large_arc=False,
                      angle_dir='-', absolute=True)
        road.push('Z')

        road.push('M', (0, 0))
        road.push('L', (dash_0.real, dash_0.imag))
        road.push('H', -dash_0.real)
        road.push('Z')

        for start, end in self.dash_points[1:-1]:
            s = cmath.rect(start, center_angle)
            e = cmath.rect(end, center_angle)
            road.push('M', (s.real, s.imag))
            road.push('H', -s.real)
            road.push('L', (-e.real, e.imag))
            road.push('H', e.real)
            road.push('Z')
        
        bg_g.add(self.drawing.circle(
            (0, 0), self.circle_radius + self.circle_stroke/2, stroke=self.fg,
            fill='none', stroke_width=self.circle_stroke))

        bg_g.add(render_text_along_arc(
            self.drawing, 'TURNPIKE', 0j,
            self.text_radius, 180, self.ref_height, self.ref_series, self.fg,
            spacing=self.ref_spacing, invert=True))
        
        return bg_g

    def make_ref(self):
        return render_text_along_arc(
            self.drawing, self.ref, self.width/2 + self.height/2 * 1j,
            self.text_radius, 0, self.ref_height, self.ref_series, self.fg,
            spacing=self.ref_spacing)

    
OK_SOUTH_BORDER_POINTS = [
    (0.0,0.0),
    (0.015402,0.010145),
    (0.030169,0.077716),
    (0.039565,0.15878),
    (0.050975,0.21621),
    (0.065741,0.23647),
    (0.079165,0.19931),
    (0.085877,0.19257),
    (0.092587,0.21283),
    (0.10198,0.22973),
    (0.11004,0.18243),
    (0.11407,0.16891),
    (0.12749,0.17905),
    (0.13488,0.22634),
    (0.13488,0.22634),
    (0.13488,0.35134),
    (0.13890,0.37160),
    (0.15501,0.44253),
    (0.17581,0.44592),
    (0.19058,0.46280),
    (0.20669,0.52361),
    (0.21877,0.54050),
    (0.23421,0.53712),
    (0.24293,0.51348),
    (0.25635,0.52361),
    (0.27447,0.59117),
    (0.28321,0.59454),
    (0.29259,0.53036),
    (0.29797,0.51348),
    (0.30938,0.53712),
    (0.32549,0.53712),
    (0.33757,0.54388),
    (0.33757,0.54388),
    (0.34092,0.59454),
    (0.35502,0.69926),
    (0.36442,0.72967),
    (0.36710,0.78710),
    (0.37851,0.82426),
    (0.38858,0.82087),
    (0.39998,0.76345),
    (0.40871,0.69926),
    (0.41542,0.67899),
    (0.42683,0.70602),
    (0.42683,0.70602),
    (0.43690,0.74994),
    (0.45300,0.83439),
    (0.46375,0.86140),
    (0.47583,0.85803),
    (0.49193,0.79385),
    (0.49932,0.78371),
    (0.50468,0.80060),
    (0.50468,0.80060),
    (0.50267,0.86816),
    (0.50133,0.95261),
    (0.51072,0.97626),
    (0.51945,0.94586),
    (0.52481,0.88168),
    (0.53355,0.82426),
    (0.53757,0.74318),
    (0.54427,0.71615),
    (0.55233,0.73642),
    (0.56441,0.79385),
    (0.57381,0.82426),
    (0.58388,0.83776),
    (0.59529,0.81411),
    (0.60335,0.77695),
    (0.61208,0.79047),
    (0.62214,0.86816),
    (0.63354,0.90532),
    (0.65033,0.93235),
    (0.66174,0.95937),
    (0.67918,0.92221),
    (0.69396,0.87492),
    (0.70872,0.82426),
    (0.72684,0.80736),
    (0.72684,0.80736),
    (0.74362,0.81750),
    (0.77249,0.79385),
    (0.78590,0.75669),
    (0.79731,0.72291),
    (0.80873,0.75331),
    (0.82014,0.78371),
    (0.83691,0.78710),
    (0.85235,0.78371),
    (0.85906,0.72629),
    (0.87516,0.66886),
    (0.88456,0.70265),
    (0.90538,0.80060),
    (0.92014,0.80736),
    (0.93558,0.89181),
    (0.94967,0.94248),
    (0.96242,0.98302),
    (0.97920,0.99315),
    (1.0000,1.0000),
]
