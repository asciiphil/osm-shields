#!/usr/bin/python

# http://sp.mdot.ms.gov/Roadway%20Design/Lists/Standard_Drawings/standards/tif/english/0221.pdf

from ..us_state import USGenericStateShield, STYLE_ELLIPSE
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MS'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:MS'] = lambda m,r: USGenericStateShield(m, r, STYLE_ELLIPSE).make_shield()
