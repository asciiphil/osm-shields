#!/usr/bin/python

# https://mdotcf.state.mi.us/public/tands/Details_Web/route%20marker.pdf#page=9

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MI'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:MI'] = lambda m,r: MIStateShield(m, r, sign=True).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:MI'] = lambda m,r: MIStateShield(m, r, guide=True).make_shield()
    else:
        network_registry['US:MI'] = lambda m,r: MIStateShield(m, r).make_shield()

class MIStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=False, guide=False):
        super(MIStateShield, self).__init__(modifier, ref)
        assert not (sign and guide)
        self.sign = sign
        self.guide = guide

        if self.guide:
            if len(ref) <= 2:
                self.height = 24.0     # A
                self.width = 24.0      # B
                self.ref_series = 'D'
                self.ref_y = 7.0       # D
            else:
                self.height = 27.0     # A
                self.width = 36.0      # B
                if '1' in self.ref:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
                self.ref_y = 8.5       # D
            self.ref_height = 10.0     # C
        else:
            if len(ref) <= 2:
                self.width = 24.0      # A
                self.ref_series = 'D'  # H
            else:
                self.width = 30.0      # A
                if '1' in ref:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
            self.height = 24.0         # B
            self.ref_height = 8.0      # H
            ref_y_offset = 2.625       # K
            self.ref_y = self.height/2 - ref_y_offset

        self.stroke_width = 0.688 # C

        self.m_y = 4.625          # D
        self.corner_radius = 1.5  # R

        border_offset = math.sqrt(2 * self.stroke_width**2)
        bound_ul = border_offset + self.height/2 * 1j
        bound_lr = self.width/2 + (self.height - bound_ul.real) * 1j
        ref_bottom_l = 0 + (self.ref_y + self.ref_height) * 1j
        ref_bottom_r = self.width + ref_bottom_l.imag * 1j
        ref_ll = intersect_lines(bound_ul, bound_lr, ref_bottom_l, ref_bottom_r)
        
        self.ref_x = ref_ll.real
        self.ref_width = self.width - self.ref_x * 2

        ###
        # "M"
        self.m_height = 3.5              # A/H
        self.m_half_width = 2.375        # B
        self.m_upper_serif_width = 1.188 # C
        self.m_lower_serif_width = 1.5   # D
        self.m_stroke_width = 0.875      # E
        self.m_serif_offset = 0.313      # F
        self.m_center_drop = 1.25        # G
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_mi-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            outline = self.drawing.path(fill=self.bg)
            border_offset = self.stroke_width
        else:
            outline = self.drawing.path(stroke=self.fg, fill=self.bg,
                                        stroke_width=self.stroke_width)
            border_offset = self.stroke_width/2
        outline.push('M', (self.width/2, border_offset))
        outline.push('L', (self.width - border_offset, self.height/2))
        outline.push('L', (self.width/2, self.height - border_offset))
        outline.push('L', (border_offset, self.height/2))
        outline.push('Z')
        bg_g.add(outline)

        if not self.guide:
            bg_g.add(self.make_m())
        
        return bg_g

    def make_m(self):
        x = self.width/2 - self.m_half_width
        y = self.m_y

        midpoint_y = self.m_center_drop + math.sqrt(2 * self.m_stroke_width**2)/2
        
        m_path = self.drawing.path(stroke=self.fg, stroke_width=self.m_stroke_width,
                                   fill='none',
                                   transform='translate({} {})'.format(x, y))
        m_path.push('M', (0, self.m_height - self.m_stroke_width/2))
        m_path.push('h', self.m_lower_serif_width)
        m_path.push('m', (-self.m_lower_serif_width/2, 0))
        m_path.push('v', -(self.m_height - self.m_stroke_width))
        m_path.push('m', (-self.m_lower_serif_width/2, 0))
        m_path.push('h', self.m_upper_serif_width)
        m_path.push('L', (self.m_half_width, midpoint_y))
        m_path.push('L', (self.m_half_width * 2 - self.m_upper_serif_width, self.m_stroke_width/2))
        m_path.push('h', self.m_upper_serif_width)
        m_path.push('m', (-self.m_lower_serif_width/2, 0))
        m_path.push('v', (self.m_height - self.m_stroke_width))
        m_path.push('m', (self.m_lower_serif_width/2, 0))
        m_path.push('h', -self.m_lower_serif_width)
        
        return m_path
    
