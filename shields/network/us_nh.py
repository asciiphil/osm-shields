#!/usr/bin/python

# https://www.nh.gov/dot/org/projectdevelopment/highwaydesign/standardplans/documents/2010_sg_1.pdf

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:NH'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:NH'] = lambda m,r: NHStateSHield(m, r, sign=True).make_shield()
    else:
        network_registry['US:NH'] = lambda m,r: NHStateSHield(m, r).make_shield()

class NHStateSHield(ShieldBase):
    def __init__(self, modifier, ref, sign=False):
        super(NHStateSHield, self).__init__(modifier, ref)
        self.sign = sign

        if len(ref) <= 2 or ref[-1] in '0123456789' or self.modifier != '':
            self.ref = ref
            self.suffix = ''
        else:
            self.ref = ref[:-1]
            self.suffix = ref[-1]

        self.width = 24.0          # A
        self.height = 24.0         # B
        if self.suffix == '' and self.modifier == '':
            self.ref_height = 12.0 # C
        else:
            self.ref_height = 9.0  # F
        self.stroke_width = 0.5    # D & I
        self.corner_radius = 1.5   # E
        self.suffix_height = 6.0   # G
        self.modifier_height = 5.0 # H

        self.modifier_x = 2.75 # one stroke-width to the right of the OMM's mouth
        
        if self.suffix == '':
            self.ref_x = 4.5  # one stroke-width to the right of the OMM's neck
        else:
            self.ref_x = 2.75 # see self.modifier_x
            self.suffix_x = 4.5  # see self.ref_x
        if self.modifier != '':
            spacing = (self.height - self.stroke_width * 4 - self.modifier_height - self.ref_height) / 3
            self.modifier_y = self.stroke_width * 2 + spacing
            self.ref_y = self.modifier_y + self.modifier_height + spacing
        elif self.suffix != '':
            spacing = (self.height - self.stroke_width * 4 - self.suffix_height - self.ref_height) / 3
            self.ref_y = self.stroke_width * 2 + spacing
            self.suffix_y = self.ref_y + self.ref_height + spacing
        else:
            self.ref_y = self.height/2 - self.ref_height/2
            
        self.ref_width = self.width - self.ref_x - self.stroke_width * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
        if self.modifier != '':
            self.modifier_width = self.width - self.modifier_x - self.stroke_width * 2
            self.modifier_series = series_for_text(self.modifier, self.modifier_height, self.modifier_width, ['D', 'C', 'B'])
        if self.suffix != '':
            self.suffix_width = self.width - self.suffix_x - self.stroke_width * 2
            self.suffix_series = series_for_text(self.suffix, self.suffix_height, self.suffix_width, ['D', 'C', 'B'])
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_nh'

    def make_shield(self):
        return self.make_base_shield()
        
    def make_blank_shield(self):
        outline = self.drawing.path(d=OMM_PATH, stroke=self.fg, fill=self.bg,
                                    stroke_width=self.stroke_width)
        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(outline)
            return bg_g
        else:
            return outline

    def make_ref(self):
        ref_g = self.drawing.g()
    
        if self.modifier != '':
            ref_g.add(
                render_text(
                    self.drawing, self.modifier, self.modifier_x, self.modifier_y,
                    self.modifier_width, self.modifier_height, self.modifier_series,
                    color=self.fg))
        if self.suffix != '':
            ref_g.add(
                render_text(
                    self.drawing, self.suffix, self.suffix_x, self.suffix_y,
                    self.suffix_width, self.suffix_height, self.suffix_series,
                    color=self.fg))
        ref_g.add(
            render_text(
                self.drawing, self.ref, self.ref_x, self.ref_y,
                self.ref_width, self.ref_height, self.ref_series,
                color=self.fg))
                
        return ref_g

OMM_PATH = 'M 23.199913,0.26668418 23.303926,0.2773508 23.403992,0.30935075 23.496271,0.35201742 23.577135,0.42668393 23.643384,0.50135044 23.69267,0.59735029 23.723074,0.69335024 23.733316,0.8000167 V 23.199983 L 23.723074,23.30665 23.69267,23.402649 23.643384,23.498649 23.577135,23.573316 23.496271,23.647983 23.403992,23.69065 23.303926,23.722649 23.199913,23.733316 H 1.8966183 L 1.7473185,23.637316 1.6260118,23.541316 1.514029,23.43465 1.4207047,23.317316 1.3367149,23.17865 1.2620597,23.039983 1.2060737,22.879984 1.1594115,22.741317 1.1220839,22.591985 1.0847563,22.431985 1.1500769,21.855986 1.3180565,21.322653 1.5420221,20.82132 1.8033153,20.341321 2.0832775,19.850655 2.3632292,19.370655 2.5871948,18.869323 2.7458398,18.33599 2.8204949,17.759992 2.7831673,17.141326 2.9698053,16.90666 3.1191157,16.639993 3.2310985,16.362661 3.3244121,16.085328 3.3990674,15.786661 3.4643987,15.477328 3.5203847,15.189329 3.5577123,14.879995 3.6137091,14.570663 3.6696951,14.271997 3.4177365,14.207997 3.1471087,14.175997 H 2.8671571 L 2.5778602,14.186664 2.2979086,14.19733 H 2.0086223 L 1.7379946,14.16533 1.4953599,14.10133 1.2713943,13.994664 1.0847563,13.802664 1.1034255,13.546664 V 13.258665 L 1.0940909,12.949332 1.0847563,12.629333 1.0940909,12.330667 1.140753,12.032 1.2340667,11.765333 1.3833771,11.530667 1.5886842,11.338668 1.8966396,11.210667 H 1.9619602 L 1.9899532,11.200001 2.0179463,11.168001 2.0552739,11.136002 1.8406428,11.029334 1.6819979,10.890668 1.5886842,10.752001 1.5513566,10.581336 V 10.400003 L 1.5886842,10.208002 1.6446703,10.016003 1.72866,9.8240033 1.8126498,9.6320032 1.8966396,9.4506702 1.756653,9.3973377 1.6166772,9.3653373 1.4580323,9.3546702 1.2993874,9.3653373 1.140753,9.3760045 0.98210807,9.3653373 0.83279768,9.3546702 0.69282186,9.3013376 0.5715045,9.2053376 0.46885627,9.0773382 0.56216994,8.7466716 0.66481816,8.4266721 0.77680095,8.1173392 0.90745288,7.8080063 1.0380942,7.4880067 1.1594115,7.1786739 1.2713943,6.8800077 1.4020462,6.5600081 1.514029,6.2506752 1.6260118,5.9306758 1.3927116,5.9626757 1.2060737,5.9413424 1.0474287,5.8773425 0.92611134,5.7493427 0.84213224,5.6106762 0.76747705,5.4400098 0.72081488,5.2693433 0.66481816,5.0773436 0.6088321,4.8960106 0.56216994,4.7253442 0.64615969,4.5760111 0.67415273,4.4373447 0.65549426,4.2986782 0.59949753,4.1706784 0.51550777,4.0320118 0.43152867,3.8826788 0.34753892,3.7333457 0.29154218,3.5840126 0.26354916,3.4133461 0.29154218,3.2320131 0.55283537,2.7626804 0.87945984,2.3893476 1.2527358,2.1013481 1.6633394,1.9093484 2.1206051,1.7813486 2.5871948,1.6960154 3.0817881,1.6426821 3.585716,1.6320155 4.0802987,1.6000155 4.5655575,1.5680156 4.7148678,1.3653492 4.8828473,1.2586827 5.0601508,1.216016 5.2654579,1.248016 5.4520959,1.3013493 5.6480685,1.3440159 5.8626994,1.3973491 H 6.058672 L 6.2546339,1.3440159 6.4412719,1.1946828 6.5999168,1.0240163 6.7865548,0.91734988 7.0011965,0.83201665 7.2064929,0.81068331 7.4397931,0.76801675 7.6450895,0.74668341 7.8690551,0.68268352 8.0743623,0.60801701 8.2516657,0.45868388 8.3916522,0.26668418 Z'
