#!/usr/bin/python

# http://www.mdt.mt.gov/other/webdata/external/const/detailed_drawings/2018_Jan/619_SIGNS_DELINS.PDF#page=19

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MT'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:MT:SECONDARY'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:MT'] = lambda m,r: MTStateShield(m, r, guide=True).make_shield()
        network_registry['US:MT:SECONDARY'] = lambda m,r: MTSecondaryShield(m, r, guide=True).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:MT'] = lambda m,r: MTStateShield(m, r).make_shield()
        network_registry['US:MT:SECONDARY'] = lambda m,r: MTSecondaryShield(m, r, sign=True).make_shield()
    else:
        network_registry['US:MT'] = lambda m,r: MTStateShield(m, r).make_shield()
        network_registry['US:MT:SECONDARY'] = lambda m,r: MTSecondaryShield(m, r).make_shield()

class MTStateShield(ShieldBase):
    def __init__(self, modifier, ref, guide=False):
        super(MTStateShield, self).__init__(modifier, ref)
        self.guide = guide

        self.height = 24.0
        if self.guide:
            if len(self.ref) <= 2:
                self.width = 24.0
            else:
                self.width = 30.0
            self.ref_height = 12.0
            self.ref_series = 'D'
            self.corner_radius = 2.0
            ref_offset = 6.5
            self.ref_y = self.height - ref_offset - self.ref_height
            self.border_width = 0.5
        else:
            if len(self.ref) <= 1:
                self.width = 24.0
                self.ref_series = 'E'
            elif len(self.ref) == 2:
                self.width = 24.0
                self.ref_series = 'D'
            else:
                self.width = 30.0
                self.ref_series = 'D'
            self.border_width = 1.5
            self.corner_radius = 1.5
            self.mt_y = 3.5
            self.mt_height = 3.0
            self.mt_series = 'C'
            self.ref_y = self.mt_y + self.mt_height + 2.5
            self.ref_height = 10.0

            self.mt_x = self.border_width * 1.5
            self.mt_width = self.width - self.mt_x * 2
        self.ref_x = self.border_width * 1.5
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_mt-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        outline = self.drawing.rect(
            (self.border_width/2, self.border_width/2),
            (self.width - self.border_width, self.height - self.border_width),
            self.corner_radius - self.border_width/2,
            stroke=self.fg, fill=self.bg, stroke_width=self.border_width)

        if self.guide:
            return outline
        else:
            bg_g = self.drawing.g()
            bg_g.add(outline)
            bg_g.add(render_text(
                self.drawing, 'MONTANA', self.mt_x, self.mt_y, self.mt_width,
                self.mt_height, self.mt_series, self.fg))
            return bg_g

class MTSecondaryShield(ShieldBase):
    def __init__(self, modifier, ref, guide=False, sign=False):
        super(MTSecondaryShield, self).__init__(modifier, ref)
        assert not (guide and sign)
        self.guide = guide
        self.sign = sign

        if self.guide:
            self.height = 26.0
            self.width = 28.0
            self.ref_height = 8.0
            ref_offset = 11.0
            self.ref_y = self.height - ref_offset - self.ref_height
            self.outline_height = self.height
            self.outline_width = self.width
        else:
            self.width = 24.0
            self.height = 24.0
            self.ref_height = 7.0
            self.outline_height = 22.75
            self.outline_width = 19.25
        self.ref_y = 6.875
        self.border_width = 0.5
        self.corner_radius = 1.5
        
        self.ref_series = 'C'
        self.ref_x = 4.5
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_mt-secondary'
    
    def make_blank_shield(self):
        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(self.drawing.path(
                d=SECONDARY_OUTLINE, fill=self.bg,
                transform='matrix({} 0 0 {} {} {})'.format(
                    self.outline_width / SECONDARY_OUTLINE_WIDTH,
                    self.outline_height / SECONDARY_OUTLINE_HEIGHT,
                    self.width/2 - self.outline_width/2,
                    self.height/2 - self.outline_height/2)))
            return bg_g
        return self.drawing.path(
            d=SECONDARY_OUTLINE, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width,
            transform='matrix({} 0 0 {} {} {})'.format(
                self.outline_width / SECONDARY_OUTLINE_WIDTH,
                self.outline_height / SECONDARY_OUTLINE_HEIGHT,
                self.width/2 - self.outline_width/2,
                self.height/2 - self.outline_height/2))

SECONDARY_OUTLINE_HEIGHT = 22.75
SECONDARY_OUTLINE_WIDTH = 19.25
SECONDARY_OUTLINE = 'M 1.2823472,11.060703 C 0.77768266,10.211739 0.49022061,9.275779 0.7053153,8.0388309 0.23166535,7.4989537 -0.00470795,6.9042789 0.65844212,6.1043128 0.96607871,5.4897388 1.2599645,4.8670652 2.4610019,4.7617697 2.7500699,4.4377834 3.0790854,4.1537954 3.7870009,4.2470914 4.3340219,4.0125014 4.434192,3.3245305 3.719351,3.1592375 2.9863428,2.9725454 2.1009717,2.6178604 2.5015517,1.5446059 2.2659814,1.0580265 2.4700352,0.7340402 2.9105628,0.4993502 3.6814108,0.2940588 4.4536639,0.0927674 5.4891898,0.5140495 6.338327,0.4020543 7.118911,0.2087625 8.3235616,0.5140495 9.1496136,0.1833635 10.002766,0.1393654 10.896066,0.5007501 11.839551,0.3247575 12.786048,0.2113624 13.762656,0.5193493 14.4241,0.2807594 15.143759,0.1073668 16.388357,0.50205 16.739655,0.7807382 16.993593,1.0900251 16.757722,1.5526056 16.96549,2.1619797 16.799878,2.6592587 16.058137,2.981845 15.945722,2.9912446 15.817247,2.9618459 15.752006,3.0992401 15.161826,2.9672456 14.505401,4.2270923 15.707843,4.2510913 16.201668,4.2990892 16.531888,4.5110803 16.8059,4.777769 17.554666,4.8937641 18.226147,5.1163546 18.673801,6.1630103 18.969895,6.6602893 19.271008,7.3109617 18.579452,8.0508304 18.672797,8.980191 18.725994,9.920751 17.989272,11.066703 18.246221,12.177656 17.997302,13.74159 16.522854,14.848543 16.644303,16.152488 15.737954,17.50843 14.501386,18.017409 14.199269,18.886372 13.654256,19.671339 12.548169,20.159318 11.910815,21.29027 10.849895,21.974241 9.5872304,22.48222 8.4259399,22.045238 7.4123953,21.335268 6.6576066,20.140319 5.7461391,19.743336 5.1209292,18.994367 4.7177396,17.97341 3.5636758,17.357437 2.7551888,16.429476 2.7155423,14.804545 1.5634859,13.875584 1.0454721,12.647636 1.2823472,11.060703 Z'
