#!/usr/bin/python

# http://www.dot.state.pa.us/public/pubsforms/Publications/PUB%20236M/Sign%20Index%20for%20Change%201%20%2011-13%20(nomenclature).pdf#page=611

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:PA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:PA:TURNPIKE'] = lambda m,r: USGenericStateShield('TOLL', r).make_shield()
    else:
        network_registry['US:PA'] = lambda m,r: PAStateShield(m, r, style).make_shield()
        network_registry['US:PA:TURNPIKE'] = lambda m,r: PATurnpikeShield(m, r, style).make_shield()

class PAStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(PAStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE

        if len(self.ref) <= 2:
            self.width = 24.0             # A
            self.top_half_width = 7.0     # G
            self.bottom_half_width = 8.4  # K
            self.ref_series = 'D'
        else:
            self.width = 30.0             # A
            self.top_half_width = 10.0    # G
            self.bottom_half_width = 11.4 # K
            if len(self.ref) == 3 and '1' in self.ref:
                self.ref_series = 'D'
            else:
                self.ref_series = 'C'
        self.height = 24.0                # B
        self.ref_y = 5.4                  # C
        self.ref_height = 12.0            # D
        self.shoulder_spacing = 5.0       # F
        self.top_x = 5.0                  # F
        self.top_spacing = 1.0            # H
        self.bottom_spacing = 1.0         # H
        self.bottom_x = 3.6               # J

        self.keystone_angle = float(80.5) / 180 * math.pi
        self.corner_radius = 1.5 # assumed
        self.ref_x = self.width/2 - self.bottom_half_width
        self.ref_width = self.width - self.ref_x * 2

        self.shoulder_x = \
            self.bottom_x - \
            (self.height - self.bottom_spacing - self.shoulder_spacing) / \
            math.tan(self.keystone_angle)
        self.shoulder_width = \
            self.top_x - self.shoulder_x + \
            (self.shoulder_spacing - self.top_spacing) / \
            math.tan(self.keystone_angle)

        if self.guide:
            self.stroke_width = 0.5
        else:
            self.stroke_width = min(self.shoulder_x, self.top_spacing)
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_pa-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        background_g = self.drawing.g()

        if self.sign:
            background_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            black_outline = self.drawing.path(fill=self.bg)
            border_offset = 0
        else:
            black_outline = self.drawing.path(
                stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
            border_offset = self.stroke_width/2
        background_g.add(black_outline)
    
        top_l = (self.top_x - border_offset, self.top_spacing - border_offset)
        top_r = (self.width - top_l[0], top_l[1])
        l_shoulder_l = (self.shoulder_x - border_offset, self.shoulder_spacing - border_offset)
        r_shoulder_r = (self.width - l_shoulder_l[0], l_shoulder_l[1])
        l_shoulder_r = (l_shoulder_l[0] + self.shoulder_width, l_shoulder_l[1])
        r_shoulder_l = (self.width - l_shoulder_r[0], l_shoulder_r[1])
        bottom_l = (self.bottom_x - border_offset, self.height - self.bottom_spacing + border_offset)
        bottom_r = (self.width - bottom_l[0], bottom_l[1])
        
        black_outline.push('M', bottom_l)
        black_outline.push('L', l_shoulder_l)
        black_outline.push('h', self.shoulder_width)
        black_outline.push('L', top_l)
        black_outline.push('h', self.top_half_width*2 + self.stroke_width)
        black_outline.push('L', r_shoulder_l)
        black_outline.push('h', self.shoulder_width)
        black_outline.push('L', bottom_r)
        black_outline.push('Z')
    
        return background_g

class PATurnpikeShield(PAStateShield):
    def __init__(self, modifier, ref, style):
        self.pa_turnpike = ref.startswith('PENNSYLVANIA TURNPIKE')
        
        super(PATurnpikeShield, self).__init__(modifier, '' if self.pa_turnpike else ref, style)
        
        self.pa_half_width = 2.3       # E
        self.turnpike_half_width = 8.4 # F
        self.pa_y = 1.4                # J
        self.pa_height = 2.7           # K
        self.turnpike_height = 2.7     # K
        self.pa_series = 'D'           # K
        self.turnpike_series = 'D'     # K
        turnpike_offset = 1.0          # M
        self.turnpike_y = self.shoulder_spacing + turnpike_offset
        ref_offset = 1.9               # N
        self.ref_y = self.turnpike_y + self.turnpike_height + ref_offset
        
        self.pa_x = self.width/2 - self.pa_half_width
        self.pa_width = self.pa_half_width * 2
        self.turnpike_x = self.width/2 - self.turnpike_half_width
        self.turnpike_width = self.turnpike_half_width * 2
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN
        
    @property
    def blank_key(self):
        if self.pa_turnpike:
            return 'us_pa-turnpike'
        else:
            return 'us_pa-turnpike-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        background_g = super(PATurnpikeShield, self).make_blank_shield()
        if self.pa_turnpike:
            return background_g
        
        pa_g = render_text(self.drawing, 'PA', self.pa_x, self.pa_y, self.pa_width, self.pa_height, self.pa_series, color=self.fg)
        background_g.add(pa_g)
        turnpike_g = render_text(self.drawing, 'TURNPIKE', self.turnpike_x, self.turnpike_y, self.turnpike_width, self.turnpike_height, self.turnpike_series, color=self.fg)
        background_g.add(turnpike_g)
            
        return background_g

    def make_ref(self):
        if self.pa_turnpike:
            return self.make_turnpike()
        else:
            return super(PATurnpikeShield, self).make_ref()
        
    def make_turnpike(self):
        text_g = self.drawing.g()
    
        penna_g = render_text(self.drawing, 'PENNA', self.width/2 - self.top_half_width, self.pa_y, self.top_half_width*2, self.pa_height, self.pa_series, color=self.fg)
        text_g.add(penna_g)
    
        turn_g = render_text(self.drawing, 'TURN-', 0, self.shoulder_spacing + 1.5, self.width, 5.5, 'D', color=self.fg)
        text_g.add(turn_g)
        
        pike_g = render_text(self.drawing, 'PIKE', 0, self.shoulder_spacing + 2 * 1.5 + 5.5, self.width, 5.5, 'D', color=self.fg)
        text_g.add(pike_g)
        
        return text_g
