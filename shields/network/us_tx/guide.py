#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=11

import math
import svgwrite

from ...shieldbase import ShieldBase
from ...utils import *

# Failsafe in case someone puts a paragraph in the ref tag.
MAX_TEXT_WIDTH = 42.0

class TXGuideShield(ShieldBase):
    def __init__(self, subset, modifier, ref):
        super(TXGuideShield, self).__init__(modifier, ref)

        self.subset = subset

        if self.modifier == 'TOLL' and self.subset == 'TEXAS':
            self.modifier = ''
            self.subset = 'TOLL'

        self.height = 24.0       # B
        self.margin = 2.5        # C
        self.subset_y = self.margin
        self.subset_height = 4.0 # D
        self.subset_series = 'D' # D
        ref_spacing = 3.0        # E
        self.ref_y = self.subset_y + self.subset_height + ref_spacing
        self.ref_height = 12.0   # F
        self.ref_series = 'D'    # F
        self.corner_radius = 1.5 # G

        self.border_width = 0.5
        self.height += 2 * self.border_width
        self.subset_y += self.border_width
        self.ref_y += self.border_width
        
        self.subset_width = text_length(self.subset, self.subset_height, self.subset_series)
        self.ref_width = min(MAX_TEXT_WIDTH,
                             text_length(self.ref, self.ref_height, self.ref_series))
        base_width = max(self.subset_width, self.ref_width) + self.margin * 2
        self.width = round(base_width + 0.5) + self.border_width * 2
        self.subset_x = self.width/2 - self.subset_width/2
        self.ref_x = self.width/2 - self.ref_width/2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_tx-guide-{}-{}'.format(self.subset, int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_ul = (self.border_width/2, self.border_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius + bg_ul[0]
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))
        bg_g.add(render_text(
            self.drawing, self.subset, self.subset_x, self.subset_y,
            self.subset_width, self.subset_height, self.subset_series, self.fg))
        
        return bg_g
