#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf

from .state import TXStateShield
from .square_top_label import TXSquareTopShield
from .park_road import TXParkRouteShield
from .farm_road import TXFarmRoadShield
from .toll import TXTollShield

from .guide import TXGuideShield

from ...us_state import USGenericStateShield
from ...utils import STYLE_GENERIC, STYLE_GUIDE, STYLE_SIGN

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:TX'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:TX:BELTWAY'] = make_generic_shield_func('BELTWAY')
        network_registry['US:TX:BUS'] = make_generic_shield_func('BUSINESS')
        network_registry['US:TX:FM'] = make_generic_shield_func('FM')
        network_registry['US:TX:LOOP'] = make_generic_shield_func('LOOP')
        network_registry['US:TX:NASA'] = make_generic_shield_func('NASA')
        network_registry['US:TX:PA'] = make_generic_shield_func('PA')
        network_registry['US:TX:PARK'] = make_generic_shield_func('PARK')
        network_registry['US:TX:RECREATIONAL'] = make_generic_shield_func('R')
        network_registry['US:TX:RM'] = make_generic_shield_func('RM')
        network_registry['US:TX:SPUR'] = make_generic_shield_func('SPUR')
    elif style == STYLE_GUIDE:
        network_registry['US:TX'] = lambda m,r: us_tx_shield(m, r, style)
        network_registry['US:TX:BELTWAY'] = lambda m,r: TXGuideShield('BELTWAY', m, r).make_shield()
        network_registry['US:TX:BUS'] = lambda m,r: TXGuideShield('TEXAS', 'BUSINESS', r).make_shield()
        network_registry['US:TX:FM'] = lambda m,r: TXGuideShield('FM', m, r).make_shield()
        network_registry['US:TX:LOOP'] = lambda m,r: TXGuideShield('LOOP', m, r).make_shield()
        network_registry['US:TX:NASA'] = lambda m,r: TXGuideShield('NASA', m, r).make_shield()
        network_registry['US:TX:PA'] = lambda m,r: TXGuideShield('PA', m, r).make_shield()
        network_registry['US:TX:PARK'] = lambda m,r: TXGuideShield('PARK', m, r).make_shield()
        network_registry['US:TX:RECREATIONAL'] = lambda m,r: TXGuideShield('R', m, r).make_shield()
        network_registry['US:TX:RM'] = lambda m,r: TXGuideShield('RM', m, r).make_shield()
        network_registry['US:TX:SPUR'] = lambda m,r: TXGuideShield('SPUR', m, r).make_shield()
    else:
        sign = style == STYLE_SIGN
        network_registry['US:TX'] = lambda m,r: us_tx_shield(m, r, style)
        network_registry['US:TX:BELTWAY'] = lambda m,r: TXSquareTopShield('BELTWAY', m, r).make_shield()
        network_registry['US:TX:BUS'] = lambda m,r: TXStateShield('BUSINESS', r).make_shield()
        network_registry['US:TX:FM'] = lambda m,r: TXFarmRoadShield('FARM', m, r, sign).make_shield()
        network_registry['US:TX:LOOP'] = lambda m,r: TXSquareTopShield('LOOP', m, r).make_shield()
        network_registry['US:TX:NASA'] = lambda m,r: TXParkRouteShield(m, r, 'NASA').make_shield()
        network_registry['US:TX:PA'] = lambda m,r: TXSquareTopShield('PA', m, r).make_shield()
        network_registry['US:TX:PARK'] = lambda m,r: TXParkRouteShield(m, r).make_shield()
        network_registry['US:TX:RECREATIONAL'] = lambda m,r: TXFarmRoadShield('R', m, r, sign).make_shield()
        network_registry['US:TX:RM'] = lambda m,r: TXFarmRoadShield('RANCH', m, r, sign).make_shield()
        network_registry['US:TX:SPUR'] = lambda m,r: TXSquareTopShield('SPUR', m, r).make_shield()
    
def us_tx_shield(modifier, ref, style):
    if modifier == 'TOLL':
        return TXTollShield(modifier, ref).make_shield()
    elif style == STYLE_GUIDE:
        return TXGuideShield('TEXAS', modifier, ref).make_shield()
    else:
        return TXStateShield(modifier, ref).make_shield()

def make_generic_shield_func(header):
    def tx_generic_shield_func(modifier, ref):
        if modifier == '':
            new_modifier = header
        else:
            new_modifier = modifier + ';' + header
        return USGenericStateShield(new_modifier, ref).make_shield()
    return tx_generic_shield_func
