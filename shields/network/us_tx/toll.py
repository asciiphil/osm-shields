#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_5.pdf#page=24
# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_11.pdf#page=84

import math
import svgwrite

from ...shieldbase import ShieldBase
from ...utils import *

# Failsafe in case someone puts a paragraph in the ref tag.
MAX_TEXT_WIDTH = 42.0

class TXTollShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(TXTollShield, self).__init__(modifier, ref)

        if self.modifier == 'TOLL':
            self.modifier = ''
        
        if len(self.ref) == 1:
            ref_padding = 2.0
            self.ref_height = 10.75
            self.ref_series = 'F'
        elif len(self.ref) == 2:
            ref_padding = 2.0
            self.ref_height = 10.75
            self.ref_series = 'E'
        elif len(self.ref) == 3:
            ref_padding = 2.375
            self.ref_height = 10
            self.ref_series = 'D'
        else:
            ref_padding = 2.875
            self.ref_height = 9.0
            self.ref_series = 'C'
        self.width = 24.0
        self.height = 24.0
        self.border_width = 0.625
        self.toll_height = 4.0
        toll_margin_bottom = 2.0
        self.toll_y = self.height - toll_margin_bottom - self.toll_height
        self.toll_x = 1.0
        toll_margin_right = 0.75
        self.toll_width = self.width - self.toll_x - toll_margin_right
        self.corner_radius = 1.5

        self.ref_y = self.border_width + ref_padding
        self.ref_x = self.border_width * 2
        self.ref_width = self.width - self.ref_x * 2
        self.bg_height = self.ref_height + ref_padding * 2
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_BLUE

    @property
    def blank_key(self):
        return 'us_tx-toll'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect(
            (0, 0), (self.width, self.height), self.corner_radius, fill=self.fg))
        bg = self.drawing.path(fill=self.bg)
        bg_g.add(bg)
        bg.push('M', (self.border_width, self.border_width + self.bg_height))
        bg.push('V', self.corner_radius)
        bg.push_arc((self.corner_radius, self.border_width), 0,
                    self.corner_radius - self.border_width, large_arc=False,
                    angle_dir='+', absolute=True)
        bg.push('H', self.width - self.corner_radius)
        bg.push_arc((self.width - self.border_width, self.corner_radius), 0,
                    self.corner_radius - self.border_width, large_arc=False,
                    angle_dir='+', absolute=True)
        bg.push('V', self.border_width + self.bg_height)
        bg.push('Z')

        toll_scale = self.toll_width / TOLL_SYMBOL_WIDTH
        toll_y = self.height - (self.height - self.border_width - self.bg_height)/2 - \
                 TOLL_SYMBOL_HEIGHT * toll_scale / 2
        toll_g = self.drawing.g(
            transform='matrix({0} 0 0 {0} {1} {2})'.format(
                toll_scale, self.toll_x, toll_y))
        bg_g.add(toll_g)
        toll_g.add(self.drawing.path(d=TOLL_BLUE, fill=COLOR_BLUE))
        toll_g.add(self.drawing.path(d=TOLL_RED, fill=COLOR_RED))
        
        return bg_g

TOLL_SYMBOL_WIDTH = 36.883
TOLL_SYMBOL_HEIGHT = 9.395
TOLL_BLUE = 'm32.591 7.2709c-0.009-1.6521 0.056-3.382 0-4.9857 0.022-0.3827-0.749-0.8263-0.005-0.684 0.77-0.0037 1.54-0.0073 2.31-0.0111-0.513 0.2751-0.486 0.8268-0.454 1.2963-0.012 1.3301 0.01 2.6958-0.026 4.0037 0.807 0.285 1.775 0.2634 2.466-0.2884-0.151 0.4767-0.302 0.9533-0.454 1.4299-1.434 0.0044-2.869 0.0089-4.303 0.0133 0.297-0.1531 0.474-0.4564 0.48-0.787m-5.516 0.013c-0.01-1.6521 0.054-3.3822 0-4.9857 0.021-0.3825-0.751-0.8261-0.006-0.684 0.77-0.0037 1.54-0.0073 2.311-0.0111-0.515 0.2748-0.487 0.8269-0.455 1.2963-0.013 1.3301 0.01 2.6958-0.025 4.0037 0.807 0.285 1.773 0.2632 2.465-0.2884-0.15 0.4767-0.302 0.9533-0.453 1.4299-1.435 0.0044-2.869 0.0089-4.304 0.0133 0.296-0.1533 0.475-0.4563 0.48-0.787m-13.35-5.6808c1.629-0.0088 3.26-0.0176 4.889-0.0264-0.136 0.5142-0.249 1.0421-0.4 1.5479-0.281-0.4669-0.879-0.4028-1.345-0.3933 0.013 1.5817-0.046 3.1737 0.013 4.7491 0.075 0.4026 0.649 0.7192-0.084 0.5905-0.732 0.0087-1.497-0.0173-2.208 0.0128 0.659-0.4831 0.316-1.3921 0.404-2.0785 0.003-1.0869 0.005-2.1739 0.009-3.2608-0.582-0.0259-1.205 0.0005-1.678 0.3673m-1.597 0.2085l-1.719-3.216c-1.2128 1.7889-3.8608 2.3055-5.6638 1.1169-0.3972-0.32802 0.5501 0.2786 0.7819 0.3257 1.7219 0.681 3.8411 0.0189 4.8689-1.5208 0.625 1.0267 1.181 2.1893 1.779 3.2682zm-8.464-2.2955c-1.2815 0.05642-2.568 0.74212-3.2773 1.7715 1.1725 2.2082 2.3449 4.4166 3.5174 6.6248 0.9347-1.2987 2.6223-1.9897 4.1972-1.7168-1.1751-2.2015-2.3651-4.462-3.5312-6.6267-0.2973-0.06922-0.6035-0.0503-0.9061-0.05282zm-1.0528 1.4707 1.1192 0.996c0.44-0.2493 0.8801-0.4986 1.3202-0.748-0.2089 0.4551-0.418 0.9101-0.6269 1.3652 0.3776 0.3327 0.7552 0.6653 1.1328 0.9981-0.502-0.0528-1.0039-0.1055-1.5059-0.1583-0.2089 0.4551-0.4179 0.9102-0.6269 1.3652-0.1016-0.4856-0.2032-0.9713-0.3046-1.4569-0.502-0.0528-1.004-0.1054-1.5059-0.1582 0.4348-0.2494 0.8697-0.4987 1.3047-0.748-0.1023-0.4851-0.2044-0.9701-0.3067-1.4551zm20.186-1.0507c-1.586 0.0128-3.268 0.9591-3.658 2.5815-0.488 1.7198 0.725 3.6419 2.476 3.9727 1.796 0.5061 4.098-0.2304 4.753-2.1051 0.708-1.6948-0.396-3.8198-2.144-4.2519-0.461-0.1415-0.945-0.2079-1.427-0.1972zm-0.186 1.0234c1.124-0.0868 1.822 1.0294 1.956 2.0166 0.173 1.0209-0.095 2.3988-1.25 2.6942-0.961 0.2704-1.945-0.4638-2.105-1.4179-0.284-0.9782-0.171-2.1752 0.52-2.9512 0.256-0.1984 0.563-0.2902 0.879-0.3417z'
TOLL_RED = 'm11.792 3.3217c0.586 1.1021 1.173 2.2041 1.76 3.3062-0.369 0.501-0.843 0.9234-1.381 1.2341-0.162 0.103-0.34 0.1806-0.514 0.2594-0.432 0.1679-0.886 0.2908-1.352 0.2997-0.4446 0.0285-0.892-0.0223-1.3186-0.1535-0.3425-0.0906-0.6777-0.2236-0.9735-0.4192-0.093-0.0402-0.2367-0.141-0.2844-0.1711-0.5907-1.1108-1.1815-2.2215-1.7721-3.3323 0.149 0.1183 0.3566 0.2071 0.523 0.3227 0.4732 0.2274 0.9865 0.3758 1.5087 0.4301 0.523 0.0292 1.0562-0.0027 1.5559-0.1707 0.247-0.0751 0.484-0.1756 0.713-0.2938 0.294-0.1675 0.583-0.3491 0.837-0.575 0.262-0.221 0.496-0.4738 0.698-0.7495h0.014'
