#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=16
# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=17
# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=18
# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=19

import math
import svgwrite

from ...shieldbase import ShieldBase
from ...utils import *

class TXFarmRoadShield(ShieldBase):
    def __init__(self, subset, modifier, ref, sign=False):
        super(TXFarmRoadShield, self).__init__(modifier, ref)
        self.sign = sign
        self.subset = subset
        
        if '-' in self.ref:
            parts = self.ref.split('-')
            self.ref = '-'.join(parts[:-1])
            self.suffix = parts[-1]
        else:
            self.suffix = ''
            
        if len(self.ref) <= 2:
            self.ref_height = 6.0 # E
        elif len(self.ref) == 3:
            self.ref_height = 5.0 # F
        else:
            self.ref_height = 4.0 # G
        self.width = 24.0         # A
        self.height = 24.0        # A
        self.farm_y = 1.75        # B
        self.farm_height = 2.0    # C
        self.farm_series = 'D'    # C
        ref_center_offset = 7.0   # D
        self.ref_y = self.farm_y + self.farm_height + ref_center_offset - self.ref_height/2
        self.ref_series = 'D'     # E, F & G
        self.road_height = 2.0    # J
        self.road_series = 'D'    # J
        road_offset = 2.75        # K
        self.road_y = self.height - road_offset - self.road_height
        farm_offset = 9.75         # L
        road_right = 9.75          # M
        self.corner_radius = 1.5   # N

        # From the FM Business dimensions
        self.suf_height = 2.0      # H
        self.suf_series = 'D'      # H
        suffix_offset = 6.5        # J
        self.suf_y = self.height - suffix_offset - self.suf_height

        if self.subset == 'RANCH':
            # M1-6R
            farm_offset = 10.5     # L
        elif self.subset == 'R':
            # M1-6RR
            self.farm_y = 1.125    # B
            self.farm_height = 4.0 # C
            farm_offset = 7.375    # L
            
        self.border_width = 0.5
        self.ref_x = 7.5
        self.ref_width = 14.0
        self.suf_x = 12.0
        self.suf_width = 5.5
        
        self.farm_x = self.width - farm_offset
        self.farm_width = text_length(self.subset, self.farm_height, self.farm_series)
        self.road_width = text_length('ROAD', self.road_height, self.road_series)
        self.road_x = road_right - self.road_width

        if self.subset == 'R':
            self.fg = COLOR_BROWN
        else:
            self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_tx-{}'.format(self.subset)
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            subset_color = self.bg
        else:
            subset_color = self.fg
            
        bg_g.add(self.drawing.path(
            d=TX_OUTLINE, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        bg_g.add(render_text(
            self.drawing, self.subset, self.farm_x, self.farm_y,
            self.farm_width, self.farm_height, self.farm_series,
            subset_color))
        bg_g.add(render_text(
            self.drawing, 'ROAD', self.road_x, self.road_y,
            self.road_width, self.road_height, self.road_series,
            subset_color))
        
        return bg_g

    def make_ref(self):
        ref = render_text(
            self.drawing, self.ref, self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            color=self.fg)

        if self.suffix == '':
            return ref
        else:
            ref_g = self.drawing.g()
            ref_g.add(ref)
            ref_g.add(render_text(
                self.drawing, self.suffix, self.suf_x, self.suf_y,
                self.suf_width, self.suf_height, self.suf_series,
                self.fg))
            return ref_g
                
TX_OUTLINE = 'm23.307 11.448c-0.058-0.404-0.508-0.556-0.529-0.964-0.096-0.34-0.448-0.537-0.64-0.7623-0.091-1.0387-0.183-2.076-0.275-3.1146-0.532-0.0693-1.097-0.0947-1.524-0.452-0.569-0.308-1.249-0.2-1.786 0.112-0.289 0.0947-0.621 0.2733-0.873 0.0133-0.631-0.3026-1.238 0.2214-1.882 0.008-0.366-0.02-0.676-0.3306-0.974-0.4146-0.692 0.1027-1.427-0.0893-1.967-0.5133-0.304 0.0786-0.483 0.0053-0.771-0.1454-0.518 0.0774-0.323-0.4613-0.38-0.8079-0.046-1.016-0.091-2.0333-0.136-3.0493-1.6469 0.0267-3.2934 0.0547-4.9398 0.0813v8.9548c-1.992-0.03-3.9841-0.059-5.9762-0.09 0.13622 1.011 0.9456 1.806 1.8395 2.165 0.7976 0.448 1.2156 1.321 1.3449 2.194 0.2452 0.814 0.9403 1.41 1.7127 1.693 0.4566 0.523 1.5589 0.252 1.5257-0.492 0.1372-0.624 0.7347-1.062 1.365-0.952 1.0633 0.017 2.0592 0.747 2.4012 1.732 0.527 1.062 1.259 2.053 2.067 2.89 0.164 0.825 0.515 1.612 1.036 2.272 0.578 0.093 1.132 0.323 1.608 0.661 0.538-0.178 1.07 0.082 1.518 0.32 0.35 0.095 0.828-0.399 0.458-0.654-0.972-1.338-0.752-3.434 0.576-4.449 0.796-0.488 1.614-1.041 2.413-1.462 0.379-0.034 0.439-0.563 0.174-0.757-0.203-0.395 0.492-0.633 0.558-0.197 0.159 0.062 0.522-0.369 0.778-0.466 0.268-0.196 0.646-0.283 0.869-0.501 0.276-0.535 0.361-1.152 0.132-1.719-0.071-0.298 0.384-0.526 0.3-0.886 0.007-0.083-0.012-0.165-0.023-0.248z'
