#!/usr/bin/python

# http://www.modot.org/business/standards_and_specs/documents/Std_Plans_07_01_2017.pdf#page=425

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MO'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:MO:SUPPLEMENTAL'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:MO'] = lambda m,r: MOStateShield(m, r).make_shield()
        network_registry['US:MO:SUPPLEMENTAL'] = lambda m,r: MOSupplementalShield(m, r, guide=True).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:MO'] = lambda m,r: MOStateShield(m, r, sign=True).make_shield()
        network_registry['US:MO:SUPPLEMENTAL'] = lambda m,r: MOSupplementalShield(m, r).make_shield()
    else:
        network_registry['US:MO'] = lambda m,r: MOStateShield(m, r).make_shield()
        network_registry['US:MO:SUPPLEMENTAL'] = lambda m,r: MOSupplementalShield(m, r).make_shield()

class MOStateShield(ShieldBase):
    def __init__(self, modifier, ref, guide=False, sign=False):
        super(MOStateShield, self).__init__(modifier, ref)
        self.sign = sign
        
        if len(ref) <= 2:
            self.width = 24.0
            self.outline = OUTLINE_2
        else:
            self.width = 30.0
            self.outline = OUTLINE_3
        self.height = 24.0
        self.stroke_width = 0.5
        self.corner_radius = 0.5
        self.ref_y = 4.75
        self.ref_height = 12.0
        self.ref_x = 3.5
        right_space = 5.0
        self.ref_width = self.width - self.ref_x - right_space

        if len(self.ref) <= 2:
            self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C'])
        else:
            self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
            
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_mo-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(self.drawing.path(
                d=self.outline, fill=self.bg,
                transform='translate({0} {0}) scale({1})'.format(
                    self.stroke_width/2,
                    (self.height - self.stroke_width) / OUTLINE_HEIGHT)))
            return bg_g
        else:
            return self.drawing.path(
                d=self.outline, stroke=self.fg, fill=self.bg,
                stroke_width=self.stroke_width, stroke_linejoin='round')

class MOSupplementalShield(ShieldBase):
    def __init__(self, modifier, ref, guide=False):
        super(MOSupplementalShield, self).__init__(modifier, ref)
        self.guide = guide
        
        if len(ref) <= 1:
            self.width = 24.0
        else:
            self.width = 30.0
        self.height = 24.0
        if self.guide:
            self.stroke_width = 0.5
        else:
            self.stroke_width = 3.0

        self.ref_y = 6.0
        self.ref_height = 12.0
        self.ref_x = 6.0
        self.ref_width = self.width - self.ref_x * 2
        self.corner_radius = 0.5
        
        self.ref_series = 'D'
            
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_mo-supplemental-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        return self.drawing.rect(
            (self.stroke_width/2, self.stroke_width/2),
            (self.width - self.stroke_width, self.height - self.stroke_width),
            self.corner_radius,
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)

OUTLINE_HEIGHT = 24.0
OUTLINE_2 = 'M 0.78219271,0.2774193 C 0.57534123,0.37768572 0.22832476,0.47539214 0.28453662,0.77505806 l 0.55877194,1.05643734 0.0632985,0.528212 C 1.1771144,3.5601311 2.0997228,4.4310756 2.2424171,5.6665125 l 0.013093,0.547852 -0.1069518,0.5915185 0.037105,0.1658796 0.05675,0.1571462 c 0.1490263,0.3637058 0.3614671,0.6903183 0.4845601,1.069544 l 0.043655,0.1615196 -0.0633,11.0029996 c 0.01762,0.388892 0.068277,0.886837 0.392887,1.15029 l 0.1287797,0.0916 0.1637023,0.06333 14.6000966,0.1244 0.309943,0.248826 0.0065,0.266292 c -0.04329,0.729772 -0.452241,1.300677 -0.763945,1.927329 l -0.113493,0.229173 0.124426,0.124399 0.355782,0.01347 c 0.962165,0.04267 1.923688,0.167293 2.887713,0.0916 l 0.360145,-0.04147 0.174617,-0.200813 0.124413,-0.235732 c 0.23204,-0.538546 0.174675,-1.34653 0.64608,-1.759249 l 0.235731,-0.16808 0.168068,-0.161506 c 0.408987,-0.473266 0.383832,-1.063757 0.696284,-1.578103 l 0.130959,-0.187706 0.259741,-0.373239 0.117866,-0.366693 0.0065,-0.353612 C 23.635494,17.5402 23.03346,16.849495 22.999832,16.097883 l 0.024,-0.403812 -0.279386,-0.473639 -0.211723,-0.502012 c -0.435915,-1.24437 -0.38155,-2.665166 -0.384155,-3.96599 l 0.0065,-0.591505 V 9.7331157 c -0.06229,-0.9929576 -0.506149,-1.758729 -1.0391,-2.569047 L 20.886886,6.8301229 20.66425,6.4939771 20.59441,6.0596315 20.48965,5.6361792 C 20.170526,4.6490617 19.600815,3.8362371 19.073067,2.9558259 L 18.861346,2.5782135 18.79368,2.3468541 C 18.676667,1.8022155 18.776266,1.1418838 18.481554,0.65743168 L 18.32658,0.48281879 18.114859,0.33872581 Z'
OUTLINE_3 = 'M 0.78219271,0.2774193 C 0.57534123,0.37768572 0.22832476,0.47539214 0.28453662,0.77505806 l 0.55877194,1.05643734 0.0632985,0.528212 C 1.1771144,3.5601311 2.0997228,4.4310756 2.2424171,5.6665125 l 0.013093,0.547852 -0.1069518,0.5915185 0.037105,0.1658796 0.05675,0.1571462 c 0.1490263,0.3637058 0.3614671,0.6903183 0.4845601,1.069544 l 0.043655,0.1615196 -0.0633,11.0029996 c 0.01762,0.388892 0.068277,0.886837 0.392887,1.15029 l 0.1287797,0.0916 0.1637023,0.06333 20.6001346,0.1244 0.309943,0.248826 0.0065,0.266292 c -0.04329,0.729772 -0.452241,1.300677 -0.763945,1.927329 l -0.113493,0.229173 0.124426,0.124399 0.355782,0.01347 c 0.962165,0.04267 1.923688,0.167293 2.887713,0.0916 l 0.360145,-0.04147 0.174617,-0.200813 0.124413,-0.235732 c 0.232041,-0.538546 0.174675,-1.34653 0.646081,-1.759249 l 0.235731,-0.16808 0.168068,-0.161506 c 0.408987,-0.473266 0.383832,-1.063757 0.696284,-1.578103 l 0.130959,-0.187706 0.259741,-0.373239 0.117866,-0.366693 0.0065,-0.353612 C 29.635532,17.5402 29.033498,16.849495 28.99987,16.097883 l 0.024,-0.403812 -0.279386,-0.473639 -0.211723,-0.502012 c -0.435915,-1.24437 -0.38155,-2.665166 -0.384155,-3.96599 l 0.0065,-0.591505 V 9.7331157 c -0.06229,-0.9929576 -0.506149,-1.758729 -1.0391,-2.569047 L 26.886924,6.8301229 26.664288,6.4939771 26.594448,6.0596315 26.489688,5.6361792 C 26.170564,4.6490617 25.600853,3.8362371 25.073105,2.9558259 L 24.861384,2.5782135 24.793718,2.3468541 C 24.676705,1.8022155 24.776304,1.1418838 24.481592,0.65743168 L 24.326618,0.48281879 24.114897,0.33872581 Z'
