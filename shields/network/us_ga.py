#!/usr/bin/python

# Shapes inferred from images of signs.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style ==STYLE_GENERIC:
        network_registry['US:GA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:GA'] = lambda m,r: GAStateShield(m, r, sign=True).make_shield()
    else:
        network_registry['US:GA'] = lambda m,r: GAStateShield(m, r).make_shield()

MODIFIER_MAPPINGS = {
    'ALTERNATE': 'ALT',
    'BUSINESS': 'BUS',
    'BYPASS': 'BYP',
    'CONNECTOR': 'CONN',
}
class GAStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=False):
        super(GAStateShield, self).__init__(modifier, ref)
        self.sign = sign
        
        if modifier.upper() in MODIFIER_MAPPINGS:
            self.modifier = MODIFIER_MAPPINGS[modifier.upper()]
        else:
            self.modifier = modifier.upper()
        self.ref = ref

        self.width = 24.0
        self.height = 24.0
        if self.modifier != '':
            self.modifier_height = 3.0
            self.modifier_y = 2.0
            self.modifier_x = 2.5
            self.modifier_width = 8.5
            self.modifier_series = series_for_text(self.modifier, self.modifier_height, self.modifier_width, ['D', 'C', 'B'])
        if len(ref) <= 2:
            self.ref_height = 10.0
            self.ref_y = 9.5
        else:
            self.ref_height = 8.0
            self.ref_y = 10.5
        self.stroke_width = 0.5
        self.ref_x = 4.5
        self.ref_width = 15.0
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
        self.corner_radius = 1.5

        if ref == '515':
            self.fg = COLOR_BLUE
        elif ref == '520':
            self.fg = COLOR_GREEN
        else:
            self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ga'
    
    def make_blank_shield(self):
        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(self.drawing.path(
                d=STATE_OUTLINE, fill=self.bg,
                transform=('matrix({0} 0 0 {0} {1} {1})'.format(
                    (self.width - self.stroke_width) / self.width, self.stroke_width/2))))
            return bg_g
        else:
            return self.drawing.path(
                d=STATE_OUTLINE, stroke=self.fg, fill=self.bg,
                stroke_width=self.stroke_width)

    def make_ref(self):
        ref_text = render_text(
            self.drawing, self.ref, self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            color=self.fg)
        if self.modifier == '':
            return ref_text
        else:
            ref_g = self.drawing.g()
            ref_g.add(ref_text)
            ref_g.add(render_text(
                self.drawing, self.modifier, self.modifier_x, self.modifier_y,
                self.modifier_width, self.modifier_height, self.modifier_series,
                color=self.fg))
            return ref_g

STATE_OUTLINE = 'M 18.645382,8.2162413 C 18.61911,9.2087718 19.632139,9.7805789 20.542823,10.249134 C 21.258139,11.056762 20.788307,12.443596 21.95339,12.793722 C 22.663383,13.541929 22.181454,14.619386 23.160935,15.095876 C 23.495251,16.19339 22.86672,17.065853 22.422371,18.032342 C 22.186337,19.246303 21.327819,20.031384 21.312919,21.255106 C 20.944708,22.045163 19.396294,21.222767 18.590634,21.029237 C 17.439569,21.312007 18.992014,23.657143 17.348374,23.192808 C 17.471384,21.666981 15.644819,22.380332 14.689784,22.120706 C 11.2427,21.913098 7.7944189,21.723063 4.3438703,21.583032 C 3.9423631,20.636602 3.2402266,19.730539 3.2044072,18.6636 C 3.5529522,17.754925 3.2786447,17.011913 3.0065769,16.14052 C 3.4708931,15.426351 3.5583691,14.888943 3.7795408,14.018131 C 3.777131,13.352656 3.6941082,12.963421 3.2610201,12.265117 C 2.8915239,11.352065 2.5973016,10.496857 2.4740554,9.4786318 C 1.9123612,6.583195 1.321973,3.6934261 0.75,0.75 C 4.7524997,0.75 8.7652719,0.75 12.775272,0.75 C 13.069205,1.1351222 10.780714,2.0020339 12.207756,2.5202281 C 12.935673,3.2364874 14.226223,3.0908953 14.485977,4.1789221 C 15.004005,5.1435534 15.624127,6.003988 16.634856,6.4791871 C 17.247261,7.1402967 17.724022,7.6147401 18.473675,8.1294579 Z'

