#!/usr/bin/python

# http://www.wsdot.wa.gov/publications/manuals/fulltext/M55-05/SeriesM.pdf#page=7

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:WA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:WA'] = lambda m,r: WAStateShield(m, r, style).make_shield()

class WAStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(WAStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        
        if len(self.ref) <= 1:
            self.ref_height = 13.0
            self.ref_x = 8.0
            self.ref_width = 9.5
        elif len(self.ref) == 2:
            self.ref_height = 10.0
            self.ref_x = 5.0
            self.ref_width = 12.5
        else:
            self.ref_height = 8.0
            self.ref_x = 4.5
            self.ref_width = 14.5
        self.ref_series = 'C'
        self.width = 24.0
        self.height = 24.0
        self.ref_y = 6.0
        self.border_width = 0.5
        self.corner_radius = 1.5
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_wa'
    
    def make_blank_shield(self):
        outline = self.drawing.path(d=WA_OUTLINE, fill=self.bg)

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            outline['transform'] = 'translate({0} {1}) scale({2}) translate(-{0} -{1})'.format(
                self.width/2, self.height/2,
                (self.height - self.border_width) / self.height)
            bg_g.add(outline)
            return bg_g
        else:
            outline['stroke'] = self.fg
            outline['stroke-width'] = self.border_width
            return outline

WA_OUTLINE = 'm2.7775 12.134c-0.4101-0.071-1.1914 0.25-1.1065-0.45 0.4183-1.156 1.2999-2.1636 1.4524-3.4036 0.0173-0.6653-0.1296-1.4026 0.3884-1.936 0.7306-0.8639 0.7128-2.1746 1.695-2.8572 1.1658-0.312 0.8438-1.9346 2.2381-1.956 2.2417-0.74795 4.6441-0.94795 6.9791-0.58796 1.342 0.17066 2.38 1.2027 3.32 2.1 1.306 1.3466 2.188 3.1159 2.357 4.9945 0.181 1.164 1.518 1.8107 1.852 2.7553 0.174 0.549 0.448 1.08 0.033 1.749-0.492 1.26-1.433 2.511-2.824 2.802-0.972 0.582-1.017 2.213 0.007 2.772 0.607-0.089 1.278-1.156 1.762-0.588-0.274 0.49-0.812 0.629-0.448 1.281 0.119 0.784-1.046 1.175-0.072 1.827 0.763 0.482 1.811 0.881 1.823 1.953 0.391 0.801-0.329 0.65-0.857 0.645h-18.15c-0.0015-1.065 1.2258-1.568 1.8602-2.266 0.8865-0.825 2.0281-1.58 2.3586-2.804-0.0887-1.202-1.5979-1.355-2.5326-1.568-0.757-0.194-2.5397 0.106-2.0726-1.211 0.2195-0.656 0.0175-1.428 0.1736-2.055-0.3776-0.304 0.4544-0.968-0.2367-1.196z'
