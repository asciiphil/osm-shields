#!/usr/bin/python

# http://www.udot.utah.gov/main/uconowner.gf?n=13202015905895727#page=334
#
# SVG paths derived by tracing over the given images.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:UT'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:UT'] = lambda m,r: UTStateShield(m, r, style).make_shield()

class UTStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(UTStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        
        if len(self.ref) <= 2:
            self.width = 24.0
            self.outline = UT_OUTLINE_2
            self.inner_lines = UT_INNER_LINES_2
            self.inner_fills = UT_INNER_FILLS_2
            self.ref_x = 5.0
            self.ref_y = 8.0
            self.ref_series = 'D'
        else:
            self.width = 30.0
            self.outline = UT_OUTLINE_3
            self.inner_lines = UT_INNER_LINES_3
            self.inner_fills = UT_INNER_FILLS_3
            self.ref_x = 6.0
            self.ref_y = 8.5
            if len(self.ref) == 3:
                self.ref_series = 'D'
            else:
                self.ref_series = 'C'
        self.height = 24.0
        self.ref_height = 8.0
        self.ref_width = self.width - self.ref_x * 2
        self.border_width = 0.5
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ut-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), fill=self.fg))
            # FIXME: This is a hack and doesn't look so great around the
            # lower edges of the beehive.
            bg_g.add(self.drawing.path(
                d=self.outline, stroke=self.bg, fill='none',
                stroke_width=self.border_width,
                transform='translate({1} {2}) scale({0}) translate(-{1} -{2})'.format(
                    self.width / (self.width - self.border_width * 2),
                    self.width/2, self.height/2)))
            
        bg_g.add(self.drawing.path(
            d=self.outline, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))
        bg_g.add(self.drawing.path(
            d=self.inner_lines, stroke=self.fg, fill='none',
            stroke_width=self.border_width))
        bg_g.add(self.drawing.path(
            d=self.inner_fills, fill=self.fg))
        
        return bg_g


UT_OUTLINE_2 = 'm21.875 16.299c0.399-0.887 0.349-1.762-0.43-2.616 0.371-1.357-0.1-2.07-0.7-2.683 0.083-1.0123-0.215-1.9176-1.179-2.6356-0.044-1.2454-0.639-2.0412-1.566-2.5671-0.485-1.2504-1.043-1.3685-1.591-1.6458-0.453-1.3069-1.24-1.6836-2.149-1.7196-1.479-2.0233-2.939-1.856-4.3849-0.0491-1.1219 0.1539-1.8585 0.7633-2.26 1.7687-1.0569 0.306-1.2018 0.9768-1.4739 1.5967-1.2532 0.6315-1.4973 1.5514-1.6212 2.5056-1.1757 0.8143-1.2158 1.6593-1.2037 2.5052-0.772 0.794-1.1063 1.733-0.7984 2.887-0.8395 0.845-0.7249 1.677-0.2948 2.555-0.795 0.933-0.3586 1.491-0.0123 2.075l-1.4608 1.13v3.844h22.5v-3.586l-1.436-1.216c0.58-0.799 0.349-1.478 0.061-2.149z'
UT_INNER_LINES_2 = 'm9.8751 2.3828c1.6569 1.4342 3.0879 1.2313 4.3849 0.0491m-6.6449 1.7196c3.0379 1.9321 5.9529 1.6327 8.7939 0m5.405 14.296c-6.535 2.923-13.069 2.997-19.603-0.172'
UT_INNER_FILLS_2 = 'm22.85 19.301-1.997 0.507 2.371 0.371 0.026-0.515zm-22.12 0.652 2.4149-0.34-2.0675-0.485-0.3269 0.278zm9.7355 0.651-0.034-1.728c-0.031-2.041 2.943-2.416 3.071-0.065l0.012 1.776'
UT_OUTLINE_3 = 'M 27.480646,16.287526 C 27.984634,15.402882 27.921969,14.530637 26.937993,13.678658 27.405981,12.325359 26.81133,11.61511 26.052682,11.003392 26.156679,9.9936835 25.780689,9.0910394 24.563386,8.3750573 24.507387,7.133755 23.755406,6.3391082 22.583435,5.8151213 21.971451,4.5684858 21.266135,4.4498221 20.574152,4.173829 20.000833,2.8711949 19.006191,2.4952043 17.856887,2.4592052 15.988934,0.4419223 14.143646,0.6085848 12.315692,2.4098731 10.897728,2.5632026 9.9669508,3.1711874 9.4596302,4.173829 8.1237969,4.4791547 7.9407348,5.1484713 7.5968767,5.7657892 6.0130497,6.3964401 5.704524,7.3137505 5.5479946,8.2643934 4.0621651,9.0763731 4.0114997,9.9190187 4.0266993,10.763531 3.0511237,11.554844 2.628601,12.491621 3.0177912,13.641859 1.9567511,14.484371 2.1015475,15.314617 2.6452672,16.189529 1.6404923,17.120572 2.1919452,17.676291 2.6296676,18.259477 L 0.74998125,19.386382 V 23.250019 H 29.250602 V 19.643576 L 27.403982,18.430939 C 28.137297,17.633759 27.845304,16.956443 27.481313,16.287526 Z'
UT_INNER_LINES_3 = 'M 12.315692,2.4098731 C 14.408973,3.840504 16.218261,3.6378424 17.856887,2.4592052 M 9.4596302,4.173829 C 13.298601,6.1004475 16.982242,5.8017883 20.574152,4.173829 M 27.403315,18.430806 C 19.146188,21.3454 10.887728,21.419398 2.6292676,18.259344'
UT_INNER_FILLS_3 = 'M 28.713949,19.281718 26.188679,19.787439 29.185937,20.156496 29.219269,19.643576 Z M 0.7573144,19.931302 3.8091048,19.592377 1.1963701,19.108522 0.78312709,19.386382 Z M 13.061407,20.580885 13.017408,18.857329 C 12.978742,16.822446 16.737848,16.448455 16.899178,18.79253 L 16.913844,20.563553'
