#!/usr/bin/python

import math
import svgwrite

from ..banner import banner
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:DC'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:DC'] = make_shield

class DCRouteParams:
    def __init__(self, ref):
        self.ref = ref
        self.width = 24.0
        self.height = 30.0
        self.stroke_width = 0.5
        self.corner_radius = 1.5

        self.outline_width = 1.0
        
        self.dc_y = 7.5
        self.dc_height = 4.0
        self.dc_x = 5.0
        self.dc_width = 10.0
        self.dc_series = 'D'

        self.ref_height = 8.25
        self.ref_width = self.width - self.stroke_width * 6
        self.ref_x = self.stroke_width * 3
        self.ref_y = 15.0
        self.ref_series = 'D'
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

DC_OUTLINE_D = 'M 21.125,14.25 22.325,12.75 9.875,1.25 1.75,10 2.7789151,11.223132 C 4.7513592,14.57307 7.098645,12.197422 8.006653,14.823439 8.861693,18.001325 10.351568,21.509325 9.2886387,23.689867 8.5455176,25.062595 8.7520031,25.839987 9.3011262,26.76006 9.5246937,27.3804 9.4938408,28.143661 9.5,28.625 H 10 L 13.625,24'

def make_shield(modifier, ref):
    if modifier == '':
        return make_base_shield(ref)
    else:
        shield_svg = make_base_shield(ref)
        banner_svgs = [banner(mod, int(shield_svg['width'])) for mod in modifier.split(';')]
        return compose_shields(banner_svgs + [shield_svg])

def make_base_shield(ref):
    params = DCRouteParams(ref)

    drawing = svgwrite.Drawing(size=(params.width, params.height))

    shield_bg = make_blank_shield(drawing, params)
    drawing.add(shield_bg)

    ref_g = make_ref(drawing, params)
    drawing.add(ref_g)
    
    return drawing

def make_blank_shield(drawing, params):
    background_g = drawing.g()

    background_g.add(
        drawing.rect(
            (params.stroke_width * 1.5, params.stroke_width * 1.5),
            (params.width - params.stroke_width * 3, params.height - params.stroke_width * 3),
            params.corner_radius - params.stroke_width * 1.5,
            stroke=params.fg, fill=params.bg, stroke_width=params.stroke_width))
    background_g.add(
        drawing.path(
            d=DC_OUTLINE_D, stroke=params.fg, fill='none', stroke_width=params.outline_width))
    background_g.add(
        drawing.rect(
            (params.ref_x, params.ref_y - 0.15), (params.ref_width, params.ref_height + 0.3),
            fill=params.bg))
    background_g.add(
        render_text(drawing, 'DC', params.dc_x, params.dc_y,
                    params.dc_width, params.dc_height, params.dc_series,
                    color=params.fg))
    
    return background_g

def make_ref(drawing, params):
    ref_g = render_text(
        drawing, params.ref, params.ref_x, params.ref_y,
        params.ref_width, params.ref_height, params.ref_series,
        color=params.fg)

    return ref_g
