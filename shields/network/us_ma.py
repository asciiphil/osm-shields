#!/usr/bin/python

# http://www.massdot.state.ma.us/Portals/8/docs/construction/StdDwgsSignsSupports1990.pdf#page=3

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:MA'] = lambda m,r: MAStateShield(m, r).make_shield()

class MAStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(MAStateShield, self).__init__(modifier, ref)

        if len(ref) <= 2:
            self.width = 24.0       # A
        elif len(ref) == 3:
            self.width = 30.0       # A
        else:
            self.width = 36.0       # A
        self.height = 24.0          # B
        self.ref_y = 6.0            # C
        self.ref_height = 12.0      # D
        self.corner_radius = 1.5    # F
        self.outer_border = 3.0/8.0 # G
        self.stroke_width = 5.0/8.0 # H
        self.ref_series = 'D'
        
        self.ref_x = self.outer_border + self.stroke_width * 2
        self.ref_width = self.width - self.ref_x * 2

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ma-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        return self.drawing.rect(
            (self.outer_border + self.stroke_width * 0.5, self.outer_border + self.stroke_width * 0.5),
            (self.width - (self.outer_border + self.stroke_width * 0.5) * 2,
             self.height - (self.outer_border + self.stroke_width * 0.5) * 2),
            self.corner_radius - self.outer_border - self.stroke_width/2,
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
