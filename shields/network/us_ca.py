#!/usr/bin/python

# http://www.dot.ca.gov/trafficops/tcd/docs/G28-1.pdf
# http://www.dot.ca.gov/trafficops/tcd/docs/G28-2.pdf

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:CA'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:CA'] = lambda m,r: CAStateShield(m, r, sign=False).make_shield()
    else:
        network_registry['US:CA'] = lambda m,r: CAStateShield(m, r).make_shield()

class CAStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=True):
        super(CAStateShield, self).__init__(modifier, ref)

        self.sign = sign
        
        if len(self.ref) <= 2:
            self.width = 24.0               # B
            self.lower_radius = 45.0        # F
            self.upper_radius = 17.125      # G
            upper_center_offset_x = 5.125   # J
            corner_center_offset_x = 8.125  # K
            corner_center_offset_y = 4.625  # L
            upper_center_offset_y = 8.625   # M
            ca_center_offset_y = 3.25       # P
            self.ca_radius = 20.0           # Q
        else:
            self.width = 28.0               # B
            self.lower_radius = 55.0        # F
            self.upper_radius = 16.125      # G
            upper_center_offset_x = 2.1875  # J
            corner_center_offset_x = 10.125 # K
            corner_center_offset_y = 4.875  # L
            upper_center_offset_y = 8.9375  # M
            ca_center_offset_y = 6.75       # P
            self.ca_radius = 23.5           # Q
        self.height = 25.0                  # A
        self.ref_y = 11.0                   # C
        self.ref_height = 10.0              # D
        self.ref_series = 'D'               # D
        self.corner_radius = 3.875          # H
        self.ca_height = 2.0                # N
        self.ca_series = 'E'                # N
        self.border_width = 0.75            # U

        self.upper_center = (self.width/2 + upper_center_offset_x, self.height - upper_center_offset_y)
        self.corner_center = (self.width/2 - corner_center_offset_x, self.height - corner_center_offset_y)
        self.lower_center = (self.width/2, self.height - self.lower_radius)
        self.ca_center = (self.width/2, self.height + ca_center_offset_y)

        self.ref_x = self.border_width * 2
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_ca-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.make_outline())
        if self.sign:
            bg_g.add(self.make_california())
        
        return bg_g

    def make_outline(self):
        outline = self.drawing.path(stroke=self.fg, fill=self.bg,
                                    stroke_width=self.border_width)

        # radii
        upper_radius = self.upper_radius - self.border_width/2
        corner_radius = self.corner_radius - self.border_width/2
        lower_radius = self.lower_radius - self.border_width/2

        # Find lower arc transition point.  It's colinear with the corner
        # and lower arc centers.
        lc = self.lower_center[0] + self.lower_center[1] * 1j
        cc = self.corner_center[0] + self.corner_center[1] * 1j
        unit_vector = (cc - lc) / abs(cc - lc)
        transition = cc + unit_vector * corner_radius
        
        # Points
        top_peak = (self.width/2, self.border_width/2)
        left_upper_arc_end = (self.border_width/2, self.upper_center[1])
        left_corner_end = (self.border_width/2, self.corner_center[1])
        left_transition = (transition.real, transition.imag)
        right_upper_arc_end = (self.width - left_upper_arc_end[0], left_upper_arc_end[1])
        right_corner_end = (self.width - left_corner_end[0], left_corner_end[1])
        right_transition = (self.width - left_transition[0], left_transition[1])

        # Drawing
        outline.push('M', top_peak)
        outline.push_arc(right_upper_arc_end, 0, upper_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push('V', right_corner_end[1])
        outline.push_arc(right_transition, 0, corner_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push_arc(left_transition, 0, lower_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push_arc(left_corner_end, 0, corner_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push('V', left_upper_arc_end[1])
        outline.push_arc(top_peak, 0, upper_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push('Z')
        
        return outline

    def make_california(self):
        return render_text_along_arc(
            self.drawing, 'CALIFORNIA', self.ca_center[0] + self.ca_center[1] * 1j,
            self.ca_radius, 0, self.ca_height, self.ca_series, self.fg, spacing=0.7)
    
