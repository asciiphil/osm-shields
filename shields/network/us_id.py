#!/usr/bin/python

# http://apps.itd.idaho.gov/apps/manuals/Sign_Chart_Supplement.pdf#page=23

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:ID'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:ID'] = lambda m,r: IDStateShield(m, r).make_shield()

class IDStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(IDStateShield, self).__init__(modifier, ref)

        if len(self.ref) <= 2:
            self.width = 24.0    # A
        else:
            self.width = 30.0    # A
        self.height = 24.0       # B
        self.ref_y = 1.0         # C
        self.ref_height = 10.0   # D
        self.ref_series = 'C'    # D
        ref_space_below = 12.0   # E
        self.id_height = self.ref_height + ref_space_below
        self.id_x = 1.5          # F
        self.id_width = 13.58    # G
        self.corner_radius = 1.5 # I

        self.id_scale_x = self.id_width / OUTLINE_WIDTH
        self.id_scale_y = self.id_height / OUTLINE_HEIGHT
        
        self.ref_x = 8.0
        self.ref_width = 15.0

        for spacing in (1.0, 0.9, 0.8, 0.7, 0.6, 0.5):
            self.ref_spacing = spacing
            if text_length(self.ref, self.ref_height, self.ref_series, spacing=self.ref_spacing) <= self.ref_width:
                break
            
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_id-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect(
            (0, 0), (self.width, self.height), self.corner_radius,
            fill=self.bg))
        bg_g.add(self.drawing.path(
            d=ID_OUTLINE, fill=self.fg,
            transform='matrix({} 0 0 {} {} {})'.format(
                self.id_scale_x, self.id_scale_y, self.id_x, self.ref_y)))
        
        return bg_g

OUTLINE_WIDTH = 26.0
OUTLINE_HEIGHT = 42.0
ID_OUTLINE = 'M -4.6096181e-4,0.00105 C 0.01000638,5.4902461 -0.02133511,10.984775 0.01510132,16.471305 0.70187081,17.011291 -0.08169093,17.839271 0.37445233,18.457922 0.34582638,19.853887 2.7813255,19.327234 2.1120089,20.883195 1.8509488,21.747173 1.1403132,22.272493 1.4553587,23.296468 1.7156188,24.967092 -0.4283276,25.464413 -0.50125911,27.052374 -1.1088839,28.397673 0.87683977,29.026724 0.42190448,30.346558 -0.28122461,31.526129 0.09223272,32.92036 -4.0496321e-4,34.217661 -3.9927002e-4,36.81173 -3.9927002e-4,39.405932 -3.9927002e-4,42 H 25.93362 V 25.997733 C 25.610961,25.609743 25.268303,25.231086 24.933645,24.861762 24.888312,24.315109 24.373659,24.232444 23.947003,24.465772 23.393683,24.740431 23.973669,25.743073 23.493681,25.684408 22.841697,25.85507 22.19638,25.73374 21.553729,25.949735 20.957744,25.88307 20.151098,25.901736 20.052433,26.519054 19.445782,26.460388 18.487139,26.340392 18.060483,26.721715 17.79249,27.311034 17.420499,26.26306 17.221837,26.025733 17.032509,25.499079 17.123173,24.607102 16.304527,24.653767 15.192555,24.625768 15.840539,23.456464 15.425882,22.895144 15.025892,22.257827 14.277911,21.925835 14.219246,21.100522 14.173914,20.308542 13.911254,19.877886 13.216604,19.552561 12.668084,19.224569 12.624618,20.235211 12.144764,20.405873 11.719574,21.269852 11.048258,20.425873 11.027325,19.788555 10.485605,19.264568 10.992126,18.959243 11.464514,18.625918 11.240653,18.127263 11.164788,17.603277 11.22252,17.061957 11.061057,16.041982 11.132656,14.916677 11.517579,13.960701 11.434915,13.140722 10.252678,14.338025 10.156547,13.427381 9.9985507,12.91806 9.1484386,13.448714 9.108173,12.730065 8.9479103,12.086081 8.2908601,11.86742 7.9664682,11.399432 7.3272841,11.46343 7.8616708,10.238127 7.0794237,10.432789 6.2319782,10.814113 5.9759846,9.7034741 5.5647949,9.2114864 5.5794612,8.147513 5.2468028,7.0822063 4.3924242,6.3235586 3.8707039,5.8449039 4.2368281,5.0449239 4.1281641,4.4156063 V 0.00105 H -7.9926002e-4 Z'
