#!/usr/bin/python

# http://www.sddot.com/business/design/plates/docs/s63220.pdf

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:SD'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:SD'] = lambda m,r: SDRouteShield(m, r, style).make_shield()

class SDRouteShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(SDRouteShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE
        
        if len(self.ref) <= 2:
            self.width = 24.0 # W
            self.outline = SD_OUTLINE_2
            self.ref_series = 'D'
        else:
            self.width = 30.0 # W
            self.outline = SD_OUTLINE_3
            if len(self.ref) == 3 and '1' in self.ref:
                self.ref_series = 'D'
            elif len(self.ref) == 3:
                self.ref_series = 'C'
            else:
                self.ref_series = 'B'
        self.height = 24.0 # H
        self.ref_height = 12.0 # M

        self.corner_radius = 1.5
        self.border_width = 0.5
        self.ref_y = 5.0
        self.ref_x = 1.5
        self.ref_width = self.width - self.ref_x - 2.75
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        if self.guide:
            self.border_color = COLOR_BLACK
        else:
            self.border_color = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_sd-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        outline = self.drawing.path(
            d=self.outline, stroke=self.border_color,
            fill=self.bg, stroke_width=self.border_width,
            stroke_linejoin='round')
        
        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.border_color))
            bg_g.add(outline)
            return bg_g
        else:
            return outline

SD_OUTLINE_2 = 'm1.1552 1.8c5.2769 0.4 10.58 0.5 15.866 0.2 1.949-0.1 3.904-0.3 5.851-0.2 0.723 0.9-0.996 1.5-1.292 2.3 0.38 0.8 0.13 1.9 1.129 2.3 0.46 0.2 0.384 0.7 0.386 1.1l0.156 7.8c-0.495-0.1-0.807 0.2-0.506 0.7-0.069 0.4 0.294 0.7 0.335 1.2 0.09 0.3-0.063 0.7 0.045 1 0.225 0.5 0.143 1.1-0.118 1.5-0.207 0.9 0.743 1.5 0.777 2.3-0.424 0.8-1.216-0.3-1.915-0.1-0.632-0.1-1.215-0.5-1.915-0.5-0.254-0.2-0.575-0.3-0.894-0.5-0.461 0-0.871 0.2-1.274 0.5-0.489-0.1-0.926-0.4-1.355-0.6-0.579-0.3-1.191-0.8-1.897-0.7-1.477-0.2-2.963 0.1-4.445 0-3.3014-0.1-6.5885-0.5-9.873-0.8 0.31312-5.8 0.62629-11.7 0.9394-17.5z'
SD_OUTLINE_3 = 'm28.542 15.653c0.047 0.404 0.1 0.841 0.231 1.233 0.34 0.462 0.226 1.056 0.232 1.58 0.27 0.56 0.217 1.211-0.087 1.734-0.223 0.934 0.791 1.535 0.828 2.39-0.469 0.933-1.349-0.333-2.12-0.02-0.647-0.317-1.335-0.537-2.052-0.626-0.252-0.281-0.629-0.347-0.972-0.443-0.455 0.038-0.851 0.226-1.253 0.443-0.522-0.022-0.988-0.358-1.446-0.568-0.641-0.412-1.332-0.842-2.118-0.839-1.563-0.039-3.127 0.117-4.691 0.079-4.961 0.105-9.9305-0.187-14.847-0.859 0.33391-6.246 0.66788-12.491 1.0019-18.738 7.3754 0.456 14.781 0.696 22.162 0.2213 1.787-0.052 3.579-0.2307 5.361-0.192 0.937 1.18-1.674 1.764-1.281 2.8799 0.285 0.8267 0.196 1.804 1.215 2.12 0.457 0.2653 0.252 0.8173 0.308 1.2626l0.163 8.1882c-0.174 0.074-0.674-0.131-0.635 0.154z'
