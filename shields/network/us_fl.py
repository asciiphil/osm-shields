#!/usr/bin/python

# http://www.fdot.gov/roadway/DS/18/IDx/17355.pdf#page=3
# http://www.fdot.gov/traffic/TrafficServices/SignLibrary/Guide/Guide_Bmp/fl_toll_rt_24[Y].bmp

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style ==STYLE_GENERIC:
        network_registry['US:FL'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:FL:TOLL'] = lambda m,r: USGenericStateShield('TOLL', r).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:FL'] = lambda m,r: FLStateShield(m, r, sign=False).make_shield()
        network_registry['US:FL:TOLL'] = make_toll_shield
    else:
        network_registry['US:FL'] = lambda m,r: FLStateShield(m, r).make_shield()
        network_registry['US:FL:TOLL'] = make_toll_shield

# Since the guide signs scale according to the text length, this is a
# failsafe to keep them from getting too long.
MAX_TEXT_LENGTH = 42.0

TURNPIKE_NAMES = [
    'FLORIDA\'S TURNPIKE',
    'HOMESTEAD EXTENSION OF FLORIDA\'S TURNPIKE',
]
def make_toll_shield(modifier, ref):
    if ref in TURNPIKE_NAMES:
        return FLTurnpikeShield(modifier, ref).make_shield()
    else:
        return FLTollShield(modifier, ref).make_shield()        
    
class FLStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=True):
        super(FLStateShield, self).__init__(modifier, ref)
        self.sign = sign

        self.height = 24.0         # B
        self.margin = 0.375
        self.border_width = 0.625
        if self.sign:
            fl_spacing = 1.75
            self.fl_y = fl_spacing
            ref_right_space = 5.0
            self.corner_radius = 2.0
            if len(ref) <= 2:
                self.width = 24.0
                self.fl_x = fl_spacing
                self.ref_y = 9.0
                self.ref_height = 10.0
            else:
                self.width = 30.0
                self.fl_x = 7.75
                self.ref_y = 10.5
                self.ref_height = 8.0
            self.fl_width = 20.5
            self.fl_height = 20.5
            self.fl_stroke_width = 1.0
            self.ref_x = self.border_width * 3
            self.ref_width = self.width - self.ref_x - ref_right_space
            self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
        else:
            # guide style
            self.fl_width = 26.0   # C
            self.ref_height = 12.0 # D
            self.ref_series = 'D'
            fl_spacing = 1.25      # E
            self.fl_height = self.height - fl_spacing * 2
            self.fl_y = fl_spacing
            ref_offset_y = 2.75    # F
            self.ref_y = self.height - ref_offset_y - self.ref_height
            ref_right_space = 8.25 # G
            self.ref_width = min(text_length(self.ref, self.ref_height, self.ref_series),
                                 MAX_TEXT_LENGTH)
            self.corner_radius = 1.5   # G
            if len(ref) <= 2:
                self.width = 30.0  # A
            else:
                self.width = round(0.5 +
                                   max(fl_spacing + self.ref_width + ref_right_space,
                                       fl_spacing + self.fl_width + fl_spacing))
            self.fl_stroke_width = 1.25
            self.ref_x = self.width - ref_right_space - self.ref_width
            self.fl_x = self.width - fl_spacing - self.fl_width
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_fl-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.margin + self.border_width/2
        bg_ul = (border_offset, border_offset)
        bg_dims = (self.width - border_offset * 2, self.height - border_offset * 2)
        bg_radius = self.corner_radius - border_offset
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        fl_x = self.fl_x + self.fl_stroke_width/2
        fl_y = self.fl_y + self.fl_stroke_width/2
        fl_width = self.fl_width - self.fl_stroke_width
        fl_height = self.fl_height - self.fl_stroke_width
        fl = self.drawing.path(stroke=self.fg, fill='none',
                               stroke_width=self.fl_stroke_width)
        fl.push('M', (FL_OUTLINE_POINTS[0][0] * fl_width + fl_x,
                      FL_OUTLINE_POINTS[0][1] * fl_height + fl_y))
        for x, y in FL_OUTLINE_POINTS[1:]:
            fl.push('L', (x * fl_width + fl_x, y * fl_height + fl_y))
        bg_g.add(fl)
    
        return bg_g
        
class FLTollShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(FLTollShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 30.0
        self.stroke_width = 0.5
        self.corner_radius = 1.5
        self.separator_y = 6.5
        self.header_y = 1.75
        self.header_height = 4.0
        self.header_x = self.stroke_width * 3
        self.header_width = self.width - self.header_x * 2
        self.header_series = 'E'
        self.header_spacing = 2.0
        self.circle_radius = self.width/2
        self.circle_y = self.height - self.circle_radius

        self.fl_x = 2.5
        self.fl_y = 7.5
        self.fl_width = 18.0
        self.fl_height = 18.0
        self.fl_stroke_width = 1.0

        self.ref_height = 8.0
        self.ref_y = 14.5
        self.ref_x = 2.0
        self.ref_width = 15.5
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width * 1.2, ['D', 'C', 'B'])
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        self.header_bg = COLOR_YELLOW

    @property
    def blank_key(self):
        return 'us:fl-toll'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()
    
        separator_l = (self.stroke_width * 1.5, self.separator_y + self.stroke_width/2)
        separator_r = (self.width - separator_l[0], separator_l[1])
        ul_l = (self.stroke_width * 1.5, self.corner_radius)
        ul_r = (self.corner_radius, self.stroke_width * 1.5)
        ur_r = (self.width - ul_l[0], ul_l[1])
        ur_l = (self.width - ul_r[0], ul_r[1])
        circle_l = (self.stroke_width * 1.5, self.circle_y)
        circle_r = (self.width - circle_l[0], circle_l[1])
        corner_radius = self.corner_radius - self.stroke_width * 1.5
        lower_radius = self.circle_radius - self.stroke_width * 1.5
        
        upper_outline = self.drawing.path(stroke=self.fg, fill=self.header_bg, stroke_width=self.stroke_width)
        bg_g.add(upper_outline)
        upper_outline.push('M', separator_l)
        upper_outline.push('V', ul_l[1])
        upper_outline.push_arc(ul_r, 0, corner_radius, large_arc=False,
                               angle_dir='+', absolute=True)
        upper_outline.push('H', ur_l[0])
        upper_outline.push_arc(ur_r, 0, corner_radius, large_arc=False,
                               angle_dir='+', absolute=True)
        upper_outline.push('V', separator_r[1])
        upper_outline.push('Z')
    
        lower_outline = self.drawing.path(stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
        bg_g.add(lower_outline)
        lower_outline.push('M', separator_l)
        lower_outline.push('V', circle_l[1])
        lower_outline.push_arc(circle_r, 0, lower_radius, large_arc=False,
                               angle_dir='-', absolute=True)
        lower_outline.push('V', separator_r[1])
        lower_outline.push('Z')
    
        bg_g.add(render_text(
            self.drawing, 'TOLL', self.header_x, self.header_y, self.header_width,
            self.header_height, self.header_series, color=self.fg,
            spacing=self.header_spacing))
    
        fl_x = self.fl_x + self.fl_stroke_width/2
        fl_y = self.fl_y + self.fl_stroke_width/2
        fl_width = self.fl_width - self.fl_stroke_width
        fl_height = self.fl_height - self.fl_stroke_width
        fl = self.drawing.path(stroke=self.fg, fill='none',
                               stroke_width=self.fl_stroke_width)
        fl.push('M', (FL_OUTLINE_POINTS[0][0] * fl_width + fl_x,
                      FL_OUTLINE_POINTS[0][1] * fl_height + fl_y))
        for x, y in FL_OUTLINE_POINTS[1:]:
            fl.push('L', (x * fl_width + fl_x, y * fl_height + fl_y))
        bg_g.add(fl)
        
        return bg_g

class FLTurnpikeShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(FLTurnpikeShield, self).__init__(modifier, ref)
        
        self.width = 30.0
        self.height = 36.0
        self.stroke_width = 0.75
        self.corner_radius = 1.5
        self.fl_y = 3.0
        self.fl_height = 5.0
        self.tp_y = self.fl_y + self.fl_height + 2.5
        self.tp_height = 5.0
        self.text_series = 'B'
        self.text_x = self.stroke_width * 2
        self.text_width = self.width - self.text_x * 2

        self.state_y = self.tp_y + self.tp_height + 2.5
        self.state_height = 15.0
        self.state_scale = self.state_height / STATE_FILLED_HEIGHT
        self.state_width = STATE_FILLED_WIDTH * self.state_scale
        self.state_x = self.width/2 - self.state_width/2
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_fl-turnpike'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()
        
        bg_g.add(self.drawing.rect(
            (self.stroke_width/2, self.stroke_width/2),
            (self.width - self.stroke_width, self.height - self.stroke_width),
            self.corner_radius,
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width))
        
        bg_g.add(render_text(
            self.drawing, 'FLORIDA\'S', self.text_x, self.fl_y, self.text_width,
            self.fl_height, self.text_series, color=self.fg))
        bg_g.add(render_text(
            self.drawing, 'TURNPIKE', self.text_x, self.tp_y, self.text_width,
            self.tp_height, self.text_series, color=self.fg))
    
        bg_g.add(self.drawing.path(
            d=STATE_FILLED, fill=self.fg,
            transform='matrix({0} 0 0 {0} {1} {2})'.format(self.state_scale, self.state_x, self.state_y)))
        
        return bg_g

    def make_ref(self):
        return None
    
FL_OUTLINE_POINTS = [
    (0.54577, 0.24338),
    (0.47725, 0.20224),
    (0.34530, 0.24089),
    (0.21081, 0.13992),
    (0.12707, 0.11748),
    (0.10169, 0.12870),
    (0.02176, 0.12247),
    (0.0,     0.0),
    (0.31485, 0.0),
    (0.33007, 0.03895),
    (0.73482, 0.03895),
    (0.75131, 0.01153),
    (0.83505, 0.01278),
    (0.92640, 0.32814),
    (0.90610, 0.44531),
    (0.93782, 0.62356),
    (0.98477, 0.72577),
    (1.0,     0.79807),
    (0.99745, 0.91025),
    (0.90103, 1.0),
    (0.83885, 1.0),
    (0.77542, 0.97133),
]
STATE_FILLED_HEIGHT = 15.0
STATE_FILLED_WIDTH = 17.0
STATE_FILLED = 'M 0.29082328,3.75e-5 C 0.24462473,0.1725704 0.07643048,0.18470371 0.07550515,0.45443636 0.1602024,0.67003583 0.34301915,0.91603521 0.50694407,0.99030169 0.66655434,1.1444346 0.30403525,1.2929676 0.50366141,1.5203004 0.30641391,1.7436198 0.23151943,2.1376055 0.67223432,1.9975258 1.3353807,1.9544059 1.9720071,1.7100332 2.6277228,1.8284996 3.2572812,1.8609662 3.7841733,2.2412586 4.3306919,2.5142445 4.4987315,2.6852041 4.6440911,2.8975503 4.8866238,2.9580034 5.1104899,3.0729765 5.0310768,3.3769091 5.0861967,3.5014954 5.1484365,3.7243749 5.5016356,3.5877085 5.6574486,3.5122954 5.888608,3.6165885 6.1904472,3.4530289 6.3300736,3.4030023 6.5542063,3.2499227 6.7266059,3.1841095 6.9529787,3.0401899 7.1358315,3.1061364 7.5177373,3.1502563 7.4275775,2.8244704 7.3063245,2.740204 7.4310308,2.5759377 7.6605102,2.6449776 7.9138829,2.5163379 8.0960158,2.6713108 8.2909087,2.8174705 8.4866948,2.9044436 8.8781872,3.1775496 8.7987207,3.2057762 8.8113207,3.4888421 9.1893864,3.4665888 9.2194797,3.7628814 9.1841331,4.1409071 9.6012121,4.0825473 9.6969718,4.3373867 9.7587183,4.4483997 9.9477979,4.7753989 10.140197,4.8912519 10.496543,4.8267721 10.566863,5.0206383 10.680582,5.273371 10.823995,5.4890638 10.685542,5.7787297 10.847422,6.0244891 10.959515,6.481488 10.642716,6.9011669 10.546636,7.3138592 10.650089,7.5143387 10.540156,7.7422981 10.484596,7.8973244 10.357023,8.1795237 10.802062,8.6308293 11.075861,8.5596828 11.138075,8.596696 10.843235,8.7902289 10.791516,8.9138685 10.635223,9.1757746 11.136475,9.2357877 11.072942,9.3531208 11.020208,9.544947 11.149155,9.68428 11.250501,9.9032927 11.340741,10.223559 11.542167,10.505811 11.729993,10.772197 11.985166,10.81605 12.182192,10.84521 12.143365,11.140063 12.186365,11.342982 12.243032,11.611288 12.508245,11.673408 12.735217,11.816941 12.642884,12.202274 12.725044,12.458486 12.86739,12.652899 12.786777,12.992832 13.048537,13.054405 13.328616,13.129245 13.496002,13.045725 13.649735,13.160925 13.806401,13.302964 13.891734,13.595244 14.016667,13.805924 14.133467,14.02414 14.263867,14.260695 14.156667,14.507315 14.153734,14.744814 14.302933,15.062417 14.547466,14.989288 14.843332,14.959933 15.034398,14.913252 15.313731,14.946553 15.511197,14.974786 15.637463,14.739942 15.86093,14.714529 16.101862,14.655881 16.167995,14.440333 16.244262,14.410928 16.415861,14.188215 16.143195,13.910487 16.349195,13.692518 16.478928,13.46067 16.564794,13.328031 16.657194,13.048511 16.798794,12.758059 16.720661,12.387313 16.832394,12.073607 16.845994,11.474249 17.036393,10.854704 16.826394,10.272745 16.573328,9.61508 16.267195,8.9706283 16.098929,8.27699 15.87093,7.8710177 15.646663,7.4599654 15.594797,6.9896732 15.421197,6.4090747 15.396931,5.7670496 15.020532,5.2686242 14.388,4.2194002 14.048934,3.0273498 13.807468,1.8350461 13.799601,1.6075001 13.822401,1.5925669 13.712535,1.3805674 13.706402,1.2505677 13.796135,0.91083522 13.634935,0.90310191 13.579335,0.70936906 13.205256,0.79856884 13.033763,0.70310241 12.85991,0.6663025 12.634471,0.43563641 12.396072,0.53910282 12.162725,0.59990267 12.112366,0.97270174 12.167179,1.120968 12.267765,1.2337677 12.224232,1.4623005 12.165125,1.6292334 11.924579,1.6869398 12.105326,1.4109673 11.979833,1.2959009 11.882806,1.1520346 11.707807,1.0769681 11.421434,1.0991014 10.568436,1.0316349 9.7141451,1.018035 8.8607472,0.95323512 8.1533223,0.96216843 7.4515641,0.85563536 6.7447792,0.84670205 6.5273264,0.83310208 6.0971275,0.90603524 6.1456074,0.59456935 6.0541276,0.41990312 6.0561676,0.06470401 5.752155,0.08377062 4.9374104,0.12737052 4.1201458,0.11550388 3.3054411,0.08377062 2.3660568,0.10217058 1.4312058,0.00283749 0.49166144,0.02257078 0.42472694,0.0149708 0.35776178,0.00750415 0.29082061,3.75e-5 Z M 6.0009424,0.23297025 C 6.013249,0.24097023 5.9726758,0.27110349 6.0009424,0.23297025 Z M 12.984031,0.70096908 C 13.029778,0.69816909 12.993391,0.70443574 12.984031,0.70096908 Z M 13.424537,0.81310213 C 13.426804,0.81230214 13.42867,0.81923545 13.424537,0.81310213 Z M 0.55255596,0.99803501 C 0.56362126,1.0245683 0.54557197,1.0029683 0.55255596,0.99803501 Z M 0.5628226,1.0185683 C 0.61352247,1.012435 0.57296257,1.0305683 0.5628226,1.0185683 Z M 9.9607058,4.609786 C 9.9723725,4.6576392 9.9279059,4.616586 9.9607058,4.609786 Z M 10.674837,5.1657179 C 10.750317,5.1569446 10.673424,5.1922645 10.674837,5.1657179 Z M 12.2543,5.6665967 C 12.496539,5.8708895 12.681592,6.1312222 12.970765,6.2737551 13.122764,6.4928879 13.416937,6.3019684 13.548537,6.5623411 13.673336,6.736354 13.70947,6.9644868 13.824536,7.1632329 13.880002,7.4007123 13.966936,7.6295784 14.208135,7.7464848 14.504268,7.9010311 14.494934,8.2855501 14.769734,8.464763 14.889467,8.6945224 15.0324,8.9685617 15.341866,8.8748286 15.499065,9.1147347 15.845864,8.9928417 15.909997,9.3190542 15.928931,9.65132 16.185997,9.918946 16.317863,10.214065 16.609329,10.444305 16.504929,10.846744 16.40253,11.134223 16.395063,11.469329 16.465063,11.816928 16.403863,12.125061 16.176797,12.309793 16.507996,12.811165 16.111997,12.751659 15.795464,12.925405 15.967331,13.347324 15.923597,13.63787 16.088797,13.89606 15.614531,14.131013 15.906664,13.732854 15.849464,13.489524 15.870931,13.167018 15.903597,12.894512 16.028264,12.635833 16.29933,12.668952 16.271597,12.391087 16.279863,12.160087 16.451729,11.966954 16.37813,11.714035 16.388396,11.414382 16.296396,11.09601 16.443329,10.82641 16.573462,10.512558 16.289996,10.327985 16.193463,10.080106 16.04213,9.8228396 15.881997,9.5620269 15.826264,9.2687477 15.747731,9.0431616 15.461732,9.1583346 15.352399,8.9398818 15.079999,8.9580018 14.869867,8.8499487 14.7668,8.582136 14.603201,8.3882032 14.472134,8.199217 14.375335,7.9671776 14.155735,7.7208982 13.785869,7.5712852 13.791069,7.1897662 13.670936,7.0398599 13.613336,6.7522206 13.55107,6.6531409 13.424004,6.3865415 13.040245,6.5399012 12.905965,6.2693685 12.635192,6.1221022 12.456153,5.8609828 12.199247,5.6974099 L 12.23862,5.6753833 Z M 10.516238,7.8603378 C 10.517784,7.9208044 10.498811,7.8686178 10.516238,7.8603378 Z M 11.246796,8.32383 C 11.188036,8.4929096 11.045796,8.5280695 11.246796,8.32383 Z M 10.77345,8.4520164 C 10.775917,8.4501364 10.780184,8.459603 10.77345,8.4520164 Z M 10.81865,8.478323 C 10.844343,8.5225095 10.79949,8.4903229 10.81865,8.478323 Z M 10.85769,8.5358428 C 10.908263,8.5288162 10.86669,8.5486828 10.85769,8.5358428 Z M 10.915197,8.5654294 C 10.919543,8.5658828 10.914917,8.5672961 10.915197,8.5654294 Z M 15.072133,9.816173 C 15.280399,9.9333193 15.408532,10.224132 15.367465,10.445758 15.273732,10.641664 14.991066,10.85909 14.805734,10.623931 14.550934,10.582211 14.348668,10.336252 14.6836,10.192759 14.8336,10.070172 15.0068,9.9866392 15.072133,9.816173 Z M 12.136807,10.992143 C 12.155153,11.00593 12.13562,10.99909 12.136807,10.992143 Z M 16.226797,13.996576 16.23253,14.00808 Z M 16.271597,14.190515 C 16.272397,14.192818 16.265463,14.194727 16.271597,14.190515 Z'
