#!/usr/bin/python

# https://www.nevadadot.com/home/showdocument?id=4464


import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:NV'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:NV'] = lambda m,r: NVStateShield(m, r, guide=True).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:NV'] = lambda m,r: NVStateShield(m, r, sign=True).make_shield()
    else:
        network_registry['US:NV'] = lambda m,r: NVStateShield(m, r).make_shield()

class NVStateShield(ShieldBase):
    def __init__(self, modifier, ref, guide=False, sign=False):
        super(NVStateShield, self).__init__(modifier, ref)
        assert not (sign and guide)
        self.sign = sign
        self.guide = guide

        if self.guide:
            if len(self.ref) <= 2:
                self.width = 18.0             # A
                self.bottom_width = 16.0      # D
                self.small_arc_radius = 0.5   # H
                self.large_arc_radius = 1.125 # I
                self.ref_series = 'D'
            else:
                self.width = 22.0             # A
                self.bottom_width = 19.556    # D
                self.small_arc_radius = 0.611 # H
                self.large_arc_radius = 1.365 # I
                if len(self.ref) == 3:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
            self.height = 24.0                # B
            self.left_height = 12.0           # C
            self.ref_y = 2.0                  # E
            self.ref_height = 8.0             # F
            right_space_below_ref = 8.0       # G
            self.right_height = self.ref_y + self.ref_height + right_space_below_ref
    
            self.ref_x = 1.5
            self.state_width = self.width
            self.state_height = self.height
        else:
            self.width = 24.0      # A
            self.height = 24.0     # B
            self.ref_y = 3.5       # C
            self.ref_height = 6.0  # D
            self.ref_series = 'C'  # D
            ref_space_below = 4.25 # E
            self.nv_y = self.ref_y + self.ref_height + ref_space_below
            self.nv_height = 3.5   # F
            self.nv_series = 'B'   # F

            self.ref_x = 5.0
            self.state_width = 16.0
            self.state_height = 19.986
            self.bottom_width = 14.511
            self.small_arc_radius = 0.375
            self.large_arc_radius = 0.875
            self.left_height = 8.625
            self.right_height = 16.5
            self.corner_radius = 3.0
            self.nv_x = self.ref_x
            self.nv_width = self.width - self.nv_x * 2

        self.border_width = 0.5
        self.ref_width = self.width - self.ref_x * 2
            
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_nv-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()
        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            border_offset = 0
        else:
            border_offset = self.border_width/2
            
        small_arc_radius = self.small_arc_radius - border_offset
        large_arc_radius = self.large_arc_radius + border_offset

        state_left = self.width/2 - self.state_width/2
        state_top = self.height/2 - self.state_height/2
        lower_tip = (state_left + self.bottom_width - border_offset,
                     state_top + self.state_height - border_offset)
        ll_corner = (state_left + border_offset, state_top + self.left_height)
        ul_corner = (state_left + border_offset, state_top + border_offset)
        ur_corner = (state_left + self.state_width - border_offset,
                     state_top + border_offset)
        lr_corner = (state_left + self.state_width - border_offset,
                     state_top + self.right_height)
        arc_transition = (state_left + self.state_width - border_offset - 2 * small_arc_radius,
                          state_top + self.right_height)
        arc_left = (arc_transition[0] - large_arc_radius * 2,
                    state_top + self.right_height)

        if not self.guide:
            nv_box_ul = (ul_corner[0], self.nv_y - self.border_width - border_offset)
            nv_box_ll = (ul_corner[0], self.nv_y + self.nv_height + self.border_width + border_offset)
            nv_box_top = unpoint(
                intersect_lines(point(*ll_corner), point(*lower_tip),
                                point(*nv_box_ul), point(*nv_box_ul) + 1))
            nv_box_bot = unpoint(
                intersect_lines(point(*ll_corner), point(*lower_tip),
                                point(*nv_box_ll), point(*nv_box_ll) + 1))

        outline = self.drawing.path(fill=self.bg)
        if not self.sign:
            outline['stroke'] = self.fg
            outline['stroke-width'] = self.border_width
        outline.push('M', lower_tip)
        if not self.guide:
            outline.push('L', nv_box_bot)
            outline.push('L', nv_box_ll)
            outline.push('L', nv_box_ul)
            outline.push('L', nv_box_top)
        outline.push('L', ll_corner)
        outline.push('V', ul_corner[1])
        outline.push('H', ur_corner[0])
        outline.push('V', lr_corner[1])
        outline.push_arc(arc_transition, 0, small_arc_radius, angle_dir='+',
                         large_arc=False, absolute=True)
        outline.push_arc(arc_left, 0, large_arc_radius, angle_dir='-',
                         absolute=True)
        outline.push('Z')

        if self.guide:
            return outline

        bg_g.add(outline)
        bg_g.add(render_text(
            self.drawing, 'NEVADA', self.nv_x, self.nv_y, self.nv_width,
            self.nv_height, self.nv_series, self.fg))
        return bg_g
