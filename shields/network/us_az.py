#!/usr/bin/python

# https://apps.azdot.gov/files/traffic/moas/pdf/m/m01-005a.pdf
# https://apps.azdot.gov/files/traffic/moas/pdf/m/m01-005b.pdf

import math
import svgwrite

from ..banner import banner
from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:AZ'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:AZ'] = lambda m,r: AZStateShield(m, r, style).make_shield()

class AZStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(AZStateShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        self.sign = style == STYLE_SIGN
        
        if len(self.ref) <= 2:
            self.width = 24.0 # A
            self.notch_height = 3.25 # D
            self.notch_width = 3.75 # H
            self.left_height = 16.0 # F
            self.diagonal_left_offset_x = 4.0 # G
            az_offset_x1 = 5.375 # K
            az_offset_x2 = 9.125 # L
            self.az_height = 2.5 # N
            self.az_series = 'D' # N
            self.ref_series = 'D'
        else:
            self.width = 30.0 # A
            self.notch_width = 4.75 # D
            self.notch_height = 3.5 # G
            self.left_height = 16.25 # F
            self.diagonal_left_offset_x = 3.5 # G
            az_offset_x1 = 6.125 # K
            az_offset_x2 = 10.875 # L
            self.az_height = 2.5 # M
            self.az_series = 'E' # M
            if len(self.ref) == 3 and '1' in self.ref:
                self.ref_series = 'D'
            elif len(self.ref) == 3:
                self.ref_series = 'C'
            else:
                self.ref_series = 'B'
        self.height = 24.0 # B
        if self.guide:
            self.border_width = 0.5
        else:
            self.border_width = 0.75 # C
        self.diagonal_right_offset_y = 3.0 # E
        self.corner_radius = 1.5 # N
        ref_space_above = 2.5 # P
        self.az_y = 2.0 # Q
        self.ref_height = 12.0 # R

        self.az_x = self.border_width + self.notch_width + self.border_width
        self.az_width = self.width - self.az_x - self.border_width * 2
        self.ref_y = self.az_y + self.az_height + ref_space_above

        self.ref_x = 2 * self.border_width
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_az-{}'.format(int(self.width))
    
    def make_shield(self):
        if self.ref is None:
            return None
        
        if self.modifier == '' or self.modifier == 'LOOP':
            return self.make_base_shield()
        else:
            shield_svg = self.make_base_shield()
            banner_svgs = [banner(mod, int(shield_svg['width']), fg=self.fg, bg=self.bg)
                           for mod in self.modifier.split(';')]
            return compose_shields(banner_svgs + [shield_svg])

    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            outline = self.drawing.path(fill=self.bg)
            border_offset = self.border_width
        else:
            outline = self.drawing.path(stroke=self.fg, fill=self.bg,
                                        stroke_width=self.border_width)
            border_offset = self.border_width/2
        bg_g.add(outline)
        outline.push('M', (border_offset, self.border_width + self.notch_height + self.left_height))
        outline.push('V', self.border_width + self.notch_height - border_offset)
        outline.push('H', self.border_width + self.notch_width - border_offset)
        outline.push('V', border_offset)
        outline.push('H', self.width - border_offset)
        outline.push('V', self.height - border_offset)
        outline.push('H', self.width/2 + self.diagonal_left_offset_x)
        outline.push('Z')

        if self.modifier == 'LOOP':
            header_text = self.modifier
        else:
            header_text = 'ARIZONA'
        if self.modifier == 'LOOP' or not self.guide:
            bg_g.add(render_text(
                self.drawing, header_text, self.az_x, self.az_y, self.az_width,
                self.az_height, self.az_series, self.fg))
        
        return bg_g
