#!/usr/bin/python
# -*- coding: utf-8 -*-

# I haven't been able to find any documentation on the design of Bergen
# County's guidance signs.  I'm basing the below on my own judgements
# based on the shield SVGs on the Wikimedia Commons.


import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield, STYLE_ELONGATED
from .us_county import USCountyShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:NJ'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:NJ:BERGEN'] = lambda m,r: USCountyShield(m, r, 'BERGEN').make_shield()
    else:
        network_registry['US:NJ'] = lambda m,r: us_nj_shield(m, r, style)
        network_registry['US:NJ:BERGEN'] = lambda m,r: NJBergenCountyShields(m, r).make_shield()

NJ_SPECIAL_REFS = [
    ('ACBC', 'Atlantic City-Brigantine Connector'),
    ('ACX', 'Atlantic City Expressway'),
    ('GSP', 'Garden State Parkway'),
    ('NJTP', 'New Jersey Turnpike'),
    ('PIP', 'Palisades Interstate Parkway'),
]
def us_nj_shield(modifier, ref, style):
    for refs in NJ_SPECIAL_REFS:
        for r in refs:
            if ref.upper() == r.upper():
                return read_template('US:NJ-' + refs[0].upper())
    return USGenericStateShield(modifier, ref, STYLE_ELONGATED, sign=style == STYLE_SIGN).make_shield()

class NJBergenCountyShields(ShieldBase):
    def __init__(self, modifier, ref):
        super(NJBergenCountyShields, self).__init__(modifier, ref)
        
        self.width = 24.0
        self.height = 24.0
        self.stroke_width = 0.5
        self.name_y = 2.0
        self.name_height = 3.0
        self.name_series = 'D'
        self.ref_y = 6.5
        self.ref_height = 11.0
        self.county_y = 19.0
        self.county_height = 3.0
        self.county_series = 'D'
        self.corner_radius = 1.5

        self.ref_x = self.stroke_width * 3
        self.ref_width = self.width - 2 * self.ref_x
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width)

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        
    @property
    def blank_key(self):
        return 'us_nj-BERGEN'
    
    def make_blank_shield(self):
        background_g = self.drawing.g()
    
        outline = self.drawing.rect(
            (self.stroke_width * 1.5, self.stroke_width * 1.5),
            (self.width - self.stroke_width * 3, self.height - self.stroke_width * 3),
            self.corner_radius - self.stroke_width * 1.5,
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
        background_g.add(outline)
        
        name_left = self.stroke_width * 3
        name_width = self.width - 2*name_left
        name_g = render_text(self.drawing, 'BERGEN', name_left, self.name_y, name_width, self.name_height, self.name_series, color=self.fg)
        background_g.add(name_g)
    
        county_left = self.stroke_width * 3
        county_width = self.width - 2*county_left
        county_g = render_text(self.drawing, 'COUNTY', county_left, self.county_y, county_width, self.county_height, self.county_series, color=self.fg)
        background_g.add(county_g)
        
        return background_g

def make_ref(drawing, ref):
        
    ref_g = render_text(drawing, ref, ref_left, PARAMS.ref_y, available_width, PARAMS.ref_height, ref_series, color=COLOR_BLACK)

    return ref_g
