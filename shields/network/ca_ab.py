#!/usr/bin/python

# Index:
#   http://www.transportation.alberta.ca/Content/docType252/Production/IB_ROUTE-TRAILBLAZER.PDF
# Images:
#  ftp://ftp.gov.ab.ca/transportation/Highway_Sign_Patterns/Information%20Signs%20(I)/IB/
#
# Standard sign variants are 450x600mm (4x) and 600x750mm (5x).  Some
# signs have other standards, but the 5x size is the best fit for the
# dimensions we're working with.
#
# | Params               | Guide | Guide | Indep | Indep |
# | Digits               |   1,2 |     3 |   1,2 |     3 |
# |----------------------+-------+-------+-------+-------|
# | width                |   546 |   600 |   600 |   600 |
# | height               |   560 |   540 |   750 |   750 |
# | border               |    19 |    19 |    19 |    19 |
# | ref_height           |   254 |   203 |   250 |   200 |
# | side_height          |   370 |   354 |   346 |   346 |
# | shield_corner_radius |   220 |   210 |   175 |   175 |
# | shield_y             |       |       |   220 |   220 |
# | shield_width         |       |       |   500 |   500 |
# | ref_y                |   110 |   130 |   315 |       |
# | corner_radius        |       |       |    37 |    37 |
# 
# ref_series
# | Digits | Series | FHWA Equivalent |
# |--------+--------+-----------------|
# |      1 | 3-B    | D               |
# |      2 | 2-B    | C               |
# |      3 | 2-B    | C               |

import math
import re
import svgwrite

from ..geometry import *
from ..shieldbase import ShieldBase
from ..utils import *

def register(network_registry, style):
    network_registry['CA:AB'] = lambda m,r: make_ab_shield(m, r, style)
    network_registry['CA:AB:PRIMARY'] = lambda m,r: ABProvinceShield(m, r, style).make_shield()

ALBERTA_TEMPLATE = 'CA:AB-Alberta'
TCH_TEMPLATE = 'CA:AB-TCH'
FIRST_SECONDARY_ROUTE = 500

def make_ab_shield(modifier, ref, style):
    if ref in ('1', '16'):
        return ABTransCanadaShield(modifier, ref, style).make_shield()
    m = re.search(r'\d+', ref)
    if m is None or int(m.group()) < FIRST_SECONDARY_ROUTE:
        return ABProvinceShield(modifier, ref, style).make_shield()
    else:
        return ABSecondaryShield(modifier, ref, style).make_shield()

def make_logo(drawing, x, y, width, height, color):
    alberta_logo = read_and_position_template(ALBERTA_TEMPLATE, x, y, width, height)
    alberta_logo['fill'] = color
    return alberta_logo
    
class ABProvinceShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(ABProvinceShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        self.sign = style == STYLE_SIGN

        # All dimensions in mm
        self.scale = 1/25.4

        self.border_width = 19.0
        
        if self.guide:
            if self.narrow:
                self.width = 546.0
                self.height = 560.0
                self.ref_height = 254.0
                self.side_height = 370.0
                self.shield_corner_radius = 110.0
                self.ref_y = 110.0
            else:
                self.width = 600.0
                self.height = 540.0
                self.ref_height = 203.0
                self.side_height = 354.0
                self.shield_corner_radius = 105.0
                self.ref_y = 130.0
            self.ref_x = self.border_width * 3
            self.shield_y = self.border_width
            self.shield_width = self.width - self.border_width * 2
            self.shield_height = self.height - self.border_width * 2
        else:
            if self.narrow:
                self.ref_height = 250.0
            else:
                self.ref_height = 200.0
            self.width = 600.0
            self.height = 750.0
            self.side_height = 346.0
            self.shield_corner_radius = 87.5
            self.shield_y = 220.0
            self.shield_width = 500.0
            self.shield_height = 512
            ref_centerline = 440.0
            self.ref_y = ref_centerline - self.ref_height/2
            self.corner_radius = 37.0
            self.ref_x = self.width/2 - self.shield_width/2 + self.border_width * 2
            self.logo_height = 160.0

        if len(self.ref) == 1:
            self.ref_series = 'D'
        else:
            self.ref_series = 'C'
        self.ref_width = self.width - self.ref_x * 2

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'ca_ab-{}'.format(int(self.width))

    def make_blank_shield(self):
        if self.guide:
            return self.make_guide_blank()
        else:
            return self.make_sign_blank()

    def make_guide_blank(self):
        bg_g = self.drawing.g()

        corner_arc = Circle(
            point(self.shield_corner_radius, self.side_height),
            self.shield_corner_radius)
        lower_point = (self.width/2, self.height)
        ll_line, ll_transition = corner_arc.tangent_line_from_point(
            point(*lower_point), left=False, include_intersection=True)
        l_arc_transition_bottom = unpoint(ll_transition)
        l_arc_transition_side = (0, self.side_height)
        ul_corner = (0, 0)
        ur_corner = (self.width - ul_corner[0], ul_corner[1])
        r_arc_transition_side = (self.width - l_arc_transition_side[0], l_arc_transition_side[1])
        r_arc_transition_bottom = (self.width - l_arc_transition_bottom[0], l_arc_transition_bottom[1])
        bg = self.drawing.path(fill=self.bg)
        bg.push('M', lower_point)
        bg.push('L', r_arc_transition_bottom)
        bg.push_arc(r_arc_transition_side, 0, corner_arc.radius, large_arc=False,
                    angle_dir='-', absolute=True)
        bg.push('V', ur_corner[1])
        bg.push('H', ul_corner[0])
        bg.push('V', l_arc_transition_side[1])
        bg.push_arc(l_arc_transition_bottom, 0, corner_arc.radius, large_arc=False,
                    angle_dir='-', absolute=True)
        bg.push('Z')
        bg_g.add(bg)

        bg_g.add(self.make_shield_outline())

        return bg_g

    def make_sign_blank(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.bg))

        bg_g.add(self.make_shield_outline())

        bg_g.add(make_logo(
            self.drawing,
            self.width/2 - self.shield_width/2,
            (self.shield_y - self.logo_height) / 2,
            self.shield_width,
            self.logo_height,
            self.fg))
        
        return bg_g

    def make_shield_outline(self):
        if self.sign:
            corner_radius = self.shield_corner_radius
            side_height = self.side_height
        else:
            corner_radius = self.shield_corner_radius - self.border_width
            side_height = self.side_height - self.border_width
        border_offset = self.border_width/2

        shield_left = self.width/2 - self.shield_width/2
        outer_corner_arc = Circle(
            point(shield_left + corner_radius, self.shield_y + side_height),
            corner_radius)
        outer_lower_point_c = point(self.width/2, self.shield_y + self.shield_height)
        outer_ll_line, outer_l_arc_transition_c = outer_corner_arc.tangent_line_from_point(
            outer_lower_point_c, left=False, include_intersection=True)
        outer_r_arc_transition_c = (self.width - outer_l_arc_transition_c).conjugate()
        outer_lr_line = Line.from_points(
            outer_lower_point_c, outer_r_arc_transition_c)

        corner_arc = outer_corner_arc.add_radius(-border_offset)
        ll_line = outer_ll_line.translate(
            unit_vector(outer_lower_point_c, outer_l_arc_transition_c) * -1j * border_offset)
        lr_line = outer_lr_line.translate(
            unit_vector(outer_lower_point_c, outer_r_arc_transition_c) * 1j * border_offset)
        lower_point = unpoint(ll_line.intersect_line(lr_line))
        ignored, ll_transition_c = corner_arc.tangent_line_from_point(
            point(*lower_point), left=False, include_intersection=True)
        l_arc_transition_bottom = unpoint(ll_transition_c)
        l_arc_transition_side = (shield_left + border_offset, self.shield_y + side_height)
        ul_corner = (shield_left + border_offset, self.shield_y + border_offset)
        ur_corner = (self.width - ul_corner[0], ul_corner[1])
        r_arc_transition_side = (self.width - l_arc_transition_side[0], l_arc_transition_side[1])
        r_arc_transition_bottom = (self.width - l_arc_transition_bottom[0], l_arc_transition_bottom[1])

        border = self.drawing.path(
            fill=self.bg, stroke=self.fg, stroke_width=self.border_width)
        border.push('M', lower_point)
        border.push('L', r_arc_transition_bottom)
        border.push_arc(r_arc_transition_side, 0, corner_arc.radius, large_arc=False,
                    angle_dir='-', absolute=True)
        border.push('V', ur_corner[1])
        border.push('H', ul_corner[0])
        border.push('V', l_arc_transition_side[1])
        border.push_arc(l_arc_transition_bottom, 0, corner_arc.radius, large_arc=False,
                    angle_dir='-', absolute=True)
        border.push('Z')

        return border

class ABSecondaryShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(ABSecondaryShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        self.sign = style == STYLE_SIGN

        # All dimensions in mm
        self.scale = 1/25.4

        self.border_width = 10.0

        if self.guide:
            self.width = 600.0
            self.height = 420.0
            self.shield_width = 580.0
            self.shield_height = 400.0
            self.ref_height = 200.0
            self.shield_y = 10.0
        else:
            self.width = 600.0
            self.height = 750.0
            self.shield_width = 500.0
            self.shield_height = 350.0
            self.ref_height = 160.0
            self.shield_y = 300.0
        self.corner_radius = 37.0

        self.shield_center = (self.width/2, self.shield_y + self.shield_height/2)
        
        self.ref_y = self.shield_y + self.shield_height/2 - self.ref_height/2
        self.ref_x = self.width/2 - self.shield_width/2
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = 'C'

        self.logo_height = 160.0

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'ca_ab-secondary'

    def make_blank_shield(self):
        if self.guide:
            return self.make_guide_blank()
        else:
            return self.make_sign_blank()

    def make_guide_blank(self):
        border_offset = self.border_width/2
        return self.drawing.ellipse(
            self.shield_center,
            (self.shield_width/2 - border_offset, self.shield_height/2 - border_offset),
            fill=self.bg, stroke=self.fg, stroke_width=self.border_width)

    def make_sign_blank(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.bg))

        bg_g.add(self.make_guide_blank())

        bg_g.add(make_logo(
            self.drawing,
            self.width/2 - self.shield_width/2,
            (self.shield_y - self.logo_height) / 2,
            self.shield_width,
            self.logo_height,
            self.fg))
        
        return bg_g

class ABTransCanadaShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(ABTransCanadaShield, self).__init__(modifier, ref)

        # All dimensions in mm
        self.scale = 1/25.4

        self.width = 600.0
        self.height = 750.0

        self.border_width = 15.0
        self.corner_radius = 37.0

        if len(self.ref) <= 1:
            self.ref_y = 290.0
            self.ref_x = 225.0
            self.ref_width = 150.0
            self.ref_height = 185.0
        else:
            self.ref_y = 333.0
            self.ref_x = 200.0
            self.ref_width = 200.0
            self.ref_height = 160.0
        self.ref_series = 'C'

        self.fg = COLOR_GREEN
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'ca_ab-transcanada'

    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect(
            (0, 0), (self.width, self.height), self.corner_radius,
            fill=self.bg))

        image = read_and_position_template(
            TCH_TEMPLATE,
            self.border_width, self.border_width,
            self.width - self.border_width * 2,
            self.height - self.border_width * 2)
        image['fill'] = self.fg
        bg_g.add(image)
        
        return bg_g
