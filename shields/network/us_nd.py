#!/usr/bin/python

# http://www.dot.nd.gov/business/bidopenings/20180209-0930/Job%2020/HES-6-999(036)%20Final%20Plans%201of2%20ED.pdf#page=91

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:ND'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:ND'] = lambda m,r: NDStateShield(m, r, style).make_shield()

class NDStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(NDStateShield, self).__init__(modifier, ref)
        self.cutout = style == STYLE_CUTOUT
        
        if len(self.ref) <= 2:
            self.width = 24.0    # A
            self.border_width = 0.38 # C
            self.nd_series = 'B' # K
            self.ref_series = 'D'
            self.outline = OUTLINE_2
            if not '1' in self.ref:
                self.ref_spacing = 0.75
        else:
            self.width = 30.0    # A
            self.border_width = 1.13 # C
            self.nd_series = 'C' # K
            self.outline = OUTLINE_3
            if len(self.ref) == 3 and '1' in self.ref:
                self.ref_series = 'D'
            elif len(self.ref) == 3:
                self.ref_series = 'C'
                self.ref_spacing = 0.75
            else:
                self.ref_series = 'B'
                self.ref_spacing = 0.50
        self.height = 24.0       # B
        self.nd_y = 2.0          # F
        self.nd_x = 2.0          # F
        self.nd_width = self.width - self.nd_x * 2
        self.ref_y = 8.5         # G
        self.ref_height = 12.0   # H
        self.nd_height = 3.0     # K
        self.corner_radius = 1.5 # O

        self.ref_x = 1.5
        ref_space_right = 3.5
        self.ref_width = self.width - self.ref_x - ref_space_right
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_nd-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.cutout:
            bg_g.add(self.drawing.path(
                d=self.outline, fill=self.bg, stroke=self.fg,
                stroke_width=self.border_width, stroke_linejoin='round'))
            bg_g.add(render_text(
                self.drawing, 'NORTH DAKOTA', self.nd_x, self.nd_y, self.nd_width,
                self.nd_height, self.nd_series, color=self.fg))
        else:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(self.drawing.path(d=self.outline, fill=self.bg))
            bg_g.add(render_text(
                self.drawing, 'NORTH DAKOTA', self.nd_x, self.nd_y, self.nd_width,
                self.nd_height, self.nd_series, color=self.bg))
        
        return bg_g
    
OUTLINE_2 = 'm1.1962 6.9867h19.26c0.047 0.3845 0.235 0.741 0.262 1.1252 0.2 0.261 0.301 0.5648 0.156 0.8741-0.063 0.4356 0.145 0.8623 0.012 1.296 0.147 0.261-0.046 0.624 0.043 0.931 0.097 0.31 0.422 0.537 0.421 0.908 0.074 0.352 0.155 0.689 0.381 0.981 0.106 0.378 0.364 0.711 0.363 1.102 0.037 0.293-0.011 0.645 0.048 0.968 0.03 0.505 0.12 1.011 0.134 1.51 0.104 0.315 0.372 0.494 0.245 0.856-0.068 0.309-0.061 0.656-0.047 0.961 0.187 0.369 0.23 0.812 0.457 1.149 0.267 0.242 0.406 0.534 0.364 0.897 0.095 0.389 0.216 0.775 0.134 1.178 0.298 0.434-0.418 0.186-0.668 0.25h-22.261c0.2322-4.995 0.46427-9.99 0.6963-14.986z'
OUTLINE_3 = 'm1.2594 7.0211h24.956c0.053 0.3884 0.247 0.7482 0.294 1.1332 0.325 0.312 0.213 0.6882 0.149 1.0727 0.076 0.376 0.061 0.788 0.069 1.14 0.06 0.301-0.105 0.723 0.047 1.03 0.531 0.447 0.372 1.237 0.819 1.739 0.122 0.379 0.393 0.709 0.401 1.103 0.02 0.351 0.006 0.774 0.068 1.158 0.07 0.459 0.072 0.937 0.156 1.381 0.235 0.322 0.32 0.639 0.185 1.039 0.002 0.411-0.07 0.791 0.183 1.159 0.119 0.388 0.267 0.744 0.591 1.011 0.174 0.337 0.091 0.793 0.262 1.162-0.114 0.294 0.297 0.975-0.16 0.859h-28.778c0.25248-4.996 0.5056-9.991 0.7587-14.987z'
