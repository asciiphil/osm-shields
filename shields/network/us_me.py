#!/usr/bin/python

# http://maine.gov/mdot/contractors/publications/standarddetail/docs/2014/SECTION600.pdf#page=144

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:ME'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:ME'] = lambda m,r: MEStateShield(m, r).make_shield()

class MEStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(MEStateShield, self).__init__(modifier, ref)

        if len(self.ref) <= 2:
            self.width = 24.0
        else:
            self.width = 30.0

        self.height = 24.0
        self.border = 3.0/8.0
        self.stroke_width = 5.0/8.0
        self.ref_height = 12.0
        self.ref_y = 6.0
        self.corner_radius = 1.5  # assumed

        self.ref_x = self.border + self.stroke_width * 2
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_me-{}'.format(int(self.width))

    def make_blank_shield(self):
        background_g = self.drawing.g()
    
        corner = (self.border + self.stroke_width/2, self.border + self.stroke_width/2)
        dims = (self.width - corner[0] * 2, self.height - corner[1] * 2)
        radius = self.corner_radius - self.border - self.stroke_width/2
        background_g.add(
            self.drawing.rect(corner, dims, radius,
                              stroke=self.fg, fill=self.bg,
                              stroke_width=self.stroke_width))
        
        return background_g
