#!/usr/bin/python

# http://www.ct.gov/dot/lib/dot/documents/dtrafficdesign/ctdot_traffic_gs.pdf#page=4
#
# Unfortunately, the above doesn't give anything in the way of dimensions
# of the shield.  The parameters below are based on WikiMedia Commons SVGs
# and photos.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:CT'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:CT'] = lambda m,r: CTStateShield(m, r).make_shield()

class CTStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(CTStateShield, self).__init__(modifier, ref)

        if len(self.ref) <= 2:
            self.width = 24.0
            self.ref_series = 'D'
        else:
            self.width = 30.0
            if len(self.ref) == 3:
                if '1' in self.ref:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
            else:
                self.ref_series = 'B'
        self.height = 24.0
        self.border_width = 1.5
        self.corner_radius = 1.5

        self.ref_height = 12.0
        self.ref_y = self.height/2 - self.ref_height/2
        self.ref_x = self.border_width * 1.5
        self.ref_width = self.width - self.ref_x * 2

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ct-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        offset = self.border_width/2
        return self.drawing.rect(
            (offset, offset), (self.width - offset * 2, self.height - offset * 2),
            self.corner_radius - offset, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width)
