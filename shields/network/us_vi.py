#!/usr/bin/python

# The US Virgin Islands apparently use the standard US state design for
# territory highway routes.

from ..us_state import USGenericStateShield, STYLE_ELONGATED
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:VI'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:VI'] = lambda m,r: USGenericStateShield(m, r, STYLE_ELONGATED, sign=style == STYLE_SIGN).make_shield()
