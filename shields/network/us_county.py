#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Standard US county shields.  (M1-6)

"""

# There's only one size for county shields: 24x24.  We adapt to that by
# dynamically shrinking the font series to fit in the available space.


import math
import future
import svgwrite

from ..shieldbase import ShieldBase
from ..utils import *

def make_county_func(county, cls, **args):
    return lambda m,r: cls(m, r, county, **args).make_shield()

def make_mi_zone_county_func(zone):
    return lambda m,r: USCountyShield(m, zone + r, '').make_shield()

def add_counties(registry, state, counties, cls, cr=True):
    for county in counties:
        c = county.upper()
        registry['US:{}:{}'.format(state, c)] = make_county_func(county, cls)
        if cr:
            registry['US:{}:CR:{}'.format(state, c)] = make_county_func(county, cls)
        if ' ' in county:
            c_ = c.replace(' ', '_')
            registry['US:{}:{}'.format(state, c_)] = make_county_func(county, cls)
            if cr:
                registry['US:{}:CR:{}'.format(state, c_)] = make_county_func(county, cls)
    
def register(network_registry, style):
    network_registry['US:AZ:CR'] = lambda m,r: USCountyShield(m, r, '').make_shield()
    network_registry['US:CA:CR'] = lambda m,r: USCountyShield(m, r, '').make_shield()
    network_registry['US:NJ:CR'] = lambda m,r: USCountyShield(m, r, '').make_shield()
    network_registry['US:IA:COUNTY'] = lambda m,r: USCountyShield(m, r, '').make_shield()
    add_counties(network_registry, 'AZ', AZ_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'CA', CA_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'CO', CA_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'FL', FL_COUNTIES, USCountyShield)
    add_counties(network_registry, 'IA', IA_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'IL', IL_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'KS', KS_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'MN', MN_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'ND', ND_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'NJ', NJ_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'NY', NY_COUNTIES, USCountyShield)
    add_counties(network_registry, 'SD', SD_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'TX', TX_COUNTIES, USCountyShield, cr=False)
    add_counties(network_registry, 'UT', UT_COUNTIES, USCountyShield)
    add_counties(network_registry, 'WY', WY_COUNTIES, USCountyShield, cr=False)

    # Michigian has a lettered zone system.  Zones are A-H, with two-digit
    # numbers following.  There appear to be two dominant tagging schemes.
    # County route B-23 (signed as "B23") might be tagged US:MI:CR;B23 or
    # US:MI:B;23.
    # Also, some counties (in the MI_COUNTIES list) don't take part in the
    # zone system.
    for zone in 'ABCDEFGH':
        network_registry['US:MI:' + zone] = make_mi_zone_county_func(zone)
    network_registry['US:MI:CR'] = lambda m,r: USCountyShield(m, r, '').make_shield()
    add_counties(network_registry, 'MI', MI_COUNTIES, USCountyShield)
    
    # Ohio uses county abbreviations in their county relations.  Also,
    # they have a variety of shields in use.
    for key, name, cls, args in OH_COUNTIES:
        if args is None:
            network_registry['US:OH:' + key.upper()] = make_county_func(name, cls)
        else:
            network_registry['US:OH:' + key.upper()] = make_county_func(name, cls, **args)

class USCountyShield(ShieldBase):
    def __init__(self, modifier, ref, county):
        super(USCountyShield, self).__init__(modifier, ref)
        self.county = county.upper()
        
        self.height = 24.0                       # A
        self.bottom_width = 14.188               # B
        self.stroke_width = 0.75                 # C
        self.name_y = 5.0                        # D
        self.name_height = 2.0                   # E
        self.name_series = 'D'                   # E
        self.county_height = 2.0                 # E
        self.county_series = 'D'                 # E
        self.ref_space_around = 2.0              # F
        self.ref_height = 8.0                    # G
        self.name_space_below = 3.0              # H
        self.arc_center_y = 6.625                # J
        self.transition_y = 5.125                # K
        self.arc_center_to_transition_x = 3.125  # L
        self.arc_center_to_shield_center = 9.313 # M
        self.county_left_width = 5.0             # P
        self.county_right_width = 5.188          # Q
        self.side_arc_radius = 2.688             # R
        self.top_arc_radius = 5.313              # S
        
        self.width = 2 * (self.arc_center_to_shield_center + self.side_arc_radius)
        self.ref_y = self.name_y + self.name_height + self.ref_space_around
        self.county_y = self.ref_y + self.ref_height + self.ref_space_around

        self.calculate_ref_params()
        
        self.fg = COLOR_YELLOW
        self.bg = COLOR_BLUE

    @property
    def blank_key(self):
        return 'us_county-pentagon-{}'.format(self.county.replace(' ', '_'))
    
    def calculate_ref_params(self):
        ll_corner = self.width/2 - self.bottom_width/2 + \
                    self.height * 1j
        ul_corner = self.width/2 - self.arc_center_to_shield_center - self.arc_center_to_transition_x + \
                    self.transition_y * 1j

        # Get a unit vector, rotate it 90 degrees clockwise, make it as long
        # as two strokes, then use it to shift the boundary line.
        ul_ll_unit_vector = (ul_corner - ll_corner) / abs(ll_corner - ul_corner)
        translation_vector = ul_ll_unit_vector * 1j * self.stroke_width * 2
        ll_corner += translation_vector
        ul_corner += translation_vector

        ll_text_corner_c = intersect_lines(ul_corner, ll_corner,
                                           self.width + (self.ref_y + self.ref_height) *1j,
                                           0 + (self.ref_y + self.ref_height) *1j)
        self.ref_x = ll_text_corner_c.real
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])

    def make_blank_shield(self):
        background_g = self.drawing.g()

        l_arc_center_c = self.width/2 - self.arc_center_to_shield_center + \
                         self.arc_center_y * 1j
        c_arc_center_c = self.width/2 + \
                         self.top_arc_radius * 1j
        left_tangent_points = line_tangent_to_two_circles(
            l_arc_center_c, self.side_arc_radius - self.stroke_width/2,
            c_arc_center_c, self.top_arc_radius - self.stroke_width/2)
    
        ll_corner = (self.width/2 - self.bottom_width/2, self.height - self.stroke_width/2)
        lr_corner = (self.width/2 + self.bottom_width/2, self.height - self.stroke_width/2)

        l_arc_lower_c = line_tangent_to_one_circle(
            ll_corner[0] + ll_corner[1]*1j, l_arc_center_c,
            self.side_arc_radius - self.stroke_width/2, False)
    
        l_arc_lower = (l_arc_lower_c.real, l_arc_lower_c.imag)
        r_arc_lower = (self.width - l_arc_lower[0], l_arc_lower[1])
        l_arc_upper = (left_tangent_points[0].real, left_tangent_points[0].imag)
        r_arc_upper = (self.width - l_arc_upper[0], l_arc_upper[1])
        top_arc_left = (left_tangent_points[1].real, left_tangent_points[1].imag)
        top_arc_right = (self.width - top_arc_left[0], top_arc_left[1])
    
        outline = self.drawing.path(stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
        background_g.add(outline)
        outline.push('M', lr_corner)
        outline.push('L', r_arc_lower)
        outline.push_arc(r_arc_upper, 0, self.side_arc_radius - self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('L', top_arc_right)
        outline.push_arc(top_arc_left, 0, self.top_arc_radius - self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('L', l_arc_upper)
        outline.push_arc(l_arc_lower, 0, self.side_arc_radius - self.stroke_width/2,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('L', ll_corner)
        outline.push('Z')

        if self.county != '':
            # FIXME: More precise calculations.
            name_left = self.width/2 - self.arc_center_to_shield_center
            name_width = self.width - 2*name_left
            name_g = render_text(self.drawing, self.county, name_left, self.name_y,
                                 name_width, self.name_height, self.name_series,
                                 color=self.fg)
            background_g.add(name_g)

        county_left = self.width/2 - self.county_left_width
        county_width = self.county_left_width + self.county_right_width
        county_g = render_text(self.drawing, 'COUNTY', county_left, self.county_y,
                               county_width, self.county_height, self.county_series,
                               color=self.fg)
        background_g.add(county_g)
    
        return background_g

class USCountySquareShield(ShieldBase):
    def __init__(self, modifier, ref, county, bg=COLOR_WHITE, level='COUNTY'):
        super(USCountySquareShield, self).__init__(modifier, ref)
        self.county = county.upper()
        self.level = level.upper()
        
        self.height = 24.0        # A
        self.width = 24.0         # A
        self.border = 0.375       # B
        self.stroke_width = 0.625 # C
        self.name_y = 3.0         # D
        self.name_height = 3.0    # E
        ref_space = 2.0           # F
        self.ref_height = 8.0     # G
        self.county_height = 3.0  # H
        self.county_series = 'D'  # H
        self.corner_radius = 1.5  # L
        
        self.ref_x = self.border * 2 + self.stroke_width
        self.ref_y = self.name_y + self.name_height + ref_space
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width,
                                          ['E', 'D', 'C', 'B'])
        self.name_x = self.ref_x
        self.name_width = self.ref_width
        self.name_series = series_for_text(self.county, self.name_height, self.name_width,
                                           ['D', 'C', 'B'])
        self.county_x = self.ref_x
        self.county_y = self.ref_y + self.ref_height + ref_space
        self.county_width = self.ref_width

        self.bg = bg
        if self.bg in (COLOR_WHITE, COLOR_YELLOW):
            self.fg = COLOR_BLACK
        else:
            self.fg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_county-square-{}-{}-{}'.format(self.county, self.level, self.bg[1:])
    
    def make_blank_shield(self):
        background_g = self.drawing.g()

        bg_ul = (self.border + self.stroke_width/2, self.border + self.stroke_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius - self.border - self.stroke_width/2
        background_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width))

        if self.county != '':
            background_g.add(render_text(
                self.drawing, self.county, self.name_x, self.name_y,
                self.name_width, self.name_height, self.name_series,
                color=self.fg))

        background_g.add(render_text(
            self.drawing, self.level, self.county_x, self.county_y,
            self.county_width, self.county_height, self.county_series,
            color=self.fg))
    
        return background_g

AZ_COUNTIES = [
    'Apache',
    'Cochise',
    'Coconino',
    'Gila',
    'Graham',
    'Greenlee',
    'La Paz',
    'Maricopa',
    'Mohave',
    'Navajo',
    'Pima',
    'Pinal',
    'Santa Cruz',
    'Yavapai',
    'Yuma',
]
CA_COUNTIES = [
    'Alameda',
    'Alpine',
    'Amador',
    'Butte',
    'Calaveras',
    'Colusa',
    'Contra Costa',
    'Del Norte',
    'El Dorado',
    'Fresno',
    'Glenn',
    'Humboldt',
    'Imperial',
    'Inyo',
    'Kern',
    'Kings',
    'Lake',
    'Lassen',
    'Los Angeles',
    'Madera',
    'Marin',
    'Mariposa',
    'Mendocino',
    'Merced',
    'Modoc',
    'Mono',
    'Monterey',
    'Napa',
    'Nevada',
    'Orange',
    'Placer',
    'Plumas',
    'Riverside',
    'Sacramento',
    'San Benito',
    'San Bernardino',
    'San Diego',
    'San Francisco',
    'San Joaquin',
    'San Luis Obispo',
    'San Mateo',
    'Santa Barbara',
    'Santa Clara',
    'Santa Cruz',
    'Shasta',
    'Sierra',
    'Siskiyou',
    'Solano',
    'Sonoma',
    'Stanislaus',
    'Sutter',
    'Tehama',
    'Trinity',
    'Tulare',
    'Tuolumne',
    'Ventura',
    'Yolo',
    'Yuba',
]
CO_COUNTIES = [
    'Adams',
    'Alamosa',
    'Arapahoe',
    'Archuleta',
    'Baca',
    'Bent',
    'Boulder',
    'Broomfield',
    'Chaffee',
    'Cheyenne',
    'Clear Creek',
    'Conejos',
    'Costilla',
    'Crowley',
    'Custer',
    'Delta',
    'Denver',
    'Dolores',
    'Douglas',
    'Eagle',
    'El Paso',
    'Elbert',
    'Fremont',
    'Garfield',
    'Gilpin',
    'Grand',
    'Gunnison',
    'Hinsdale',
    'Huerfano',
    'Jackson', 
    'Jefferson',
    'Kiowa',
    'Kit Carson',
    'La Plata',
    'Lake',
    'Larimer',
    'Las Animas',
    'Lincoln',
    'Logan',
    'Mesa',
    'Mineral',
    'Moffat',
    'Montezuma',
    'Montrose',
    'Morgan',
    'Otero',
    'Ouray',
    'Park',
    'Phillips',
    'Pitkin',
    'Prowers',
    'Pueblo',
    'Rio Blanco',
    'Rio Grande',
    'Routt',
    'Saguache',
    'San Juan',
    'San Miguel',
    'Sedgwick',
    'Summit',
    'Teller',
    'Washington',
    'Weld',
    'Yuma',
]
FL_COUNTIES = [
    'Alachua',
    'Baker',
    'Bay',
    'Bradford',
    'Brevard',
    'Broward',
    'Calhoun',
    'Charlotte',
    'Citrus',
    'Clay',
    'Collier',
    'Columbia',
    'DeSoto',
    'Dixie',
    'Duval',
    'Escambia',
    'Flagler',
    'Franklin',
    'Gadsden',
    'Gilchrist',
    'Glades',
    'Gulf',
    'Hamilton',
    'Hardee',
    'Hendry',
    'Hernando',
    'Highlands',
    'Hillsborough',
    'Holmes',
    'Indian River',
    'Jackson',
    'Jefferson',
    'Lafayette',
    'Lake',
    'Lee',
    'Leon',
    'Levy',
    'Liberty',
    'Madison',
    'Manatee',
    'Marion',
    'Martin',
    'Miami-Dade',
    'Monroe',
    'Nassau',
    'Okaloosa',
    'Okeechobee',
    'Orange',
    'Osceola',
    'Palm Beach',
    'Pasco',
    'Pinellas',
    'Polk',
    'Putnam',
    'Saint Johns',
    'Saint Lucie',
    'Santa Rosa',
    'Sarasota',
    'Seminole',
    'Sumter',
    'Suwannee',
    'Taylor',
    'Union',
    'Volusia',
    'Wakulla',
    'Walton',
    'Washington',
]
IA_COUNTIES = [
    'Adair',
    'Adams',
    'Allamakee',
    'Appanoose',
    'Audubon',
    'Benton',
    'Black Hawk',
    'Boone',
    'Bremer',
    'Buchanan',
    'Buena Vista',
    'Butler',
    'Calhoun',
    'Carroll',
    'Cass',
    'Cedar',
    'Cerro Gordo',
    'Cherokee',
    'Chickasaw',
    'Clarke',
    'Clay',
    'Clayton',
    'Clinton',
    'Crawford',
    'Dallas',
    'Davis',
    'Decatur',
    'Delaware',
    'Des Moines',
    'Dickinson',
    'Dubuque',
    'Emmet',
    'Fayette',
    'Floyd',
    'Franklin',
    'Fremont',
    'Greene',
    'Grundy',
    'Guthrie',
    'Hamilton',
    'Hancock',
    'Hardin',
    'Harrison',
    'Henry',
    'Howard',
    'Humboldt',
    'Ida',
    'Iowa',
    'Jackson',
    'Jasper',
    'Jefferson',
    'Johnson',
    'Jones',
    'Keokuk',
    'Kossuth',
    'Lee',
    'Linn',
    'Louisa',
    'Lucas',
    'Lyon',
    'Madison',
    'Mahaska',
    'Marion',
    'Marshall',
    'Mills',
    'Mitchell',
    'Monona',
    'Monroe',
    'Montgomery',
    'Muscatine',
    'O\'Brien',
    'Osceola',
    'Page',
    'Palo Alto',
    'Plymouth',
    'Pocahontas',
    'Polk',
    'Pottawattamie',
    'Poweshiek',
    'Ringgold',
    'Sac',
    'Scott',
    'Shelby',
    'Sioux',
    'Story',
    'Tama',
    'Taylor',
    'Union',
    'Van Buren',
    'Wapello',
    'Warren',
    'Washington',
    'Wayne',
    'Webster',
    'Winnebago',
    'Winneshiek',
    'Woodbury',
    'Worth',
    'Wright',
]
IL_COUNTIES = [
    'Adams',
    'Alexander',
    'Bond',
    'Boone',
    'Brown',
    'Bureau',
    'Calhoun',
    'Carroll',
    'Cass',
    'Champaign',
    'Christian',
    'Clark',
    'Clay',
    'Clinton',
    'Coles',
    'Cook',
    'Crawford',
    'Cumberland',
    'DeKalb',
    'DeWitt',
    'Douglas',
    'DuPage',
    'Edgar',
    'Edwards',
    'Effingham',
    'Fayette',
    'Ford',
    'Franklin',
    'Fulton',
    'Gallatin',
    'Greene',
    'Grundy',
    'Hamilton',
    'Hancock',
    'Hardin',
    'Henderson',
    'Henry',
    'Iroquois',
    'Jackson',
    'Jasper',
    'Jefferson',
    'Jersey',
    'Jo Daviess',
    'Johnson',
    'Kane',
    'Kankakee',
    'Kendall',
    'Knox',
    'Lake',
    'LaSalle',
    'Lawrence',
    'Lee',
    'Livingston',
    'Logan',
    'Macon',
    'Macoupin',
    'Madison',
    'Marion',
    'Marshall',
    'Mason',
    'Massac',
    'McDonough',
    'McHenry',
    'McLean',
    'Menard',
    'Mercer',
    'Monroe',
    'Montgomery',
    'Morgan',
    'Moultrie',
    'Ogle',
    'Peoria',
    'Perry',
    'Piatt',
    'Pike',
    'Pope',
    'Pulaski',
    'Putnam',
    'Randolph',
    'Richland',
    'Rock',
    'Saline',
    'Sangamon',
    'Schuyler',
    'Scott',
    'Shelby',
    'Saint Clair',
    'Stark',
    'Stephenson',
    'Tazewell',
    'Union',
    'Vermilion',
    'Wabash',
    'Warren',
    'Washington',
    'Wayne',
    'White',
    'Whiteside',
    'Will',
    'Williamson',
    'Winnebago',
    'Woodford',
]
KS_COUNTIES = [
    'Douglas',
]
MI_COUNTIES = [
    'Leelanau',
    'Marquette',
]
MN_COUNTIES = [
   'Aitkin',
   'Anoka',
   'Becker',
   'Beltrami',
   'Benton',
   'Big Stone',
   'Blue Earth',
   'Brown',
   'Carlton',
   'Carver',
   'Cass',
   'Chippewa',
   'Chisago',
   'Clay',
   'Clearwater',
   'Cook',
   'Cottonwood',
   'Crow Wing',
   'Dakota',
   'Dodge',
   'Douglas',
   'Faribault',
   'Fillmore',
   'Freeborn',
   'Goodhue',
   'Grant',
   'Hennepin',
   'Houston',
   'Hubbard',
   'Isanti',
   'Itasca',
   'Jackson',
   'Kanabec',
   'Kandiyohi',
   'Kittson',
   'Koochiching',
   'Lac qui Parle',
   'Lake of the Woods',
   'Lake',
   'Le Sueur',
   'Lincoln',
   'Lyon',
   'Mahnomen',
   'Marshall',
   'Martin',
   'McLeod',
   'Meeker',
   'Mille Lacs',
   'Morrison',
   'Mower',
   'Murray',
   'Nicollet',
   'Nobles',
   'Norman',
   'Olmsted',
   'Otter Tail',
   'Pennington',
   'Pine',
   'Pipestone',
   'Polk',
   'Pope',
   'Ramsey',
   'Red Lake',
   'Redwood',
   'Renville',
   'Rice',
   'Rock',
   'Roseau',
   'Saint Louis',
   'Scott',
   'Sherburne',
   'Sibley',
   'Stearns',
   'Steele',
   'Stevens',
   'Swift',
   'Todd',
   'Traverse',
   'Wabasha',
   'Wadena',
   'Waseca',
   'Washington',
   'Watonwan',
   'Wilkin',
   'Winona',
   'Wright',
   'Yellow Medicine',
]
ND_COUNTIES = [
    'Adams',
    'Barnes',
    'Benson',
    'Billings',
    'Bottineau',
    'Bowman',
    'Burke',
    'Burleigh',
    'Cass',
    'Cavalier',
    'Dickey',
    'Divide',
    'Dunn',
    'Eddy',
    'Emmons',
    'Foster',
    'Golden Valley',
    'Grand Forks',
    'Grant',
    'Griggs',
    'Hettinger',
    'Kidder',
    'LaMoure',
    'Logan',
    'McHenry',
    'McIntosh',
    'McKenzie',
    'McLean',
    'Mercer',
    'Morton',
    'Mountrail',
    'Nelson',
    'Oliver',
    'Pembina',
    'Pierce',
    'Ramsey',
    'Ransom',
    'Renville',
    'Richland',
    'Rolette',
    'Sargent',
    'Sheridan',
    'Sioux',
    'Slope',
    'Stark',
    'Steele',
    'Stutsman',
    'Towner',
    'Traill',
    'Walsh',
    'Ward',
    'Wells',
    'Williams',
]
NJ_COUNTIES = [
    'Atlantic',
    'Burlington',
    'Camden',
    'Cape May',
    'Cumberland',
    'Essex',
    'Gloucester',
    'Hudson',
    'Hunterdon',
    'Mercer',
    'Middlesex',
    'Morris',
    'Monmouth',
    'Ocean',
    'Passaic',
    'Salem',
    'Somerset',
    'Sussex',
    'Union',
    'Warren',
]
NY_COUNTIES = [
    'Albany',
    'Allegany',
    'Bronx',
    'Broome',
    'Cattaraugus',
    'Cayuga',
    'Chautauqua',
    'Chemung',
    'Chenango',
    'Clinton',
    'Columbia',
    'Cortland',
    'Delaware',
    'Dutchess',
    'Erie',
    'Essex',
    'Franklin',
    'Fulton',
    'Genesee',
    'Greene',
    'Hamilton',
    'Herkimer',
    'Jefferson',
    'Kings',
    'Lewis',
    'Livingston',
    'Madison',
    'Monroe',
    'Montgomery',
    'Nassau',
    'New York',
    'Niagara',
    'Oneida',
    'Onondaga',
    'Ontario',
    'Orange',
    'Orleans',
    'Oswego',
    'Otsego',
    'Putnam',
    'Queens',
    'Rensselaer',
    'Richmond',
    'Rockland',
    'Saint Lawrence',
    'Saratoga',
    'Schenectady',
    'Schoharie',
    'Schuyler',
    'Seneca',
    'Steuben',
    'Suffolk',
    'Sullivan',
    'Tioga',
    'Tompkins',
    'Ulster',
    'Warren',
    'Washington',
    'Wayne',
    'Westchester',
    'Wyoming',
    'Yates',
]
OH_COUNTIES = [
    ('ATH', 'Athens', USCountySquareShield, {'bg': COLOR_GREEN}),
    ('ATH:Trimble', 'Trimble', USCountySquareShield, {'level': 'TOWNSHIP', 'bg': COLOR_GREEN}),
    ('AUG', 'Auglaize', USCountySquareShield, None),
    ('BEL', 'Belmont', USCountySquareShield, {'bg':COLOR_GREEN}),
    ('BEL:Kirkwood', 'Kirkwood', USCountySquareShield, {'level': 'TWP'}),
    ('CAR', 'Carroll', USCountySquareShield, None),
    ('COL', 'Columbiana', USCountyShield, None),
    ('COS', 'Coshocton', USCountySquareShield, None),
    ('COS:Adams', 'Adams', USCountySquareShield, {'bg': COLOR_YELLOW, 'level': 'TWP'}),
    ('COS:Jackson', 'Jackson', USCountySquareShield, {'level': 'TWP.', 'bg': COLOR_GREEN}),
    ('FAI', 'Fairfield', USCountySquareShield, None),
    ('FAI:Violet', 'Violet', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('FUL', 'Fulton', USCountySquareShield, None),
    ('GAL', 'Gallia', USCountySquareShield, None),
    ('GUE', 'Guernsey', USCountySquareShield, {'bg': COLOR_GREEN}),
    ('HAS', 'Harrison', USCountySquareShield, None),
    ('HEN', 'Henry', USCountySquareShield, {'bg': COLOR_GREEN}),
    ('HOC', 'Hocking', USCountySquareShield, None),
    ('HOL', 'Holmes', USCountySquareShield, None),
    ('HOL:Berlin', 'Berlin', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('HOL:Clark', 'Clark', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('HOL:Knox', 'Knox', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('HOL:Paint', 'Paint', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('HOL:Salt_Creek', 'Salt Creek', USCountySquareShield, {'level': 'TWP'}),
    ('JEF', 'Jefferson', USCountyShield, None),
    ('JEF:Springfield', 'Springfield', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('KNO', 'Knox', USCountySquareShield, None),
    ('KNO:Milford', 'Milford', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('LAW', 'Lawrence', USCountySquareShield, None),
    ('LIC', 'Licking', USCountySquareShield, None),
    ('LIC:Jersey', 'Jersey', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('LOG', 'Logan', USCountySquareShield, None),
    ('LOG:Jefferson', 'Jefferson', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('LOG:Lake', 'Lake', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('LOG:Liberty', 'Liberty', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('LOG:Monroe', 'Monroe', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('LOG:Perry', 'Perry', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('MAD', 'Madison', USCountyShield, None),
    ('MAH', 'Mahoning', USCountyShield, None),
    ('MED', 'Medina', USCountySquareShield, {'bg': COLOR_BLUE}),
    ('MED:Harrisville', 'Harrisville', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('MED:Sharon', 'Sharon', USCountySquareShield, {'level': 'TOWNSHIP', 'bg': COLOR_GREEN}),
    ('MED:Wadsworth', 'Wads', USCountySquareShield, {'level': 'TWP'}),
    ('MED:York', 'York', USCountySquareShield, {'level': 'TWP.', 'bg': COLOR_BLUE}),
    ('MIA', 'Miami', USCountySquareShield, None),
    ('MRG', 'Morgan', USCountySquareShield, None),
    ('MRG:York', 'York', USCountySquareShield, {'level': 'TOWNSHIP', 'bg': COLOR_GREEN}),
    ('MRW', 'Morrow', USCountySquareShield, None),
    ('WAS:Bennington', 'Bennington', USCountySquareShield, {'bg': COLOR_GREEN, 'level': 'TOWNSHIP'}),
    ('MRW:Franklin', 'Franklin', USCountySquareShield, {'level': 'TOWNSHIP', 'bg': COLOR_GREEN}),
    ('MRW:Harmony', 'Harmony', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('MRW:South_Bloomfield', 'South Bloomfield', USCountySquareShield, {'level': 'TWP'}),
    ('MRW:Westfield', 'Westfield', USCountySquareShield, {'level': 'TOWNSHIP'}),
    ('NOB', 'Noble', USCountySquareShield, {'bg': COLOR_BLUE}),
    ('OTT', 'Ottawa', USCountyShield, None),
    ('PER', 'Perry', USCountySquareShield, None),
    ('SEN', 'Seneca', USCountyShield, None),
    ('SHE', 'Shelby', USCountySquareShield, None),
    ('STA', 'Stark', USCountyShield, None),
    ('SUM', 'Summit', USCountyShield, None),
    ('TUS', 'Tuscarawas', USCountyShield, None),
    ('UNI', 'Union', USCountySquareShield, None),
    ('VIN', 'Vinton', USCountySquareShield, {'bg': COLOR_YELLOW}),
    ('WAS', 'Washington', USCountySquareShield, {'bg': COLOR_GREEN}),
    ('WAS:Aurelius', 'Aurelius', USCountySquareShield, {'bg': COLOR_GREEN, 'level': 'TOWNSHIP'}),
    ('WAS:Salem', 'Salem', USCountySquareShield, {'level': 'TWP', 'bg': COLOR_GREEN}),
    ('WAY', 'Wayne', USCountySquareShield, None),
    ('WIL', 'Williams', USCountySquareShield, {'bg': COLOR_GREEN}),
    ('WYA', 'Wyandot', USCountySquareShield, None),
]
SD_COUNTIES = [
    'Abbeville',
    'Aiken',
    'Allendale',
    'Anderson',
    'Bamberg',
    'Barnwell',
    'Beaufort',
    'Berkeley',
    'Calhoun',
    'Charleston',
    'Cherokee',
    'Chester',
    'Chesterfield',
    'Clarendon',
    'Colleton',
    'Darlington',
    'Dillon',
    'Dorchester',
    'Edgefield',
    'Fairfield',
    'Florence',
    'Georgetown',
    'Greenville',
    'Greenwood',
    'Hampton',
    'Horry',
    'Jasper',
    'Kershaw',
    'Lancaster',
    'Laurens',
    'Lee',
    'Lexington',
    'Marion',
    'Marlboro',
    'McCormick',
    'Newberry',
    'Oconee',
    'Orangeburg',
    'Pickens',
    'Richland',
    'Saluda',
    'Spartanburg',
    'Sumter',
    'Union',
    'Williamsburg',
    'York',
]
TX_COUNTIES = [
    'Anderson',
    'Andrews',
    'Angelina',
    'Aransas',
    'Archer',
    'Armstrong',
    'Atascosa',
    'Austin',
    'Bailey',
    'Bandera',
    'Bastrop',
    'Baylor',
    'Bee',
    'Bell',
    'Bexar',
    'Blanco',
    'Borden',
    'Bosque',
    'Bowie',
    'Brazoria',
    'Brazos',
    'Brewster',
    'Briscoe',
    'Brooks',
    'Brown',
    'Burleson',
    'Burnet',
    'Caldwell',
    'Calhoun',
    'Callahan',
    'Cameron',
    'Camp',
    'Carson',
    'Cass',
    'Castro',
    'Chambers',
    'Cherokee',
    'Childress',
    'Clay',
    'Cochran',
    'Coke',
    'Coleman',
    'Collin',
    'Collingsworth',
    'Colorado',
    'Comal',
    'Comanche',
    'Concho',
    'Cooke',
    'Coryell',
    'Cottle',
    'Crane',
    'Crockett',
    'Crosby',
    'Culberson',
    'Dallam',
    'Dallas',
    'Dawson',
    'Deaf Smith',
    'Delta',
    'Denton',
    'DeWitt',
    'Dickens',
    'Dimmit',
    'Donley',
    'Duval',
    'Eastland',
    'Ector',
    'Edwards',
    'Ellis',
    'El Paso',
    'Erath',
    'Falls',
    'Fannin',
    'Fayette',
    'Fisher',
    'Floyd',
    'Foard',
    'Fort Bend',
    'Franklin',
    'Freestone',
    'Frio',
    'Gaines',
    'Galveston',
    'Garza',
    'Gillespie',
    'Glasscock',
    'Goliad',
    'Gonzales',
    'Gray',
    'Grayson',
    'Gregg',
    'Grimes',
    'Guadalupe',
    'Hale',
    'Hall',
    'Hamilton',
    'Hansford',
    'Hardeman',
    'Hardin',
    'Harris',
    'Harrison',
    'Hartley',
    'Haskell',
    'Hays',
    'Hemphill',
    'Henderson',
    'Hidalgo',
    'Hill',
    'Hockley',
    'Hood',
    'Hopkins',
    'Houston',
    'Howard',
    'Hudspeth',
    'Hunt',
    'Hutchinson',
    'Irion',
    'Jack',
    'Jackson',
    'Jasper',
    'Jeff Davis',
    'Jefferson',
    'Jim Hogg',
    'Jim Wells',
    'Johnson',
    'Jones',
    'Karnes',
    'Kaufman',
    'Kendall',
    'Kenedy',
    'Kent',
    'Kerr',
    'Kimble',
    'King',
    'Kinney',
    'Kleberg',
    'Knox',
    'Lamar',
    'Lamb',
    'Lampasas',
    'La Salle',
    'Lavaca',
    'Lee',
    'Leon',
    'Liberty',
    'Limestone',
    'Lipscomb',
    'Live Oak',
    'Llano',
    'Loving',
    'Lubbock',
    'Lynn',
    'McCulloch',
    'McLennan',
    'McMullen',
    'Madison',
    'Marion',
    'Martin',
    'Mason',
    'Matagorda',
    'Maverick',
    'Medina',
    'Menard',
    'Midland',
    'Milam',
    'Mills',
    'Mitchell',
    'Montague',
    'Montgomery',
    'Moore',
    'Morris',
    'Motley',
    'Nacogdoches',
    'Navarro',
    'Newton',
    'Nolan',
    'Nueces',
    'Ochiltree',
    'Oldham',
    'Orange',
    'Palo Pinto',
    'Panola',
    'Parker',
    'Parmer',
    'Pecos',
    'Polk',
    'Potter',
    'Presidio',
    'Rains',
    'Randall',
    'Reagan',
    'Real',
    'Red River',
    'Reeves',
    'Refugio',
    'Roberts',
    'Robertson',
    'Rockwall',
    'Runnels',
    'Rusk',
    'Sabine',
    'San Augustine',
    'San Jacinto',
    'San Patricio',
    'San Saba',
    'Schleicher',
    'Scurry',
    'Shackelford',
    'Shelby',
    'Sherman',
    'Smith',
    'Somervell',
    'Starr',
    'Stephens',
    'Sterling',
    'Stonewall',
    'Sutton',
    'Swisher',
    'Tarrant',
    'Taylor',
    'Terrell',
    'Terry',
    'Throckmorton',
    'Titus',
    'Tom Green',
    'Travis',
    'Trinity',
    'Tyler',
    'Upshur',
    'Upton',
    'Uvalde',
    'Val Verde',
    'Van Zandt',
    'Victoria',
    'Walker',
    'Waller',
    'Ward',
    'Washington',
    'Webb',
    'Wharton',
    'Wheeler',
    'Wichita',
    'Wilbarger',
    'Willacy',
    'Williamson',
    'Wilson',
    'Winkler',
    'Wise',
    'Wood',
    'Yoakum',
    'Young',
    'Zapata',
    'Zavala',
]
UT_COUNTIES = [
    'Beaver',
    'Box Elder',
    'Cache',
    'Carbon',
    'Daggett',
    'Davis',
    'Duchesne',
    'Emery',
    'Garfield',
    'Grand',
    'Iron',
    'Juab',
    'Kane',
    'Millard',
    'Morgan',
    'Piute',
    'Rich',
    'Salt Lake',
    'San Juan',
    'Sanpete',
    'Sevier',
    'Summit',
    'Tooele',
    'Uintah',
    'Utah',
    'Wasatch',
    'Washington',
    'Wayne',
    'Weber',
]
WY_COUNTIES = [
    'Albany',
    'Big Horn',
    'Campbell',
    'Carbon',
    'Converse',
    'Crook',
    'Fremont',
    'Goshen',
    'Hot Springs',
    'Johnson',
    'Laramie',
    'Lincoln',
    'Natrona',
    'Niobrara',
    'Park',
    'Platte',
    'Sheridan',
    'Sublette',
    'Sweetwater',
    'Teton',
    'Uinta',
    'Washakie',
    'Weston',
]
