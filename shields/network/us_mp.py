#!/usr/bin/python

# Northern Mariana Islands!
#
# I haven't been able to locate an official government source for route
# marker design in the Northern Mariana Islands.  The stuff below is based
# on photos of signs.

import math
import svgwrite

from ..geometry import *
from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MP'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:MP'] = lambda m,r: USMarianaShield(m, r, style).make_shield()

class USMarianaShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(USMarianaShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE
        
        self.width = 24.0
        self.height = 24.0
        self.corner_radius = 1.5
        if self.guide:
            self.border_width = 0.5
        else:
            self.border_width = 1.0

        self.side_height = 4.5
        self.arc_radius = 6.0

        self.ref_height = 11.0
        self.ref_series = 'B'
        self.ref_y = self.height/2 - self.ref_height/2
        self.ref_x = self.border_width + self.arc_radius + self.border_width
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_gu'

    def calc_arc_params(self):
        # https://math.stackexchange.com/questions/564058
        l = self.height
        h = self.width/2 - self.border_width
        arc_radius = (4 * h**2 + l**2) / (8 * h)

        self.arc = Circle(
            (self.border_width + arc_radius) + self.height/2 * 1j,
            arc_radius)
        
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.make_sign_background())
            border_offset = 0
            outline = self.drawing.path(fill=self.bg)
        else:
            border_offset = self.border_width/2
            outline = self.drawing.path(
                fill=self.bg, stroke=self.fg, stroke_width=self.border_width)

        arc = Circle(
            (self.border_width + self.arc_radius) + (self.border_width + self.side_height) * 1j,
            self.arc_radius + border_offset)
        ul_corner = (self.border_width - border_offset, self.border_width - border_offset)
        ur_corner = (self.width - ul_corner[0], ul_corner[1])
        ll_corner = (ul_corner[0], self.height - ul_corner[1])
        lr_corner = (ur_corner[0], ll_corner[1])
        l_arc_top = (ul_corner[0], ul_corner[1] + self.side_height)
        r_arc_top = (self.width - l_arc_top[0], l_arc_top[1])
        l_arc_bot = (l_arc_top[0] + self.arc_radius + border_offset,
                     l_arc_top[1] + self.arc_radius + border_offset)
        r_arc_bot = (self.width - l_arc_bot[0], l_arc_bot[1])
        
        outline.push('M', ur_corner)
        outline.push('V', r_arc_top[1])
        outline.push_arc(r_arc_bot, 0, arc.radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push('L', lr_corner)
        outline.push('H', ll_corner[0])
        outline.push('L', l_arc_bot)
        outline.push_arc(l_arc_top, 0, arc.radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push('V', ul_corner[1])
        outline.push('Z')
        bg_g.add(outline)

        if self.sign:
            return bg_g
        else:
            return outline
