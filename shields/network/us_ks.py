#!/usr/bin/python

# https://kart.ksdot.org/StandardDrawings/resources/us-pdf/te515.pdf
# https://kart.ksdot.org/StandardDrawings/resources/us-pdf/te557.pdf

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *
from ..geometry import *

def register(network_registry, style):
    network_registry['US:KS'] = lambda m,r: us_ks_shield(m, r, style)

def us_ks_shield(modifier, ref, style):
    if ref == 'KANSAS TURNPIKE':
        return KSTurnpikeShield(modifier, ref, style).make_shield()
    if style == STYLE_GENERIC:
        return USGenericStateShield(modifier, ref).make_shield()
    else:
        return KSStateShield(modifier, ref, style).make_shield()

class KSStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(KSStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE
        
        if len(self.ref) <= 2:
            self.width = 24.0
            self.ref_series = 'D'
            self.petal_height = 1.875
            self.petal_width = 4.0
            self.petal1_arc = 1.25
            self.petal_radius1 = 10.25
            self.petal_radius2 = 10.25
        else:
            self.width = 30.0
            if '1' in self.ref:
                self.ref_series = 'D'
            else:
                self.ref_series = 'C'
            self.petal_height = 2.25
            self.petal_width = 4.5
            self.petal1_arc = 1.5
            self.petal_radius1 = 10.0
            right_petal_offset = 12.75
            self.petal_radius2 = abs(right_petal_offset + self.petal_width/2 * 1j)
        self.height = 24.0
        self.corner_radius = 1.5
        self.petal2_arc = 0.25
        self.border_width = 0.75

        self.ref_x = 6.0
        self.ref_y = 6.0
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 12.0
            
        self.fg = COLOR_BLACK
        self.bg = COLOR_YELLOW

    @property
    def blank_key(self):
        return 'us_ks-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g(transform='translate({} {})'.format(self.width/2, self.height/2))

        if self.sign:
            bg_g.add(self.drawing.rect(
                (-self.width/2, -self.height/2), (self.width, self.height), self.corner_radius,
                fill=self.fg))
        else:
            bg_g.add(self.make_flower_background())
        bg_g.add(self.make_flower())
        
        return bg_g


    def calculate_flower_nodes(self):
        # There are four key nodes per quadrant.
        # The upper-left node is half a petal width to the right of the y axis.
        # The lower-right node is half a petal width above the x axis.
        # The other two are along a line parallel to the line between the
        # upper-left and lower-right nodes, each at a distance of a petal width
        # from those nodes.  The four nodes form a trapezoid, so we can do some
        # quick pythagorean math to find the coordinates of the middle two.
        
        ul_x = self.petal_width/2
        ul_y = math.sqrt(self.petal_radius1**2 - ul_x**2)
        upper_left = ul_x + ul_y * 1j
        lr_y = self.petal_width/2
        lr_x = math.sqrt(self.petal_radius2**2 - lr_y**2)
        lower_right = lr_x + lr_y * 1j

        diagonal_distance = abs(lower_right - upper_left)
        next_point_relative_x = (diagonal_distance - self.petal_width) / 2
        next_point_relative_y = math.sqrt((self.petal_width)**2 - next_point_relative_x**2)
        diagonal_unit_vector = (lower_right - upper_left) / diagonal_distance
        middle_left = upper_left + diagonal_unit_vector * next_point_relative_x + \
                      diagonal_unit_vector * 1j * next_point_relative_y
        middle_right = middle_left + diagonal_unit_vector * self.petal_width

        # The returned sequence starts with a copy of the final node so
        # that rendering can start with the top petal.
        base_sequence = [upper_left, middle_left, middle_right, lower_right]
        return [-base_sequence[0].conjugate()] + base_sequence + \
            [p.conjugate() for p in reversed(base_sequence)] + \
            [-p for p in base_sequence] + \
            [-p.conjugate() for p in reversed(base_sequence)]
    
    def make_flower_background(self):
        return self.drawing.ellipse(
            (0, 0), (self.width/2, self.height/2),
            fill=self.fg)
        
        nodes = [n / abs(n) * (abs(n) + self.border_width) for n in self.calculate_flower_nodes()]

        bg = self.drawing.path(
            fill=self.fg, transform='scale({}, -{})'.format(
                (self.width - self.border_width * 2) / self.width,
                (self.height - self.border_width * 2) / self.height))
        
        bg.push('M', (nodes[0].real, nodes[0].imag))
        for i in range(0, 16, 1):
            bg.push_arc((nodes[i+1].real, nodes[i+1].imag), 0,
                        abs(nodes[i+1] - nodes[i]) * 0.51, large_arc=False,
                        angle_dir='-', absolute=True)
        bg.push('Z')
        
        return bg
        
    def make_flower(self):
        flower = self.drawing.path(
            fill=self.bg, transform='scale({}, -{})'.format(
                (self.width - self.border_width * 2) / self.width,
                (self.height - self.border_width * 2) / self.height))

        nodes = self.calculate_flower_nodes()
        
        flower.push('M', (nodes[0].real, nodes[0].imag))
        for i in range(0, 16, 2):
            self.add_petals(flower, nodes[i+0], nodes[i+1], nodes[i+2])
        flower.push('Z')
        
        return flower

    def add_petals(self, flower, start, middle, end):
        self.add_petal1(flower, start, middle)
        self.add_petal2(flower, middle, end)
        
    def add_petal1(self, flower, start, end):
        start_to_end_unit = (end - start) / abs(end - start)
        tip = start + start_to_end_unit * self.petal_width/2 + start_to_end_unit * 1j * self.petal_height

        left_midpoint = (tip - start) / 2
        flower.push_arc(
            (left_midpoint.real, left_midpoint.imag), 0, self.petal1_arc,
            large_arc=False, angle_dir='-', absolute=False)
        flower.push_arc(
            (tip.real, tip.imag), 0, self.petal1_arc,
            large_arc=False, angle_dir='+', absolute=True)

        right_midpoint = (end - tip) / 2
        flower.push_arc(
            (right_midpoint.real, right_midpoint.imag), 0, self.petal1_arc,
            large_arc=False, angle_dir='+', absolute=False)
        flower.push_arc(
            (end.real, end.imag), 0, self.petal1_arc,
            large_arc=False, angle_dir='-', absolute=True)
        
    def add_petal2(self, flower, start, end):
        start_to_end_unit = (end - start) / abs(end - start)
        arc1_center = (end - start) / 2
        arc3_center = end - start_to_end_unit * self.petal1_arc
        arc2_center = arc3_center + start_to_end_unit * 1j * (self.petal1_arc + self.petal2_arc)

        transition_1to2 = arc2_center + unit_vector(arc1_center, arc2_center) * self.petal2_arc
        transition_2to3 = arc3_center + start_to_end_unit * 1j * self.petal1_arc

        flower.push_arc(
            (transition_1to2.real, transition_1to2.imag), 0, self.petal_height,
            large_arc=False, angle_dir='-', absolute=True)
        flower.push_arc(
            (transition_2to3.real, transition_2to3.imag), 0, self.petal2_arc,
            large_arc=True, angle_dir='+', absolute=True)
        flower.push_arc(
            (end.real, end.imag), 0, self.petal1_arc, large_arc=False,
            angle_dir = '-', absolute=True)
        
class KSTurnpikeShield(KSStateShield):
    def __init__(self, modifier, ref, style):
        super(KSTurnpikeShield, self).__init__('', '', style)

        self.center_radius = 5.0
        self.ks_radius = 5.75
        self.ks_height = 2.5
        self.ks_series = 'D'
        self.ks_spacing = 0.7

        # Note: All x and y coordinates are from the *center* of the shield.
        self.ka_height = 4.0
        self.ka_y = -self.ka_height/2
        self.ka_series = 'D'
        self.ka_spacing = 5.0
        self.ka_width = text_length('KA', self.ka_height, self.ka_series, spacing=self.ka_spacing)
        self.ka_x = -self.ka_width/2 + 0.25
        
        self.t_height = 7.0
        self.t_y = -self.t_height/2
        self.t_series = 'D'
        self.t_width = text_length('T', self.t_height, self.t_series)
        self.t_x = -self.t_width/2
        
        self.fg = COLOR_BLUE
        self.kta_color = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ks-turnpike'
    
    def make_blank_shield(self):
        bg_g = super(KSTurnpikeShield, self).make_blank_shield()

        bg_g.add(self.drawing.circle((0, 0), self.center_radius, fill=self.fg))
        bg_g.add(render_text_along_arc(
            self.drawing, 'KANSAS', 0j, self.ks_radius, 0, self.ks_height,
            self.ks_series, self.fg, spacing=self.ks_spacing))
        bg_g.add(render_text_along_arc(
            self.drawing, 'TURNPIKE', 0j, self.ks_radius, 180, self.ks_height,
            self.ks_series, self.fg, spacing=self.ks_spacing, invert=True))

        bg_g.add(render_text(
            self.drawing, 'KA', self.ka_x, self.ka_y, self.ka_width, self.ka_height,
            self.ka_series, color=self.kta_color, spacing=self.ka_spacing))
        bg_g.add(render_text(
            self.drawing, 'T', self.t_x, self.t_y, self.t_width, self.t_height,
            self.t_series, color=self.kta_color))
        
        return bg_g
        
    def make_ref(self):
        return None
    
