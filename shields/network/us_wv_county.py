#!/usr/bin/python
"""West Virginia's unique "fraction" style county routes."""

import math
import svgwrite

from ..banner import banner
from ..utils import *

COUNTIES = [
    'Barbour',
    'Berkeley',
    'Boone',
    'Braxton',
    'Brooke',
    'Cabell',
    'Calhoun',
    'Clay',
    'Doddridge',
    'Fayette',
    'Gilmer',
    'Grant',
    'Greenbrier',
    'Hampshire',
    'Hancock',
    'Hardy',
    'Harrison',
    'Jackson',
    'Jefferson',
    'Kanawha',
    'Lewis',
    'Lincoln',
    'Logan',
    'Marion',
    'Marshall',
    'Mason',
    'McDowell',
    'Mercer',
    'Mineral',
    'Mingo',
    'Monongalia',
    'Monroe',
    'Morgan',
    'Nicholas',
    'Ohio',
    'Pendleton',
    'Pleasants',
    'Pocahontas',
    'Preston',
    'Putnam',
    'Raleigh',
    'Randolph',
    'Ritchie',
    'Roane',
    'Summers',
    'Taylor',
    'Tucker',
    'Tyler',
    'Upshur',
    'Wayne',
    'Webster',
    'Wetzel',
    'Wirt',
    'Wood',
    'Wyoming',
]
def register(network_registry, style):
    for county in COUNTIES:
        network_registry['US:WV:' + county.upper()] = make_shield

class WVCountyRouteParams:
    def __init__(self, A, B, C, D, E, F, G):
        self.width = float(A)
        self.height = 24.0
        self.lone_ref_height = float(B)
        self.frac_ref_height = float(C)
        self.frac_space_around_ref = float(E)
        self.fraction_width = float(E)
        self.fraction_length = float(F)
        
        self.stroke_width = 0.5

        self.ref_y = self.height/2 - self.lone_ref_height/2
        self.numerator_y = self.height/2 - self.fraction_width/2 - \
                           self.frac_space_around_ref - \
                           self.frac_ref_height
        self.denominator_y = self.height/2 + self.fraction_width/2 + \
                             self.frac_space_around_ref
        self.fraction_x = self.height/2 - self.fraction_length/2
        self.fraction_y = self.height/2

        
PARAMS = {
    24: WVCountyRouteParams(24, 12, 8, 12, 1, 20, 2),
    30: WVCountyRouteParams(30, 12, 6, 12, 1, 20, 2),
}

def make_shield(modifier, ref):
    if modifier == '':
        return make_base_shield(ref)
    else:
        shield_svg = make_base_shield(ref)
        banner_svgs = [banner(mod, int(shield_svg['width'])) for mod in modifier.split(';')]
        return compose_shields(banner_svgs + [shield_svg])

def make_base_shield(ref):
    refs = ref.split(':')
    assert len(refs) <= 2
    max_ref_len = max([len(r) for r in refs])
    if max_ref_len <= 2:
        params = PARAMS[24]
        ref_series = 'C'
    else:
        params = PARAMS[30]
        if max_ref_len == 3:
            ref_series = 'C'
        else:
            ref_series = 'B'

    if len(refs) > 1:
        drawing = svgwrite.Drawing(size=(params.height, params.height))
    else:
        drawing = svgwrite.Drawing(size=(params.width, params.height))
    
    shield_bg = make_blank_shield(drawing, params, refs)
    drawing.add(shield_bg)

    ref_g = make_ref(drawing, refs, params, ref_series)
    drawing.add(ref_g)
    
    return drawing

def make_blank_shield(drawing, params, refs):
    background_g = drawing.g()

    if len(refs) > 1:
        outline = drawing.circle(
            (params.height/2, params.height/2),
            params.height/2 - params.stroke_width/2,
            stroke=COLOR_BLACK, fill=COLOR_WHITE,
            stroke_width=params.stroke_width)
    else:
        outline = drawing.ellipse(
            (params.width/2, params.height/2),
            (params.width/2 - params.stroke_width/2, params.height/2 - params.stroke_width/2),
            stroke=COLOR_BLACK, fill=COLOR_WHITE,
            stroke_width=params.stroke_width)
    background_g.add(outline)

    if len(refs) > 1:
        line = drawing.line(
            (params.fraction_x, params.fraction_y),
            (params.fraction_x + params.fraction_length, params.fraction_y),
            stroke=COLOR_BLACK, stroke_width=params.fraction_width)
        background_g.add(line)
    
    return background_g

def make_ref(drawing, refs, params, ref_series):
    if len(refs) == 1:
        ref_g = render_text(drawing, refs[0], 0, params.ref_y, params.width, params.lone_ref_height, ref_series, color=COLOR_BLACK)
    else:
        ref_g = drawing.g()
        ref_g.add(render_text(drawing, refs[0], 0, params.numerator_y, params.height, params.frac_ref_height, ref_series, color=COLOR_BLACK))
        ref_g.add(render_text(drawing, refs[1], 0, params.denominator_y, params.height, params.frac_ref_height, ref_series, color=COLOR_BLACK))

    return ref_g

