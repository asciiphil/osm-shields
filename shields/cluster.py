#!/usr/bin/python

import math

import svgwrite

def cluster_shields(shields, orientation):
    if len(shields) <= 3:
        return make_small_cluster(shields, orientation)
    else:
        return make_large_cluster(shields, orientation)

def make_large_cluster(shields, orientation):
    source_images = group_sources(shields, orientation)
    dst_width, dst_height = get_image_dims(source_images)
    dst = svgwrite.Drawing(size=(dst_width, dst_height))
    row_top = 0
    for row in source_images:
        row_width, row_height = get_row_dims(row)
        copy_row(dst, row, row_top, (dst_width - row_width) / 2)
        row_top += row_height
    return dst

def group_sources(sources, orientation):
    n = len(sources)
    cols = int(math.ceil(math.sqrt(n)))
    if n == 3:
        cols = 3
    if orientation == 'N':
        cols = int(math.ceil(n / float(cols)))
    return [ sources[i:i+cols] for i in range(0, n, cols) ]

def get_image_dims(rows):
    width = 0
    height = 0
    for row in rows:
        (rw, rh) = get_row_dims(row)
        width = max(width, rw)
        height += rh
    return (width, height)

def get_row_dims(row):
    width = sum([float(img['width']) for img in row])
    height = max([float(img['height']) for img in row])
    return (width, height)

def copy_row(dst, row, row_top, row_left):
    img_left = row_left
    row_width, row_height = get_row_dims(row)
    for img in row:
        copy_image_to(dst, img, img_left, row_top + (row_height - float(img['height']))/2)
        img_left += float(img['width'])

def copy_image_to(dest, source, x, y):
    defs = set([e['id'] for e in dest.defs.elements])
    for e in source.defs.elements:
        if e['id'] not in defs:
            dest.defs.add(e)
            defs.add(e['id'])
    g = dest.g(transform='translate({},{})'.format(x, y))
    dest.add(g)
    for e in source.elements[1:]:
        g.add(e)


def make_small_cluster(shields, orientation):
    source_images = shields
    dst_width, dst_height = get_simple_dims(source_images)
    dst = svgwrite.Drawing(size=(dst_width, dst_height))
    x, y = first_pos(source_images[0], dst, orientation)
    copy_image_to(dst, source_images[0], x, y)
    envelope = (x, y, x+float(source_images[0]['width']), y+float(source_images[0]['height']))
    for last, this in zip(source_images, source_images[1:]):
        x, y = next_pos(x, y, last, this, orientation)
        copy_image_to(dst, this, x, y)
        envelope = (min(envelope[0], x), min(envelope[1], y),
                    max(envelope[2], x+float(this['width'])), max(envelope[3], y+float(this['height'])))
    final = svgwrite.Drawing(size=(envelope[2] - envelope[0], envelope[3] - envelope[1]))
    copy_image_to(final, dst, -envelope[0], -envelope[1])
    return final

def get_simple_dims(source_images):
    return (sum([float(i['width']) for i in source_images]),
            sum([float(i['height']) for i in source_images]))

def first_pos(img, dst, orientation):
    if orientation == 'N':
        return (float(dst['width'])/2 - float(img['width'])/2, 0)
    elif orientation == 'NNE':
        return (float(dst['width']) - float(img['width']), 0)
    elif orientation == 'NE' or orientation == 'ENE':
        return (0, float(dst['height']) - float(img['height']))
    elif orientation == 'E':
        return (0, float(dst['height'])/2 - float(img['height'])/2)
    elif orientation == 'ESE' or orientation == 'SE' or orientation == 'SSE':
        return (0, 0)
        
def next_pos(x, y, last_img, next_img, orientation):
    if orientation == 'N':
        return (x + float(last_img['width'])/2 - float(next_img['width'])/2, y + float(last_img['height']))
    elif orientation == 'NNE':
        return (x + (float(last_img['width'])-3*float(next_img['width']))/4, y + float(last_img['height']))
    elif orientation == 'NE':
        return (x + float(last_img['width']), y - float(last_img['height']))
    elif orientation == 'ENE':
        return (x + float(last_img['width']), y + (float(last_img['height'])-3*float(next_img['height']))/4)
    elif orientation == 'E':
        return (x + float(last_img['width']), y + float(last_img['height'])/2 - float(next_img['height'])/2)
    elif orientation == 'ESE':
        return (x + float(last_img['width']), y - (float(last_img['height'])-3*float(next_img['height']))/4)
    elif orientation == 'SE':
        return (x + float(last_img['width']), y + float(last_img['height']))
    elif orientation == 'SSE':
        return (x + (3*float(last_img['width'])-float(next_img['width']))/4, y + float(last_img['height']))
