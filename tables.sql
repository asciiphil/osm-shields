-- Next, the replacement tables.  If two or more concurrent routes should
-- represented differently than just having their shields side by side,
-- these tables let you specify the replacement.  The replace_refs()
-- function does the work.  If all refs with the same id in
-- ref_replace_src are present in the supplied array, they're removed and
-- replaced by the ref with that id in ref_replace_dst.  Use the
-- add_replacement() function to add new replacements.
--
-- Note: replacements can overlap.  If you have mappings (B,C) -> X and
-- (B,D,E) -> Y, then the set (A,B,C,D,E,F) will be turned into (A,F,X,Y).

DROP INDEX IF EXISTS shields_replacement_src_ref;
DROP TABLE IF EXISTS shields_replacement_src;
DROP TABLE IF EXISTS shields_replacement_dst;
CREATE TABLE shields_replacement_dst (
  network TEXT NOT NULL,
  ref TEXT NOT NULL,
  id SERIAL,
  count INTEGER NOT NULL,
  PRIMARY KEY(id));
CREATE TABLE shields_replacement_src (
  network TEXT NOT NULL,
  ref TEXT NOT NULL,
  id INTEGER NOT NULL REFERENCES shields_replacement_dst);
CREATE INDEX shields_replacement_src_ref ON shields_replacement_src (network, ref);
GRANT SELECT ON shields_replacement_src, shields_replacement_dst TO PUBLIC;

CREATE OR REPLACE FUNCTION shields_add_replacement(srcs fullref[], dst_network TEXT, dst_ref TEXT)
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  replacement_idx INTEGER;
BEGIN
  INSERT INTO shields_replacement_dst (network, ref, count)
    VALUES (dst_network, dst_ref, array_upper(srcs, 1))
    RETURNING id INTO replacement_idx;
  INSERT INTO shields_replacement_src SELECT (UNNEST(srcs)).network, (UNNEST(srcs)).ref, replacement_idx;
END;
$$;
