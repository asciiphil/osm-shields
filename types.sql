CREATE TYPE fullref AS (
    network TEXT,
    modifier TEXT,
    ref TEXT
);
