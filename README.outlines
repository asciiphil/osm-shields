The SVG templates here are all designed around a standard US shield height
of 24 pixels.  This makes a lot of things easy because most official
definitions give dimensions in inches for a 24-inch-tall sign, so we can
just set one pixel equal to one inch.  In many cases, our templates have a
half-pixel border around the shield to account for slight overruns in path
rendering.

When dimensions are given by state DOT or other relevant authorities, they
define the height of the route numbers.  Otherwise, the ideal height is 12
pixels, with a preferred minimum of 10 pixels.  (Most states use 12;
Interstate highways use 10.)

The standard FHWA series for route numbers are as follows:
 * 1-2 characters: series D (on a 24 pixel wide shield)
 * 3 characters with at least one "1": series D (on a 30 pixel wide shield)
 * 3 characters with no "1"s: series C (on a 30 pixel wide shield)
 * 4 characters: series B (on a 30 pixel wide shield)
