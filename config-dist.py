import os

# Database configuration.  Use None if you don't need a parameter.
DB_HOST='localhost'
DB_PORT=None
DB_NAME='osm'
DB_USER=os.environ['USER']
DB_PASS=None
DB_PREFIX='planet_osm'

### Mapnik config
# Location of world shapefiles directory.
WORLD_BOUNDARIES="world_boundaries/"
# Database projection
EPSG=900913
# Pull data extent from the database (more flexible but can be slow).
ESTIMATE_EXTENT=False
# The extent if ESTIMATE_EXTENT, above, is False.  Must be in the database's
# coordinate system.
EXTENT='-20037508,-19929239,20037508,19929239'
