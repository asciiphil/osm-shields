#!/usr/bin/env python3

import mapnik
import argparse
import cairo

from coords import *

parser = argparse.ArgumentParser(description='Render an image with mapnik.', add_help=False)
parser.add_argument('--center', '-c', action='store', required=True,
                    help='Coordinates at which to center the image in lon,lat.')
parser.add_argument('--zoom', '-z', action='store', type=int, default=14,
                    help='The zoom level at which to render the image.')
parser.add_argument('--width', '-w', action='store', type=int, default=512,
                    help='The width of the rendered image.')
parser.add_argument('--height', '-h', action='store', type=int, default=512,
                    help='The height of the rendered image.')
parser.add_argument('--stylesheet', '-s', action='store', default='rendering/osm.xml',
                    help='The Mapnik stylesheet to use.')
parser.add_argument('--output', '-o', action='store', default='image.png',
                    help='The filename with which to save the rendered image.  Image is stored as PNG regardless of the extension.')
parser.add_argument('--help', action='help')
args = parser.parse_args()

(lon, lat) = [ float(l) for l in args.center.split(',') ]

center_ll = mapnik.Coord(lon, lat)
center_pixel = LLToPixel(center_ll, args.zoom)
ul_merc = pixelToMerc(mapnik.Coord(center_pixel.x - args.width/2, center_pixel.y - args.height/2), args.zoom)
lr_merc = pixelToMerc(mapnik.Coord(center_pixel.x + args.width/2 - 1, center_pixel.y + args.height/2 - 1), args.zoom)
print('bbox: {0} {1} {2} {3}'.format(ul_merc.x, lr_merc.y, lr_merc.x, ul_merc.y))

m = mapnik.Map(args.width, args.height)
mapnik.load_map(m, args.stylesheet)
m.zoom_to_box(mapnik.Envelope(ul_merc.x, lr_merc.y, lr_merc.x, ul_merc.y))

# Cairo
s = cairo.ImageSurface(cairo.FORMAT_ARGB32, args.width, args.height)
mapnik.render(m, s)
im = mapnik.Image.from_cairo(s)

# Non-Cairo
#im = mapnik.Image(args.width, args.height)
#mapnik.render(m, im)

im.save(args.output, 'png')
