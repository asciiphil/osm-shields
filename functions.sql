CREATE EXTENSION plpython3u;


------
-- Saving single shields
--

CREATE OR REPLACE FUNCTION shields_get_single_filename(anetwork TEXT, amodifier TEXT, aref TEXT)
RETURNS TEXT
LANGUAGE plpython3u
AS $$
  import os
  import shields

  if not 'image_dir' in GD:
    plpy.execute('SELECT shields_set_python_dirs()')
  base_dir = GD['image_dir']

  os.umask(0o022)
  return shields.render_and_save(anetwork, amodifier, aref, base_dir)
$$;


------
-- Saving shield clusters.
--

CREATE OR REPLACE FUNCTION shields_get_cluster_filename(fullrefs fullref[], aorientation TEXT)
RETURNS TEXT
LANGUAGE plpython3u
AS $$
  import os
  import shields

  if not 'image_dir' in GD:
    plpy.execute('SELECT shields_set_python_dirs()')
  base_dir = GD['image_dir']

  os.umask(0o022)
  return shields.cluster_and_save(fullrefs, aorientation, base_dir)
$$;


------
-- Get the right filename for a route's references.
--

CREATE OR REPLACE FUNCTION shields_get_filename(fullrefs fullref[], aorientation TEXT)
RETURNS TEXT
RETURNS NULL ON NULL INPUT
LANGUAGE plpgsql
AS $$
DECLARE
  ref_count INTEGER;
  filename TEXT;
  simple_orientation TEXT;
BEGIN
  SELECT array_upper(fullrefs, 1) INTO ref_count;
  IF ref_count = 0 THEN
    RETURN NULL;
  ELSIF ref_count = 1 THEN
    SELECT shields_get_single_filename(fullrefs[1].network, fullrefs[1].modifier, fullrefs[1].ref) INTO filename;
    RETURN filename;
  ELSIF ref_count <= 3 THEN
    SELECT shields_get_cluster_filename(fullrefs, aorientation) INTO filename;
    RETURN filename;
  ELSIF ref_count = 4 OR ref_count IN (7, 8, 9) THEN
    RETURN shields_get_cluster_filename(fullrefs, 'N');
  ELSE
    IF aorientation IN ('N', 'NNE', 'NE', 'SE', 'SSE') THEN
      simple_orientation = 'N';
    ELSE
      simple_orientation = 'E';
    END IF;
    SELECT shields_get_cluster_filename(fullrefs, simple_orientation) INTO filename;
    RETURN filename;
  END IF;
END;
$$;


------
-- Effect the shield replacements encoded in the shields_replacement_* tables.
--

CREATE OR REPLACE FUNCTION shields_replace_groups(fullref[])
RETURNS fullref[]
STABLE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL
AS $$
  SELECT ARRAY(
    WITH replacements AS (
        SELECT d.id
          FROM (SELECT DISTINCT * FROM unnest($1)) AS f
            JOIN shields_replacement_src s ON f.network = s.network AND f.ref = s.ref
            JOIN shields_replacement_dst d ON s.id = d.id
          GROUP BY d.id, d.count
          HAVING count(s.ref) = d.count
      )
    SELECT unnest($1)
    UNION
    SELECT ROW(d.network, NULL, d.ref)::fullref
      FROM shields_replacement_dst d
        JOIN replacements r ON d.id = r.id
    EXCEPT
    SELECT ROW(s.network, NULL, s.ref)::fullref
      FROM shields_replacement_src s
        JOIN replacements r ON s.id = r.id
  );
$$;


------
-- Get the route memberships for a way.
--

CREATE OR REPLACE FUNCTION osm_get_tag_value(tags TEXT[], tag TEXT)
RETURNS TEXT
IMMUTABLE
RETURNS NULL ON NULL INPUT
LANGUAGE plpgsql
AS $$
DECLARE
  tag_index INTEGER;
BEGIN
  SELECT i INTO tag_index
    FROM (SELECT generate_subscripts(tags, 1) AS i) AS indices
    WHERE i % 2 = 1 AND tags[i] = tag;
  RETURN tags[tag_index + 1];
END;
$$;

CREATE OR REPLACE FUNCTION shields_sanitize_text(TEXT)
RETURNS TEXT
IMMUTABLE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL
AS $$
  SELECT replace($1, '/', ':');
$$;

CREATE OR REPLACE FUNCTION shields_get_route_memberships(planet_osm_rels.id%TYPE)
RETURNS fullref[]
STABLE
RETURNS NULL ON NULL INPUT
COST 5000
LANGUAGE SQL
AS $$
  SELECT shields_replace_groups(array_agg(ROW(shields_sanitize_text(network), shields_sanitize_text(modifier), shields_sanitize_text(ref))::fullref))
    FROM (SELECT osm_get_tag_value(tags, 'route') route,
                 UPPER(osm_get_tag_value(tags, 'modifier')) modifier,
                 UPPER(osm_get_tag_value(tags, 'network')) network,
                 UPPER(COALESCE(NULLIF(osm_get_tag_value(tags, 'ref'), ''), osm_get_tag_value(tags, 'name'))) AS ref
            FROM planet_osm_rels
            WHERE parts && ARRAY[$1]
              AND parts[way_off:array_upper(parts, 1)] && ARRAY[$1]) AS ref_inner
    WHERE route IN ('road', 'bicycle', 'hiking') AND network IS NOT NULL AND ref IS NOT NULL;
$$;


------
-- Way orientation
--

CREATE OR REPLACE FUNCTION shields_get_orientation(way geometry)
RETURNS TEXT
IMMUTABLE
RETURNS NULL ON NULL INPUT
LANGUAGE plpgsql
AS $$
DECLARE
  azimuth FLOAT;
BEGIN
  azimuth := ST_Azimuth(ST_StartPoint(way), ST_EndPoint(way));
  IF azimuth >= pi() THEN
    azimuth := azimuth - pi();
  END IF;
  CASE
    WHEN pi()*1/16 < azimuth AND azimuth <= pi()*3/16 THEN
      RETURN 'NNE';
    WHEN pi()*3/16 < azimuth AND azimuth <= pi()*5/16 THEN
      RETURN 'NE';
    WHEN pi()*5/16 < azimuth AND azimuth <= pi()*7/16 THEN
      RETURN 'ENE';
    WHEN pi()*7/16 < azimuth AND azimuth <= pi()*9/16 THEN
      RETURN 'E';
    WHEN pi()*9/16 < azimuth AND azimuth <= pi()*11/16 THEN
      RETURN 'ESE';
    WHEN pi()*11/16 < azimuth AND azimuth <= pi()*13/16 THEN
      RETURN 'SE';
    WHEN pi()*13/16 < azimuth AND azimuth <= pi()*15/16 THEN
      RETURN 'SSE';
    ELSE
      RETURN 'N';
  END CASE;
END;
$$;


------
-- The finished product: go from a record in planet_osm_line to a filename.
--

CREATE OR REPLACE FUNCTION shields_get_filename(osm_id planet_osm_rels.id%TYPE, way geometry)
RETURNS TEXT
RETURNS NULL ON NULL INPUT
LANGUAGE SQL
AS $$
  SELECT shields_get_filename(shields_get_route_memberships($1), shields_get_orientation($2));
$$;

-- And a convenience function that only needs an osm_id.  The version with the
-- geometry parameter is preferred, because this one incurs an additional table
-- lookup.
CREATE OR REPLACE FUNCTION shields_get_filename(aosm_id planet_osm_rels.id%TYPE)
RETURNS TEXT
RETURNS NULL ON NULL INPUT
LANGUAGE SQL
AS $$
  SELECT shields_get_filename(osm_id, way) FROM planet_osm_line WHERE osm_id = $1;
$$;
