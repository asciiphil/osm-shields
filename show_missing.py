#!/usr/bin/env python3

import psycopg2, psycopg2.extras

from config import *

db = psycopg2.connect(database=DB_NAME, host=DB_HOST, user=DB_USER, password=DB_PASS)
cur = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
cur.execute("WITH routes AS (" +
            "  SELECT id, osm_get_tag_value(tags, 'network') AS network," +
            "         shields_sanitize_text(COALESCE(osm_get_tag_value(tags, 'ref'), '')) AS ref," +
            "         COALESCE(osm_get_tag_value(tags, 'name'), '') AS name" +
            "    FROM planet_osm_rels" +
            "    WHERE osm_get_tag_value(tags, 'route') = 'road'" +
            "      AND (osm_get_tag_value(tags, 'ref') IS NOT NULL OR osm_get_tag_value(tags, 'name') IS NOT NULL)" +
            ")" +
            "SELECT r.id, r.network, r.ref, r.name" +
            "  FROM routes r" +
            "    LEFT JOIN shields_images i" +
            "           ON UPPER(r.network) = i.network" +
            "          AND (UPPER(r.ref) = i.ref OR UPPER(r.name) = i.ref)" +
            "  WHERE r.network IS NOT NULL AND i.network IS NULL" +
            "  ORDER BY r.network, shields_ref_sort_key(r.ref), r.ref")

print("<html>")
print("  <head>")
print('    <meta http-equiv="content-type" content="text/html; charset=UTF-8">')
print("    <title>Unmatched Route References</title>")
print("    <style>")
print("      table {")
print("        border-collapse: collapse;")
print("        text-align: left;")
print("      }")
print("      table th {")
print("        font-size: large;")
print("        font-weight: normal;")
print("        padding: 10px 8px;")
print("        border-bottom: 2px solid #666666;")
print("        color: #333;")
print("      }")
print("      table td {")
print("        padding: 9px 8px 0px 8px;")
print("        color: #333;")
print("      }")
print("      table tbody tr:hover td {")
print("        color: #000;")
print("      }")
print("      a {text-decoration: none}")
print("      a:hover {text-decoration: underline}")
print("    </style>")
print("  </head>")
print("  <body>")
print("    <h1>Unmatched Route References</h1>")
print("    <table>")
print("      <thead>")
print("        <tr>")
print("          <th>ID</th>")
print('          <th style="text-align: right">Network</th>')
print("          <th>Ref</th>")
print("          <th>Name</th>")
print("        </tr>")
print("      </thead>")
print("      <tbody>")
for row in cur:
    print("        <tr>")
    print('          <td><a href="http://www.openstreetmap.org/browse/relation/{0}">{0}</a><br><small><tt>(<span title="XML"><a href="http://api.openstreetmap.org/api/0.6/relation/{0}">x</a></span>&#160;<span title="analyze"><a href="http://ra.osmsurround.org/analyze.jsp?relationId={0}">a</a></span>&#160;<a href="http://osmrm.openstreetmap.de/relation.jsp?id={0}">r</a>&#160;<span title="JOSM"><a href="http://localhost:8111/import?url=http://api.openstreetmap.org/api/0.6/relation/{0}/full">j</a></span>&#160;<span title="history"><a href="http://osm.virtuelle-loipe.de/history/?type=relation&amp;ref={0}">h</a></span>&#160;<a href="http://openstreetmap.org?relation={0}">v</a>)</tt></small></td>'.format(row.id))
    print('          <td style="text-align: right"><tt>{0}</tt></td>'.format(row.network))
    print("          <td><tt>{0}</tt></td>".format(row.ref))
    print("          <td>{0}</td>".format(row.name))
print("      </tbody>")
print("    </table>")
print("  </body>")
print("</html>")
