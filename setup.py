#!/usr/bin/env python3

from setuptools import setup, find_packages

exec(open('shields/version.py').read())

setup(
    name='shields',
    version=__version__,
    packages=find_packages(),
    package_data={
        '': ['roadgeek/*.svg', 'roadgeek/*.dat', 'template/*.svg'],
    },
)
