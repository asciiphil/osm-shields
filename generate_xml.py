#!/usr/bin/env python3

import os
import sys
import glob

from config import *

def color_text(color, text):
    if os.name == 'nt':
        return text
    return "\033[9%sm%s\033[0m" % (color,text)

def serialize(xml,output,from_string):
    try:
        import mapnik
    except:
        sys.exit(color_text(1,'Error: saving xml requires Mapnik python bindings to be installed'))
    m = mapnik.Map(1,1)
    if from_string:
        mapnik.load_map_from_string(m,xml,True)
    else:
        mapnik.load_map(m,xml,True)
    if output:
        mapnik.save_map(m,output)
    else:
        if hasattr(mapnik,'mapnik_version') and mapnik.mapnik_version() >= 700:
            print(mapnik.save_map_to_string(m))
        else:
            sys.exit(color_text(1,'Minor error: printing XML to stdout requires Mapnik >=0.7.0, please provide a second argument to save the output to a file'))

if __name__ == '__main__':
    search_path = 'rendering/inc/*.template'
    inc_dir = os.path.dirname(search_path)
    # get all the includes
    includes = glob.glob(search_path)

    replacements = {
        'dbname':DB_NAME,
        'epsg':EPSG,
        'estimate_extent':'true' if ESTIMATE_EXTENT else 'false',
        'extent':EXTENT,
        'host':DB_HOST or '',
        'password':DB_PASS or '',
        'port':DB_PORT or '',
        'prefix':DB_PREFIX,
        'shields':SHIELDS,
        'style':STYLE,
        'symbols':'symbols',
        'user':DB_USER or '',
        'world_boundaries':WORLD_BOUNDARIES
        }
    for xml in includes:
        template = file(xml,'rb')
        new_name = xml.replace('.template','')
        new_file = file(new_name,'wb')
        try:
            new_file.write(template.read() % replacements)
        except ValueError, e:
            sys.exit(color_text(1,"\n%s (found in %s)" % (e,xml)))
        template.close()
        new_file.close()
        
    output = None
    from_string = False
    template_xml = None
    # accepting XML as stream...
    if not sys.stdin.isatty():
        template_xml = sys.stdin.read()
        from_string = True
        if len(sys.argv) > 1:
            output = sys.argv[1]
    elif len(sys.argv) == 1:
        template_xml = None
        output = None
    else:
        template_xml = sys.argv[1]
        if len(sys.argv) > 2:
            output = sys.argv[2]

    if template_xml:
        serialize(template_xml,output,from_string)
    else:
        print('Include files written successfully! Pass the osm.xml file as an argument if you want to serialize a new version or test reading the XML')
