VERSION := $(shell shields/version.py)
FHWA_SERIES = B C D E EM F
FONTS = $(FHWA_SERIES:%=shields/roadgeek/font-%.svg)
TEMPLATES = shields/template/*.svg
PYTHON_MOD_SOURCES = shields/*.py shields/network/*.py
EGG = dist/shields-${VERSION}-py2.7.egg

dist/shields-${VERSION}-py3-none-any.whl: ${PYTHON_MOD_SOURCES} ${FONTS} ${TEMPLATES}
	python3 -m build

shields/roadgeek/font-%.svg:
	fontforge -lang ff -c 'Open($$1); Generate($$2)' roadgeek-fonts/$(*F).sfdir $@

avatar.svg: ${PYTHON_MOD_SOURCES} ${FONTS} ${TEMPLATES}
	python3 -c "import shields; shields.render('US:US', '', 'OSM', 'guide').saveas('$@')"

avatar.png: avatar.svg
	cairosvg --output-width 128 --output-height 128 -f png -o $@ $<

.PHONY: clean
clean:
	rm -rf build dist shields.egg-info
	find -name __pycache__ -exec rm -rf {} +
